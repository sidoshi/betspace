const setActiveMarkets = payload => ({
    type: 'SET_ACTIVE_MARKETS',
    payload
})

const updateMarket = payload => ({
    type: 'UPDATE_MARKET',
    payload
})
const closeMarket = payload => ({
    type: 'CLOSE_MARKET',
    payload
})

const deleteMarket = payload => ({
    type: 'DELETE_MARKET',
    payload
})

const setWatchedMarkets = payload => ({
    type: 'SET_WATCHED_MARKETS',
    payload
})

const removeWatchedMarkets = payload => ({
    type: 'REMOVE_WATCHED_MARKETS',
    payload
})

const addBlockedMarkets = payload => ({
    type: 'ADD_BLOCKED_MARKETS',
    payload
})

const removeBlockedMarkets = payload => ({
    type: 'REMOVE_BLOCKED_MARKETS',
    payload
})

const addLimitedMarkets = payload => ({
    type: 'ADD_LIMITED_MARKETS',
    payload,
})


export {
    setActiveMarkets,
    setWatchedMarkets,
    removeWatchedMarkets,
    updateMarket,
    deleteMarket,
    closeMarket,
    addBlockedMarkets,
    removeBlockedMarkets,
    addLimitedMarkets
}
