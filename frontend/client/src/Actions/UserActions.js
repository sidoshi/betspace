const updateUser = payload => ({
    type: 'UPDATE_USER',
    payload
})

const addUserLimit = payload => ({
    type: 'ADD_USER_LIMIT',
    payload
})

export {updateUser, addUserLimit}
