const filterBets = (filter, bets, activeBets) => {
    switch (filter) {
        case 'ALL':
            return bets
        case 'MATCHED':
            return bets.filter( id => {
                return activeBets.getIn(['byId', id, 'matched']) === true
            })
        case 'UNMATCHED':
            return bets.filter( id => {
                return activeBets.getIn(['byId', id, 'matched']) === false
            })
        default:
            return bets
    }
}


export default filterBets
