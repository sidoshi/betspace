import {bindActionCreators} from 'redux'
import alertify from 'alertify.js'

import socket from 'Lib/socket'
import {store} from 'Store'
import * as MarketsPageActions from 'Actions/MarketsPageActions'
import * as BetsActions from 'Actions/BetsActions'
import * as UserActions from 'Actions/UserActions'
import getProjection from './getProjection'
import * as ProjectionActions from 'Actions/ProjectionActions'
import jwtDecoded from 'Lib/jwtDecoded'

alertify.logPosition('bottom right')

let actions = bindActionCreators({
    ...BetsActions,
    ...MarketsPageActions,
    ...UserActions,
    ...ProjectionActions
},store.dispatch)

let {
    setActiveMarkets,
    updateMarket,
    removeWatchedMarkets,
    deleteMarket,
    addBet,
    updateBet,
    deleteBet,
    updateUser,
    deleteProjection,
    setProjection,
    setWatchedMarkets,
    addBlockedMarkets,
    removeBlockedMarkets,
    addLimitedMarkets,
    addUserLimit
} = actions

const getBetsAndMarket = bet => {
    let state = store.getState()
    let market = state.MarketsPage.getIn(['activeMarkets', bet.marketId])
    let bets = state.Bets.getIn(['activeBets','byMarket',bet.marketId])
    let byId = state.Bets.getIn(['activeBets','byId'])
    bets = bets.map( betId => byId.get(betId))
    return {market, bets}
}

const getMarket = marketId => {
    let state = store.getState()
    let market = state.MarketsPage.getIn(['activeMarkets', marketId])
    return market
}

const handleSocketListeners = () => {
    socket.on('MARKETS_UPDATE', markets => {
        setActiveMarkets(markets)
    })
    socket.on('MARKET_UPDATE', market => {
        updateMarket(market)
    })
    socket.on('MARKET_CLOSE', marketId => {
        deleteProjection(marketId)
        removeWatchedMarkets(marketId)
        deleteMarket(marketId)
    })
    socket.on('MARKET_CLOSE_WARN', market => {
        updateMarket(market)
    })
    socket.on('INPLAY_CHANGE', market => {
        let oldMarket = getMarket(market.marketId)
        if (oldMarket.inplay) return
        alertify.success(`${market.name} inplay`)
        updateMarket(market)
    })
    socket.on('BET_PLACED', bet => {
        addBet(bet)
        let {market, bets} = getBetsAndMarket(bet)
        let projection = getProjection(market, bets)
        setWatchedMarkets(bet.marketId)
        setProjection({
            marketId: bet.marketId,
            projection: projection
        })
        alertify.success('Bet Placed')
    })
    socket.on('BET_MATCHED', bet => {
        updateBet(bet)
        let {market, bets} = getBetsAndMarket(bet)
        let projection = getProjection(market, bets)
        setProjection({
            marketId: bet.marketId,
            projection: projection
        })
    })
    socket.on('REMOVED_UNMATCHED_BET', bet => {
        deleteBet(bet)
        alertify.log('Bet Removed')
    })
    socket.on('BET_RESULT', bet => {
        updateBet(bet)
    })
    socket.on('CANCEL_BET_FAILED', bet => {
        alertify.error('Bet cancelling failed')
    })
    socket.on('PLACE_BET_FAILED', message => {
        alertify.error(message)
    })
    socket.on('UPDATE_USER', user => {
        updateUser(user)
    })
    socket.on('MARKET_BLOCKED', marketId => {
        addBlockedMarkets(marketId)
    })
    socket.on('MARKET_UNBLOCKED', marketId => {
        removeBlockedMarkets(marketId)
    })
    socket.on('ADD_MARKET_LIMIT', limit => {
        addLimitedMarkets(limit)
    })
    socket.on('ADD_USER_LIMIT', limit => {
        let selfId = jwtDecoded.userId
        addUserLimit(limit[selfId] || {min: 0, max: Infinity})
    })
}

export default handleSocketListeners
