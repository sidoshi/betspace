const normalizeBets = bets => {
    let normalized = {
        byId: {},
        byMarket: {}
    }
    bets.forEach( bet => {
        if(!bet) return
        normalized.byId[bet._id] = bet
        !normalized.byMarket[bet.marketId] &&
        (normalized.byMarket[bet.marketId] = [])
        normalized.byMarket[bet.marketId].push(bet._id)
    })
    return normalized
}

export default normalizeBets
