const statusToWin = {
    'BACK': 'WINNER',
    'LAY':  'LOSER'
}

const getBetResult = (runnersStatus, bet) => {
    let status = runnersStatus[bet.get('runnerId')]
    if (status !== 'WINNER' && status !== 'LOSER'){
        return 'NEUTRAL'
    }
    if ( status === statusToWin[bet.get('betType')]){
        return 'WINNER'
    }else{
        return 'LOSER'
    }
}

const getRunnersStatus = (runnerIds, runnersStatus) => {
    runnerIds.forEach( runnerId => {
        runnersStatus[runnerId] = 'LOSER'
    })
}

const calculateBets = (bets, runnerId, runnersStatus, projection) => {
    bets.forEach( bet => {
        let result = getBetResult(runnersStatus, bet)
        let exposure = +bet.get('exposure')
        let profit = +bet.get('profit')
        if(!bet.get('matched')) return true
        projection.self[runnerId] = projection.self[runnerId] || 0
        switch(result){
            case 'WINNER':
                projection.self[runnerId] += profit
                return true
            case 'LOSER':
                projection.self[runnerId] -= exposure
                return true
            default:
                console.log('nither winner nor looser')
                return  true
        }
    })
}

const getProjection = (market, bets) => {
    let projection = {
        self: {}
    }
    let runnersStatus = {}

    let runners = market.get('runners')
    let runnerIds = runners.keySeq().toArray()
    getRunnersStatus(runnerIds, runnersStatus)
    runnerIds.forEach( runnerId => {
        runnersStatus[runnerId] = 'WINNER'
        calculateBets(bets, runnerId, runnersStatus, projection)
        runnersStatus[runnerId] = 'LOSER'
    })
    return projection
}

export default getProjection
