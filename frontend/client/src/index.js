import React from 'react'
import ReactDOM from 'react-dom'
import App from 'Components'
import { Provider } from 'react-redux'
import 'react-mdl/extra/material.css'
import 'react-mdl/extra/material.js'

import { store } from 'Store'
import handleSocketListeners from 'Lib/socketListeners'
import 'mdi/css/materialdesignicons.min.css'
import './main.css'


handleSocketListeners()

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
  document.getElementById('root')
)
