import React from 'react'
import {Textfield} from 'react-mdl'

const BetNumbers = ({handleChange, rate, stake, betType, profit, exposure}) =>
    <div>
        <Textfield
            onChange={handleChange}
            label="Rate"
            floatingLabel
            value={rate}
            name="rate"
            pattern="-?[0-9]*(\.[0-9]+)?"
            error="Input is not a number!"
        /><br />
        <Textfield
            onChange={handleChange}
            label="Stake"
            floatingLabel
            value={stake}
            name="stake"
            pattern="-?[0-9]*(\.[0-9]+)?"
            error="Input is not a number!"
        /><br />
         <Textfield
            disabled
            label="Exposure"
            floatingLabel
            value={exposure}
            className="Label"
        /><br />
         <Textfield
            disabled
            label="Profit"
            floatingLabel
            value={profit}
            className="Label"
        /><br />
    </div>

export default BetNumbers
