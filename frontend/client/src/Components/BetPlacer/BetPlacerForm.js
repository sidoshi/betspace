import React from 'react'
import {Button} from 'react-mdl'
import alertify from 'alertify.js'

import socket from 'Lib/socket'
import BetDetailsLabels from './BetDetailsLabels'
import BetNumbers from './BetNumbers'
import StakeButtons from './StakeButtons'
import BetRadios from './BetRadios'
import { toFixed } from 'Lib/numberUtils'

class BetPlacerForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            ...props,
            profit: 0,
            exposure: 0
        }
        this.handleChange = this.handleChange.bind(this)
        this.placeBet = this.placeBet.bind(this)
        this.setStake = this.setStake.bind(this)
    }
    handleChange(event){
        const target = event.target
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        const name = target.name
        this.setState( state => ({
            [name]: value
        }), this.calculateProfitAndExposure)
    }
    calculateProfitAndExposure = () => {
        let exposure = this.state.betType === 'BACK'
            ? this.state.stake
            : (this.state.rate - 1) * this.state.stake
        let profit = this.state.betType === 'BACK'
            ? (this.state.rate - 1) * this.state.stake
            : this.state.stake
        profit = toFixed(profit)
        exposure = toFixed(exposure)
        this.setState({
            exposure,
            profit
        })
    }
    setStake(stake){
        this.setState( state => ({
            stake
        }), this.calculateProfitAndExposure )
    }
    validatePlaceBet = bet => {
        if(isNaN(bet.stake)) return false
        if(isNaN(bet.rate)) return false
        if(bet.stake <= 0) return false
        if(bet.rate <= 0) return false
        if(['BACK', 'LAY'].indexOf(bet.betType) < 0) return false
        return true
    }
    placeBet(){
        let state = this.state
        let bet = {
            stake: +state.stake,
            rate: +state.rate,
            betType: state.betType,
            runnerId: state.runnerId,
            marketId: state.marketId
        }
        if (this.props.blocked) {
            this.props.closeBetPlacer()
            return alertify.error('Market Blocked')
        } 
        if(this.validatePlaceBet(bet)){
            socket.emit('PLACE_BET', bet)
            this.props.closeBetPlacer()
        }else{
            alertify.logPosition('bottom right')
            alertify.error('Invalid Bet')
        }
    }
    render(){
        let {market,runners} = this.props
        let runnerId = this.state.runnerId
        return(
            <div className="BetPlacerForm">
                <BetDetailsLabels
                    market={market}
                    runner={runners.get('' + runnerId)}
                />
                <BetNumbers
                    handleChange={this.handleChange}
                    rate={this.state.rate}
                    stake={this.state.stake}
                    betType={this.state.betType}
                    profit={this.state.profit}
                    exposure={this.state.exposure}
                />
                <StakeButtons setStake={this.setStake} />
                <BetRadios
                    handleChange={this.handleChange}
                    betType={this.state.betType}
                />
                <Button
                    onClick={this.placeBet}
                    primary
                    ripple
                    raised
                >
                    Place Bet
                </Button>
                <Button
                    onClick={this.props.closeBetPlacer}
                    accent
                    ripple
                    raised
                >
                    Close
                </Button>
            </div>
        )
    }
}

export default BetPlacerForm
