import React from 'react'
import $ from 'jquery'


class Overlay extends React.Component {
    constructor(){
        super()
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.closeOverlay = this.closeOverlay.bind(this)
    }
    closeOverlay(event, closeBetPlacer){
        if(event.target === event.currentTarget){
            closeBetPlacer()
        }
    }
    handleKeyDown(e){
        if(e.keyCode === 27){
            this.props.closeBetPlacer()
        }
    }
    componentDidMount(){
        $(document.body).on('keydown', this.handleKeyDown)
    }
    componentWillUnmount(){
        $(document.body).off('keydown')
    }
    render(){
        let {closeBetPlacer,visible,children} = this.props
        let {closeOverlay} = this
        return (
            visible
            ?   (
                    <div
                        className="BetPlacer"
                        onClick={(e) => closeOverlay(e,closeBetPlacer)}
                    >
                        {children}
                    </div>
                )
            :   <div></div>
        )
    }
}




export default Overlay
