import React from 'react'

import Overlay from './Overlay'
import BetPlacerForm from './BetPlacerForm'
import './BetPlacer.css'

const BetPlacer = (props) => {
    let {
        closeBetPlacer,
        visible,
    } = props

    return (
        <Overlay closeBetPlacer={closeBetPlacer} visible={visible} >
            <BetPlacerForm {...props} />
        </Overlay>
    )
}


export default BetPlacer
