import React from 'react'
import {Textfield} from 'react-mdl'

const BetDetailsLabels = ({market,runner}) =>
    <div>
        <Textfield
            disabled
            label="Match"
            floatingLabel
            value={market.get('name')}
            className="Label"
        /><br />
        <Textfield
            disabled
            label="Selection"
            floatingLabel
            value={runner.get('name')}
            className="Label"
        /><br />
    </div>

export default BetDetailsLabels
