import React from 'react'
import {RadioGroup, Radio} from 'react-mdl'

const BetRadios = ({handleChange, betType}) =>
    <div>
        <RadioGroup name="betType" value={betType} onChange={handleChange}>
            <Radio value="BACK" ripple>Back</Radio>
            <Radio value="LAY" ripple>Lay</Radio>
        </RadioGroup>
        <br />
    </div>

export default BetRadios
