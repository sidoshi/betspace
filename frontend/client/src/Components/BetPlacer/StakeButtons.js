import React from 'react'
import {Button} from 'react-mdl'

const StakeButtons = ({setStake}) =>
    <div>
        <Button
            onClick={() => setStake(100)}
            raised
            ripple
        >
            100
        </Button>
        <Button
            onClick={() => setStake(1000)}
            raised
            ripple
        >
            1000
        </Button>
        <Button
            onClick={() => setStake(10000)}
            raised
            ripple
        >
            10000
        </Button>
        <Button
            onClick={() => setStake(100000)}
            raised
            ripple
        >
            100000
        </Button>
    </div>


export default StakeButtons
