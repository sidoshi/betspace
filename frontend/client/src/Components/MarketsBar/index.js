import React from 'react'

import EventList from 'Components/EventList'
import './MarketsBar.css'
import config from 'Lib/config'

let {eventTypes} = config

const MarketsBar = ({
    activeMarkets,
    setWatchedMarkets,
    watchedMarkets,
    blockedMarkets
}) =>
    <div className='MarketsBar'>
        {eventTypes.map( eventType =>
            <EventList
                key={eventType}
                eventType={eventType}
                activeMarkets={activeMarkets}
                setWatchedMarkets={setWatchedMarkets}
                watchedMarkets={watchedMarkets}
                blockedMarkets={blockedMarkets}
            />
        )}
    </div>

export default MarketsBar

MarketsBar.propTypes = {
    activeMarkets: React.PropTypes.object.isRequired,
    watchedMarkets: React.PropTypes.object.isRequired,
    setWatchedMarkets: React.PropTypes.func.isRequired
}
