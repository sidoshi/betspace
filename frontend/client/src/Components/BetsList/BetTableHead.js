import React from 'react'

const BetTableHead = () =>
    <thead>
        <tr>
            <td>Selection</td>
            <td>Rate</td>
            <td>Stake</td>
            <td>Profit</td>
            <td>Exposure</td>
            <td>Type</td>
            <td>Result</td>
            <td>Matched</td>
            <td></td>
        </tr>
    </thead>

export default BetTableHead
