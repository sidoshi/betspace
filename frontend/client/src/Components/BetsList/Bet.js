import React from 'react'
import classnames from 'classnames'
import {IconButton} from 'react-mdl'

const getBetTypeClass = betType => classnames({
    back: betType === 'BACK',
    lay:  betType === 'LAY'
})

import socket from 'Lib/socket'

const Bet = ({
    id,
    activeBets
}) => {
    let bet = activeBets.getIn(['byId',id])
    let matched = bet.get('matched')
    let deleteButton = (
        <IconButton onClick={() => socket.emit('CANCEL_BET', bet) } name="delete">
            delete
        </IconButton>
    )
    return (
        <tr className={getBetTypeClass(bet.get('betType'))}>
            <td>{bet.get('runnerName')}</td>
            <td>{bet.get('rate')}</td>
            <td>{bet.get('stake')}</td>
            <td>{bet.get('profit')}</td>
            <td>{bet.get('exposure')}</td>
            <td>{bet.get('betType')}</td>
            <td>{bet.get('result')}</td>
            <td>{'' + matched}</td>
            <td>{!matched && deleteButton}</td>
        </tr>
    )
}

export default Bet
