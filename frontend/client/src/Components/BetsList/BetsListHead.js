import React from 'react'
import {Tabs, Tab} from 'react-mdl'

const handleTabs = (tabId,setFilter) => {
    let filter = ['ALL', 'MATCHED', 'UNMATCHED']
    setFilter(filter[tabId])
}

const BetsListHead = ({setFilter, filter}) =>
    <div className="BetsListHead">
        <Tabs
            activeTab={0}
            onChange={(tabId) => handleTabs(tabId,setFilter)}
            ripple
        >
            <Tab>All</Tab>
            <Tab>Matched</Tab>
            <Tab>Unmatched</Tab>
        </Tabs>
    </div>

export default BetsListHead
