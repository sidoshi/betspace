import React from 'react'

import filterBets from 'Lib/filterBets'
import BetsListHead from './BetsListHead'
import BetTableHead from './BetTableHead'
import Bet from './Bet'
import './BetsList.css'

class BetsList extends React.Component {
    constructor(){
        super()
        this.state = {
            filter: 'ALL'
        }
        this.setFilter = this.setFilter.bind(this)
    }
    setFilter(filter){
        this.setState({filter: filter})
    }
    render(){
        let {
            activeBets,
            marketId
        } = this.props
        let betsList = activeBets.getIn(['byMarket',marketId])
        let size = (betsList && betsList.size) || 0
        if(size <= 0) return <div className="BetsList"></div>
        betsList = filterBets(this.state.filter, betsList, activeBets)
        return (
            <div className="BetsList">
                <BetsListHead
                    setFilter={this.setFilter}
                    filter={this.state.filter}
                />
                <table>
                    <BetTableHead />
                    <tbody>
                        {betsList.map( id =>
                            <Bet
                                id={id}
                                activeBets={activeBets}
                                key={id}
                            />
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}


export default BetsList
