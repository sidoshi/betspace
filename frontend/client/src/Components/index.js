import React from 'react'
import { Router, Route, hashHistory, IndexRoute } from 'react-router'

import Layout from './Layout'
import MarketsPage from './MarketsPage'

const App = () => (
    <Router history={hashHistory} >
        <Route path="/" component={Layout} >
            <IndexRoute component={MarketsPage} />
        </Route>
    </Router>
)

export default App
