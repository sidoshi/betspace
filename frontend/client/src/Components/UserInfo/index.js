import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Button} from 'react-mdl'
import classnames from 'classnames'

import * as userActions from 'Actions/UserActions'
import PasswordChanger from 'Components/PasswordChanger'
import jwtDecoded from 'Lib/jwtDecoded'
import request from 'Lib/request'
import config from 'Lib/config'
import socket from 'Lib/socket'
import './UserInfo.css'

class UserInfo extends React.Component {
    constructor(){
        super()
        this.state = {
            passwordChangerVisible: false,
            connected: socket.connected
        }
    }
    getConnectedClass = (connected) => {
        return classnames({
            mdi: true,
            'mdi-lan-connect': connected,
            'mdi-lan-disconnect': !connected,
            'connected': connected,
            connection: true
        })
    }
    handleSocketConnection = () => {
        socket.on('disconnect', () => {
            this.setState({
                connected: false
            })
        })
        socket.on('connect', () => {
            this.setState({
                connected: true,
            })
        })

    }
    openPasswordChanger = () => {
        this.setState({
            passwordChangerVisible: true
        })
    }
    closePasswordChanger = () => {
        this.setState({
            passwordChangerVisible: false
        })
    }
    logout(){
        request.post('/logout')
        .then( res => {
            let {url,port} = config
            window.location = `${url}:${port}`
        })
        .catch( err => console.log(err))
    }
    componentDidMount(){
        this.setState({
            connected: socket.connected
        })
        this.handleSocketConnection()
        request.get('/api/users/user-info')
        .then( res => {
            let {updateUser} = this.props
            updateUser(res.data)
        })
    }
    render(){
        let {userType} = jwtDecoded
        let connected = this.state.connected
        let connectedClass = this.getConnectedClass(connected)
        return (
            <div className="UserInfo">
                <i className={connectedClass}/>
                <Button ripple className="name" accent>
                    {userType}
                </Button>
                <Button onClick={this.openPasswordChanger} accent raised ripple>
                    <i className="mdi mdi-lock"></i>
                    Change Password
                </Button>
                <Button onClick={this.logout} accent raised ripple>
                    <i className="mdi mdi-logout"></i>
                    logout
                </Button>
                <PasswordChanger
                    visible={this.state.passwordChangerVisible}
                    closePasswordChanger={this.closePasswordChanger}
                />
            </div>
        )
    }
}

const mapStateToProps = store => ({
    user: store.User
})
const mapDispatchToProps = dispatch =>
    bindActionCreators(userActions, dispatch)

export default connect(mapStateToProps,mapDispatchToProps)(UserInfo)
