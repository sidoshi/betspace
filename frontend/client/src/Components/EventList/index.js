import React from 'react'
import _ from 'lodash'
import $ from 'jquery'
import classnames from 'classnames'

import './EventList.css'
import socket from 'Lib/socket'

const watchMarkets = (marketId,setWatchedMarkets) => {
    setWatchedMarkets(marketId)
    socket.emit('SUBSCRIBE', marketId)
}



const getEventList = (
    activeMarkets, 
    eventType, 
    setWatchedMarkets,
    blockedMarkets
) => {
    let id = _.kebabCase(eventType)
    let getInplayClass = (inplay, status) => classnames({
        mdi: true,
        'mdi-octagon': true,
        inplay: inplay,
        closed: status === 'CLOSED',
        suspended: status === 'SUSPENDED'
    })
    return (
        <ul id={id}>
            {activeMarkets.get(eventType).map( marketId => {
                let eventName = activeMarkets.getIn([marketId, 'name'])
                let inplay = activeMarkets.getIn([marketId, 'inplay'])
                let status = activeMarkets.getIn([marketId, 'status'])
                let inplayClass = getInplayClass(inplay,status)
                let blocked = blockedMarkets.indexOf(marketId) >= 0
                return (
                    <li
                        key={marketId}
                        title={eventName}
                        onClick={() => watchMarkets(marketId,setWatchedMarkets)}
                        className={blocked ? 'blocked' : ''}
                    >

                        <i className={inplayClass}></i>
                        <span>{eventName}</span>
                    </li>
                )
            })}
        </ul>
    )
}

const EventList = ({
    activeMarkets,
    eventType,
    setWatchedMarkets,
    watchedMarkets,
    blockedMarkets
}) => {
    let length = activeMarkets.get(eventType).size
    let id = _.kebabCase(eventType)
    return (
        <div className="EventList" >
            <span
                className="EventHead"
                onClick={ () => $(`#${id}`).slideToggle() }
            >
                {eventType}
                <i className="length">{length}</i>
            </span>
            {
                length > 0 &&
                getEventList(
                    activeMarkets, 
                    eventType, 
                    setWatchedMarkets,
                    blockedMarkets
                )
            }
        </div>
    )
}

export default EventList

EventList.propTypes = {
    activeMarkets:          React.PropTypes.object.isRequired,
    eventType:              React.PropTypes.string.isRequired,
    setWatchedMarkets:      React.PropTypes.func.isRequired,
    watchedMarkets:         React.PropTypes.object.isRequired
}
