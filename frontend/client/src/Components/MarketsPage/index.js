import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import co from 'co'

import MarketsBar from 'Components/MarketsBar'
import MarketsList from 'Components/MarketsList'
import request from 'Lib/request'
import * as MarketsPageActions from 'Actions/MarketsPageActions'
import * as ProjectionActions from 'Actions/ProjectionActions'
import * as BetsActions from 'Actions/BetsActions'
import normalizeBets from 'Lib/normalizeBets'
import getProjection from 'Lib/getProjection'
import socket from 'Lib/socket'
import SelectedUserInfo from 'Components/SelectedUserInfo'

const normalizeLimitedMarkets = m => {
    let limitedMarkets = {}
    m.forEach(m => {
        limitedMarkets[m.marketId] = {
            min: m.min,
            max: m.max,
        }
    })
    return limitedMarkets
}


class MarketsPage extends React.Component {
    constructor(){
        super()
        this.state = {
            visible: false
        }
    }
    toggleVisibility = () => {
        this.setState({
            visible: !this.state.visible
        })
    }
    componentDidMount(){
        let {
            setActiveMarkets,
            setActiveBets,
            setWatchedMarkets,
            addBlockedMarkets,
            addLimitedMarkets
        } = this.props
        let {updateMarketProjection} = this
        co(function* () {
            let res = yield request.get('/api/betfair/active-markets')
            setActiveMarkets(res.data)
            res = yield request.get('/api/betfair/active-bets')
            let bets = normalizeBets(res.data)
            res = yield request.get('/api/betfair/blocked-markets')
            addBlockedMarkets(res.data)
            res = yield request.get('/api/betfair/limited-markets')
            let limitedMarkets = normalizeLimitedMarkets(res.data)
            addLimitedMarkets(limitedMarkets)
            setActiveBets(bets)
            updateMarketProjection()
            Object.keys(bets.byMarket).forEach( marketId => {
                setWatchedMarkets(marketId)
                socket.emit('SUBSCRIBE', marketId)
            })
        }).catch(console.log)
    }
    updateMarketProjection = () => {
        let {activeBets} = this.props
        let byId = activeBets.get('byId')
        let byMarket = activeBets.get('byMarket')
        byMarket.forEach((betIds, marketId) => {
            let bets = betIds.map( id => byId.get(id) )
            let market = this.props.activeMarkets.get(marketId)
            if(!market) return
            let projection = getProjection(market, bets)
            let {setProjection} = this.props
            setProjection({
                marketId: marketId,
                projection: projection
            })
        })
    }
    render(){
        let {
            activeMarkets,
            setWatchedMarkets,
            watchedMarkets,
            removeWatchedMarkets,
            activeBets,
            projections,
            blockedMarkets,
            limitedMarkets
        } = this.props
        blockedMarkets = blockedMarkets.get('allMarkets') || []
        return(
            <div className="MarketsPage">
                <MarketsBar
                    activeMarkets={activeMarkets}
                    watchedMarkets={watchedMarkets}
                    setWatchedMarkets={setWatchedMarkets}
                    blockedMarkets={blockedMarkets}
                />
                <MarketsList
                    activeMarkets={activeMarkets}
                    watchedMarkets={watchedMarkets}
                    removeWatchedMarkets={removeWatchedMarkets}
                    activeBets={activeBets}
                    projections={projections}
                    blockedMarkets={blockedMarkets}
                    limitedMarkets={limitedMarkets}
                />
                <SelectedUserInfo 
                    user={this.props.user}
                    visible={this.state.visible}
                    toggleVisibility={this.toggleVisibility}
                />
            </div>
        )
    }
}

const mapStateToProps = store => ({
    activeMarkets:  store.MarketsPage.get('activeMarkets'),
    blockedMarkets: store.MarketsPage.get('blockedMarkets'),
    watchedMarkets: store.MarketsPage.get('watchedMarkets'),
    limitedMarkets: store.MarketsPage.get('limitedMarkets'),
    activeBets:     store.Bets.get('activeBets'),
    projections:    store.Projections,
    user:           store.User
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        ...BetsActions,
        ...MarketsPageActions,
        ...ProjectionActions
    },dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(MarketsPage)

MarketsPage.propTypes = {
    setActiveMarkets:       React.PropTypes.func.isRequired,
    setWatchedMarkets:      React.PropTypes.func.isRequired,
    removeWatchedMarkets:   React.PropTypes.func.isRequired,
    activeMarkets:          React.PropTypes.object.isRequired,
    watchedMarkets:         React.PropTypes.object.isRequired
}
