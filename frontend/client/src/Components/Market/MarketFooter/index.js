import React from 'react'
import classnames from 'classnames'

import './MarketFooter.css'

const getStatusClass = (blocked) => classnames({
    status: true,
    blocked: blocked
})

const MarketFooter = ({market, blocked, limit}) => {
    let status = market.get('status')
    let statusText = blocked ? 'MARKET BLOCKED' : `MARKET ${status}`
    let statusClass = getStatusClass(blocked)
    let min, max
    if (limit) {
        min = limit.get('min') || 0
        max = limit.get('max') || 'Infinite'
    }
    let limitSpan = limit 
        ? <span className="limit">{min} - {max}</span> 
        : null
    return (
        <div className='MarketFooter'>
            <span className={statusClass}>{statusText}</span>
            {limitSpan}
            <span className="time">{market.get('openDate')}</span>
        </div>
    )
}

export default MarketFooter

MarketFooter.propTypes = {
    market: React.PropTypes.object.isRequired
}
