import React from 'react'

import MarketHeader from './MarketHeader'
import MarketFooter from './MarketFooter'
import RunnersRateList from 'Components/RunnersRateList'
import BetsList from 'Components/BetsList'
import './Market.css'

const Market = ({
    market,
    removeWatchedMarkets,
    activeBets,
    projection,
    blockedMarkets,
    limit
}) => {
    return (
        <div className="Market">
            <MarketHeader
                market={market}
                removeWatchedMarkets={removeWatchedMarkets}
            />
            <RunnersRateList
                runners={market.get('runners')}
                marketId={market.get('marketId')}
                market={market}
                projection={projection}
                blocked={blockedMarkets.indexOf(market.get('marketId')) >= 0}
            />
            <BetsList
                activeBets={activeBets}
                marketId={market.get('marketId')}
            />
            <MarketFooter 
                market={market} 
                blocked={blockedMarkets.indexOf(market.get('marketId')) >= 0}
                limit={limit}
            />
        </div>
    )
}

export default Market
