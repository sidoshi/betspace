import React from 'react'
import classnames from 'classnames'

import { toFixed } from 'Lib/numberUtils'

const rateClass = valueType => classnames({
    price: valueType === 'price',
    size: valueType === 'size',
    rate: true
})

const cellClass = (element, betType) => classnames({
    RateCell: true,
    main: element === 0,
    back: betType === 'availableToBack',
    lay: betType === 'availableToLay',
})

const makeBet = (runnerId, marketId, price, type) => {
    let betType = type === 'availableToBack' ? 'BACK' : 'LAY'
    let bet = {
        stake: '',
        rate: price,
        betType: betType,
        marketId: marketId,
        runnerId: runnerId
    }
    return bet
}



const RateCell = ({
    type,
    element,
    runner,
    marketId,
    showBetPlacer
}) => {
    let price = runner.getIn([type, element, 'price']) || '-'
    let size = runner.getIn([type, element, 'size'])
    size = toFixed(size) || '-'
    let priceClass = rateClass('price')
    let sizeClass = rateClass('size')
    let id = runner.get('selectionId')
    return(
        <td
            className={cellClass(element, type)}
            onClick={() => {
                let bet = makeBet(id,marketId,price,type)
                showBetPlacer(bet)
            }}
        >
            <p className={priceClass}>{price}</p>
            <p className={sizeClass}>{size}</p>
        </td>
    )
}

export default RateCell
