import React from 'react'

import RunnersRateListHead from './RunnersRateListHead'
import RunnerRate from './RunnerRate'
import BetPlacer from 'Components/BetPlacer'
import './RunnersRateList.css'

class RunnersRateList extends React.Component {
    constructor(){
        super()
        this.state = {
            visible: false,
            stake: '',
            rate: '',
            betType: '',
            marketId: '',
            runnerId: ''
        }
        this.showBetPlacer = this.showBetPlacer.bind(this)
        this.closeBetPlacer = this.closeBetPlacer.bind(this)
    }
    closeBetPlacer(){
        this.setState({
            visible: false
        })
    }
    showBetPlacer(betValues){
        this.setState({
            visible: true,
            ...betValues
        })
    }
    render(){
        let {runners, marketId, projection, market} = this.props
        return (
            <div className="RunnersRateList">
                <table>
                    <RunnersRateListHead />
                    <tbody>
                        {market.get('sortedRunners').map( runnerId =>
                            <RunnerRate
                                runner={runners.get(''+runnerId)}
                                marketId={marketId}
                                key={runnerId}
                                showBetPlacer={this.showBetPlacer}
                                projection={projection}
                            />
                        )}
                    </tbody>
                </table>
                <BetPlacer
                    {...this.state}
                    closeBetPlacer={this.closeBetPlacer}
                    market={this.props.market}
                    runners={this.props.runners}
                    blocked={this.props.blocked}
                />
            </div>
        )
    }
}

export default RunnersRateList
