import { combineReducers } from 'redux'

import MarketsPageReducer from './MarketsPageReducer'
import BetsReducer from './BetsReducer'
import UserReducer from './UserReducer'
import ProjectionsReducers from './ProjectionsReducer'


let reducer = combineReducers({
    MarketsPage: MarketsPageReducer,
    Bets:        BetsReducer,
    User:        UserReducer,
    Projections: ProjectionsReducers
})

exports.reducer = reducer
