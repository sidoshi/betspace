import { fromJS } from 'immutable'

let defaults = fromJS({
    balance: '-'
})

const UserReducer = (state=defaults, action) => {
    let payload = action.payload
    switch(action.type){
        case 'UPDATE_USER':
            payload.userId = payload.userId || payload._id
            return state = fromJS(payload)
        case 'ADD_USER_LIMIT':
            return state.set('userLimit', fromJS({
                min: payload.min,
                max: payload.max
            }))
        default:
            return state
    }
}


export default UserReducer
