#!/bin/bash

rm -rf build
npm run build
cd build
html2jade index.html
rm index.html
mv index.jade client.pug
mv client.pug ../../../backend/slave/_views/
rm -rf ../../../backend/slave/_static/client/*
mv * ../../../backend/slave/_static/client/
cd ..
rm -rf build

