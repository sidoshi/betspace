import React from 'react'

import MarketHeader from './MarketHeader'
import MarketFooter from './MarketFooter'
import RunnersRateList from 'Components/RunnersRateList'
import MarketBooks from 'Components/MarketBooks'
import './Market.css'

const Market = ({
    market,
    removeWatchedMarkets,
    activeBets,
    projection,
    users,
    blockedMarkets,
    limit
}) =>
    <div className="Market">
        <MarketHeader
            market={market}
            removeWatchedMarkets={removeWatchedMarkets}
        />
        <RunnersRateList
            runners={market.get('runners')}
            marketId={market.get('marketId')}
            market={market}
            projection={projection}
        />
        <MarketBooks
            projection={projection}
            runners={market.get('runners')}
            users={users}
            market={market}
        />
        <MarketFooter 
            market={market} 
            blocked={blockedMarkets.indexOf(market.get('marketId')) >= 0}
            limit={limit}
        />
    </div>

export default Market
