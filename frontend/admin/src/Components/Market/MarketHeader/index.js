import React from 'react'
import classnames from 'classnames'

import './MarketHeader.css'

const inplayClass = (inplay, status) => classnames({
    inplayStatus: true,
    mdi: true,
    'mdi-octagon': true,
    inplay: inplay,
    closed: status === 'CLOSED'
})

const MarketHeader = ({
    market,
    removeWatchedMarkets
}) => {
    let inplay = market.get('inplay')
    let status = market.get('status')
    return (
        <div className="MarketHeader">
            <span className={inplayClass(inplay, status)}></span>
            <span className="marketName">{market.get('name')}</span>
            <span
                className="close mdi mdi-close"
                onClick={() => removeWatchedMarkets(market.get('marketId'))}
            >
            </span>
        </div>
    )
}

export default MarketHeader

MarketHeader.propTypes = {
    market:                     React.PropTypes.object.isRequired,
    removeWatchedMarkets:       React.PropTypes.func.isRequired
}
