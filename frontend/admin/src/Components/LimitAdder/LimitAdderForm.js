import React from 'react'
import { Textfield, Button } from 'react-mdl'
import alertify from 'alertify.js'

import request from 'Lib/request'

class LimitAdderForm extends React.Component {
    constructor(props) {
        super(props)
        let min = 0
        let max = Infinity
        let limit = this.props.limit
        if (limit){
            min = limit.get('min') || 0
            max = limit.get('max') || Infinity
        }
        this.state = {min, max}
    }
    handleChange = event => {
        const target = event.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name
        this.setState({
            [name]: value,
        })
    }
    addLimit = async () => {
        let error = alertify.error
        let min = this.state.min
        let max = this.state.max
        let marketId = this.props.marketId
        if (isNaN(min)) return error('Please enter valid minimum value')
        if (isNaN(max)) return error('Please enter valid maximum value')
        try {
            let res = await request.post('/api/betfair/limit-market', {
                min,
                max,
                marketId
            })
            alertify.success(res.data)
            this.props.closeLimitAdder()
        } catch (err) {
            error(err.response.data)
        }
    }
    render() {
        let { handleChange } = this
        return (
            <div>
                <Textfield
                    onChange={handleChange}
                    label="Market Name"
                    floatingLabel
                    value={this.props.marketName}
                    name="marketName"
                    disabled
                />
                <br />
                <Textfield
                    onChange={handleChange}
                    label="Minimum Value"
                    floatingLabel
                    value={this.state.min}
                    name="min"
                />
                <br />
                <Textfield
                    onChange={handleChange}
                    label="Maximum Value"
                    floatingLabel
                    value={this.state.max}
                    name="max"
                />
                <br />
                <Button onClick={this.addLimit} raised colored>
                    Limit Market
                </Button>
                <Button
                    onClick={this.props.closeLimitAdder}
                    raised
                    colored
                >
                    Cancel
                </Button>
            </div>
        )
    }
}

export default LimitAdderForm
