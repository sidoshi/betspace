import React from 'react'

import Modal from 'Components/Modal'

import LimitAdderForm from './LimitAdderForm'
import './LimitAdder.css'

const LimitAdder = ({visible, closeLimitAdder, marketId, limit, market}) =>
    <Modal closeModal={closeLimitAdder} visible={visible}>
        <div className="LimitAdder">
            <LimitAdderForm 
                closeLimitAdder={closeLimitAdder} 
                marketId={marketId}
                limit={limit}
                marketName={(market && market.get('name')) || ''}
            />
        </div>
    </Modal>

export default LimitAdder