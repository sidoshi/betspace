import React from 'react'
import $ from 'jquery'
import { ContextMenuProvider } from 'react-contexify'
import classnames from 'classnames'

import './UserTree.css'
import jwtDecoded from 'Lib/jwtDecoded'

const getUserClass = (blocked,online) => classnames({
    user: true,
    blocked: blocked,
    online: online
})

const bindSlideToggle = userId => {
    $(`#${userId}children`).slideToggle()
    let buttonClasses = 'mdi-plus-box-outline mdi-minus-box-outline'
    $(`#${userId}button i`).toggleClass(buttonClasses)
}

const TreeToggleButton = ({userId}) => {
    let buttonClass = userId === jwtDecoded.userId
        ? 'mdi mdi-minus-box-outline'
        : 'mdi mdi-plus-box-outline'
    return (
        <span
            onClick={() => bindSlideToggle(userId)}
            id={userId + 'button'}
            className="TreeToggleButton"
        >
            <i className={buttonClass}></i>
        </span>
    )
}

const Username = ({user, setSelectedUser}) => {
    let blocked = user.get('blocked')
    let online  = user.get('online')
    let userClass = getUserClass(blocked, online)
    let id = user.get('userId')
    return(
        <ContextMenuProvider id="user-context" renderTag="span">
            <span id={id}>
                <span
                    id={user.get('userId')}
                    className={userClass}
                    onClick={() => setSelectedUser(id)}
                >
                    <i className="mdi mdi-account-box"></i>
                    {user.get('username')}
                </span>
                <span
                    className="balance"
                    id={user.get('userId')}
                >
                    <i className="mdi mdi-coin"></i>
                    {user.get('balance')}
                </span>
            </span>
        </ContextMenuProvider>
    )
}




function ChildrenList({children, userId, users, setSelectedUser}) {
    let style = {}
    if (userId !== jwtDecoded.userId) style.display = 'none'
    return (
        <ul id={userId + 'children'} style={style}>
            {children.map(userId => {
                return (
                    <li key={userId}>
                        <UserTree 
                            userId={userId} 
                            users={users} 
                            setSelectedUser={setSelectedUser} 
                        />
                    </li>
                )
            })}
        </ul>
    )
}

class UserTree extends React.Component{
    componentWillUpdate(){
        let {userId, users} = this.props
        let user = users.get(userId)
        if(!user) return null
        user.rendered = false
    }
    render(){
        let {userId, users, setSelectedUser} = this.props
        let user = users.get(userId)
        if(!user) return null
        if(user.rendered) return null
        user.rendered = true

        let children = user.get('children')
        let hasChildren = children.size > 0
        let mainUser = users.get(jwtDecoded.userId)
        let lastChild = (
            mainUser.get('children').indexOf(userId) < 0 &&
            userId !== jwtDecoded.userId
        )
        if(!hasChildren || lastChild) return (
            <div className="UserTree">
                <span className="Username childless">
                    <Username user={user} setSelectedUser={setSelectedUser} />
                </span>
            </div>
        )
        return (
            <div className="UserTree">
                <span className="Username">
                    <TreeToggleButton userId={userId}/>
                    <Username user={user} setSelectedUser={setSelectedUser} />
                </span>
                <ChildrenList 
                    children={children} 
                    userId={userId} 
                    users={users}
                    setSelectedUser={setSelectedUser}
                />
            </div>
        )
    }
}

export default UserTree
