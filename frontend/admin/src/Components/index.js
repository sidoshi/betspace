import React from 'react'
import { Router, Route, hashHistory, IndexRoute} from 'react-router'

import Layout from './Layout'
import MarketsPage from 'Components/MarketsPage'
import AccountPage from 'Components/AccountPage'
import handleSocketListeners from 'Lib/socketListeners'

handleSocketListeners()

const App = () => (
    <Router history={hashHistory} >
        <Route path="/" component={Layout} >
            <IndexRoute component={MarketsPage} />
            <Route path="/account" component={AccountPage} />
        </Route>
    </Router>
)

export default App
