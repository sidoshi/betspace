import React from 'react'

import Modal from 'Components/Modal'

import UserLimitAdderForm from './UserLimitAdderForm'
import './UserLimitAdder.css'

const UserLimitAdder = ({ 
    visible, 
    closeLimitAdder, 
    name,
    limit, 
    userId
}) => (
    <Modal closeModal={closeLimitAdder} visible={visible}>
        <div className="UserLimitAdder">
            <UserLimitAdderForm
                closeLimitAdder={closeLimitAdder}
                limit={limit}
                name={name}
                userId={userId}
            />
        </div>
    </Modal>
)

export default UserLimitAdder
