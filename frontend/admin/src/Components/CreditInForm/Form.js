import React from 'react'
import { Textfield, Button } from 'react-mdl'
import alertify from 'alertify.js'
import co from 'co'

import request from 'Lib/request'


class Form extends React.Component {
    constructor() {
        super()
        this.state = {
            credit: ''
        }
    }
    creditIn = () => {
        let userId = this.props.user.get('userId')
        let credit = this.state.credit
        let self = this.props.self
        let {closeCreditInForm} = this.props
        if (credit > self.get('balance')){
            alertify.error('You dont have enough balance')
            return
        }
        co(function*(){
            let res = yield request.post('/api/users/credit-in',{
                userId: userId,
                credit: credit
            })
            if(res.status === 200) alertify.success('Credited in')
            else alertify.error('Failed to Credit in')
            closeCreditInForm()
        }).catch(console.log)
    }
    handleChange = event => {
        const target = event.target
        const value = target.type === 'checkbox'
            ? target.checked
            : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
    }
    render(){
        let self = this.props.self
        return (
            <div>
                <Textfield
                    disabled
                    label="Username"
                    floatingLabel
                    value={this.props.user.get('username')}
                    className="Label"
                /><br />
                <Textfield
                    disabled
                    label="Current Credit"
                    floatingLabel
                    value={this.props.user.get('credit')}
                    className="Label"
                /><br />
                <Textfield
                    onChange={this.handleChange}
                    label="Credit"
                    floatingLabel
                    value={this.state.credit}
                    name="credit"
                    pattern="-?[0-9]*(\.[0-9]+)?"
                    error="Input is not a number!"
                /><br />
                <Textfield
                    disabled
                    label="Own Balance"
                    floatingLabel
                    value={+self.get('balance') - +this.state.credit}
                /><br />
                <Button onClick={this.creditIn} raised colored>Credit In</Button>
                <Button onClick={this.props.closeCreditInForm} raised colored>
                    Cancel
                </Button>
            </div>
        )
    }
}

export default Form