import React from 'react'

import Modal from 'Components/Modal'
import Form from './Form'
import './CreditInForm.css'

const CreditInForm = ({visible, closeCreditInForm, users, creditInId, self}) =>
    <Modal closeModal={closeCreditInForm} visible={visible}>
        <div className="CreditInForm">
            <Form
                closeCreditInForm={closeCreditInForm}
                user={users.get(creditInId)}
                self={self}
            />
        </div>
    </Modal>

export default CreditInForm