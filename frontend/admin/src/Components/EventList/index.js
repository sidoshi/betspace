import React from 'react'
import _ from 'lodash'
import $ from 'jquery'
import classnames from 'classnames'
import { ContextMenuProvider } from 'react-contexify'

import './EventList.css'
import socket from 'Lib/socket'

const watchMarkets = (marketId,setWatchedMarkets) => {
    setWatchedMarkets(marketId)
    socket.emit('SUBSCRIBE', marketId)
}

let getInplayClass = (inplay, status) => classnames({
    mdi: true,
    'mdi-octagon': true,
    inplay: inplay,
    closed: status === 'CLOSED',
    suspended: status === 'SUSPENDED'
})

let getBettedClass = (betted, blocked) => classnames({
    betted: betted,
    blocked: blocked
})

const getEventList = (
    activeMarkets,
    eventType,
    setWatchedMarkets,
    activeBets,
    blockedMarkets
) => {
    let id = _.kebabCase(eventType)
    return (
        <ul id={id}>
            {activeMarkets.get(eventType).map( marketId => {
                let eventName = activeMarkets.getIn([marketId, 'name'])
                let inplay = activeMarkets.getIn([marketId, 'inplay'])
                let status = activeMarkets.getIn([marketId, 'status'])
                let betted = activeBets.getIn(['byMarket',marketId])
                let inplayClass = getInplayClass(inplay,status)
                let blocked = blockedMarkets.indexOf(marketId) >= 0
                return (
                    <ContextMenuProvider 
                        id="market-context" 
                        renderTag="span"
                        key={marketId}
                    >
                        <li
                            id={marketId}
                            title={eventName}
                            onClick={
                                () => watchMarkets(marketId, setWatchedMarkets)
                            }
                            className={getBettedClass(betted, blocked)}
                        >
                            <i id={marketId} className={inplayClass}></i>
                            <span id={marketId} >{eventName}</span>
                        </li>
                    </ContextMenuProvider >   
                )
            })}
        </ul>
    )
}

const EventList = ({
    activeMarkets,
    eventType,
    setWatchedMarkets,
    watchedMarkets,
    activeBets,
    blockedMarkets
}) => {
    if(!activeMarkets) return null
    let length = activeMarkets.get(eventType).size
    let id = _.kebabCase(eventType)
    return (
        <div className="EventList" >
            <span
                className="EventHead"
                onClick={ () => $(`#${id}`).slideToggle() }
            >
                {eventType}
                <i className="length">{length}</i>
            </span>
            {
                length > 0 &&
                getEventList(
                    activeMarkets,
                    eventType,
                    setWatchedMarkets,
                    activeBets,
                    blockedMarkets
                )
            }
        </div>
    )
}

export default EventList

EventList.propTypes = {
    activeMarkets:          React.PropTypes.object.isRequired,
    eventType:              React.PropTypes.string.isRequired,
    setWatchedMarkets:      React.PropTypes.func.isRequired,
    watchedMarkets:         React.PropTypes.object.isRequired
}
