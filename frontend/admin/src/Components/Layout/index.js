import React from 'react'

import Header from 'Components/Header'

const Layout = ({children}) =>
    <div>
        <Header />
        <div id="app">{children}</div>
    </div>

export default Layout
