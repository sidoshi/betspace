import React from 'react'
import $ from 'jquery'

import './Modal.css'

class Modal extends React.Component {
    constructor(){
        super()
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.closeOverlay = this.closeOverlay.bind(this)
    }
    closeOverlay(event, closeModal){
        if(event.target === event.currentTarget){
            closeModal()
        }
    }
    handleKeyDown(e){
        if(e.keyCode === 27){
            this.props.closeModal()
        }
    }
    componentDidMount(){
        $(document.body).on('keydown', this.handleKeyDown)
    }
    componentWillUnmount(){
        $(document.body).off('keydown')
    }
    render(){
        let {closeModal,visible,children} = this.props
        let {closeOverlay} = this
        return (
            visible
            ?   (
                    <div
                        className="ModalOverlay"
                        onClick={(e) => closeOverlay(e,closeModal)}
                    >
                        <div className="ModalBody">
                            {children}
                        </div>
                    </div>
                )
            :   <div></div>
        )
    }
}




export default Modal
