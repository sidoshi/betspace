import React from 'react'
import classnames from 'classnames'

import socket from 'Lib/socket'

const getBetTypeClass = betType => classnames({
    back: betType === 'BACK',
    lay:  betType === 'LAY'
})

const Bet = ({
    id,
    activeBets
}) => {
    let bet = activeBets.getIn(['byId',id])
    let matched = bet.get('matched')
    let deleteButton = (
        <button onClick={() => socket.emit('CANCEL_BET', bet) }>
            delete
        </button>
    )
    return (
        <tr className={getBetTypeClass(bet.get('betType'))}>
            <td>{bet.get('runnerName')}</td>
            <td>{bet.get('rate')}</td>
            <td>{bet.get('stake')}</td>
            <td>{bet.get('profit')}</td>
            <td>{bet.get('exposure')}</td>
            <td>{bet.get('betType')}</td>
            <td>{bet.get('result')}</td>
            <td>{'' + matched}</td>
            <td>{!matched && deleteButton}</td>
        </tr>
    )
}

export default Bet
