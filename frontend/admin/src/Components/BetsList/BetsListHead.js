import React from 'react'

const BetsListHead = ({setFilter, filter}) =>
    <div>
        <h3>Bets</h3>
        <button onClick={() => setFilter( 'ALL')}>All</button>
        <button onClick={() => setFilter('MATCHED')}>Matched</button>
        <button onClick={() => setFilter('UNMATCHED')}>Unmatched</button>
    </div>

export default BetsListHead
