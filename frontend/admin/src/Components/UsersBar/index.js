import React from 'react'
import {connect} from 'react-redux'
import co from 'co'
import {bindActionCreators} from 'redux'

import request from 'Lib/request'
import UserTree from 'Components/UserTree'
import UserContextMenu from 'Components/UserContextMenu'
import UserAdder from 'Components/UserAdder'
import Withdrawer from 'Components/Withdrawer'
import Depositer from 'Components/Depositer'
import CreditInForm from 'Components/CreditInForm'
import CreditOutForm from 'Components/CreditOutForm'
import SelectedUserInfo from 'Components/SelectedUserInfo'
import * as UserActions from 'Actions/UserActions'
import jwtDecoded from 'Lib/jwtDecoded'
import normalizeUsers from 'Lib/normalizeUsers'
import PasswordChanger from 'Components/PasswordChanger'
import UserLimitAdder from 'Components/UserLimitAdder'
import './UsersBar.css'

class UsersBar extends React.Component{
    constructor(){
        super()
        this.state = {
            userAdderVisible: false,
            withdrawerVisible: false,
            depositerVisible: false,
            creditInVisible: false,
            creditOutVisible: false,
            passwordChangerVisible: false,
            creditInId: null,
            creditOutId: null,
            withdrawerId: null,
            depositerId: null,
            selectedUserId: jwtDecoded.userId,
            userLimitAdderVisible: false,
            limitAdderId: null,
            visible: false
        }
    }
    setSelectedUser = userId => {
        this.setState({
            selectedUserId: userId,
            visible: true
        })
    }
    openUserLimitAdder = userId => {
        this.setState({
            userLimitAdderVisible: true,
            limitAdderId: userId
        })
    }
    closeUserLimitAdder = () => {
        this.setState({
            userLimitAdderVisible: false
        })
    }
    toggleVisibility = () => {
        this.setState({
            visible: !this.state.visible
        })
    }
    closeUserAdder = () => {
        this.setState({
            userAdderVisible: false
        })
    }
    openUserAdder = () => {
        this.setState({
            userAdderVisible: true
        })
    }
    openPasswordChanger = () => {
        this.setState({
            passwordChangerVisible: true
        })
    }
    closePasswordChanger = () => {
        this.setState({
            passwordChangerVisible: false
        })
    }
    closeWithdrawer = () => {
        this.setState({
            withdrawerVisible: false
        })
    }
    openWithdrawer = (id) => {
        this.setState({
            withdrawerVisible: true,
            withdrawerId: id
        })
    }
    closeDepositer = () => {
        this.setState({
            depositerVisible: false
        })
    }
    openDepositer = (id) => {
        this.setState({
            depositerVisible: true,
            depositerId: id
        })
    }
    openCreditInForm = id => {
        this.setState({
            creditInVisible: true,
            creditInId: id
        })
    }
    openCreditOutForm = id => {
        this.setState({
            creditOutVisible: true,
            creditOutId: id
        })
    }
    closeCreditInForm = () => {
        this.setState({
            creditInVisible: false
        })
    }
    closeCreditOutForm = () => {
        this.setState({
            creditOutVisible: false
        })
    }
    componentDidMount(){
        let {setUsers, setBalanceSheet} = this.props
        co(function* (){
            let res = yield request.get('/api/users')
            let users = res.data
            users = normalizeUsers(users)
            setUsers(users)
            res = yield request.get('/api/users/balance-sheet')
            let balanceSheet = res.data
            let credit = balanceSheet.credit
            let debit = balanceSheet.debit
            setBalanceSheet({
                debit,
                credit
            })
        }).catch(console.log)
    }
    render(){
        let {users} = this.props
        let selfId = jwtDecoded.userId
        let selectedUserId = this.state.selectedUserId
        let limitAdderId = this.state.limitAdderId
        let limitAdderName = users.getIn([limitAdderId, 'username']) || ''
        let userLimit = users.getIn([limitAdderId, 'userLimit']) || {
            min: 0,
            max: Infinity
        }
        return (
            <div className="UsersBar">
                <UserTree 
                    userId={selfId} 
                    users={users} 
                    setSelectedUser={this.setSelectedUser}
                />
                <UserContextMenu
                    openDepositer={this.openDepositer}
                    openWithdrawer={this.openWithdrawer}
                    openCreditInForm={this.openCreditInForm}
                    openCreditOutForm={this.openCreditOutForm}
                    openUserAdder={this.openUserAdder}
                    openPasswordChanger={this.openPasswordChanger}
                    openUserLimitAdder={this.openUserLimitAdder}
                    users={users}
                    selfId={selfId}
                />
                <UserAdder
                    visible={this.state.userAdderVisible}
                    closeUserAdder={this.closeUserAdder}
                    self={users.get(selfId)}
                />
                <Withdrawer
                    visible={this.state.withdrawerVisible}
                    closeWithdrawer={this.closeWithdrawer}
                    users={users}
                    withdrawerId={this.state.withdrawerId}
                    self={users.get(selfId)}
                    balanceSheet={this.props.balanceSheet}
                />
                <PasswordChanger 
                    visible={this.state.passwordChangerVisible}
                    closePasswordChanger={this.closePasswordChanger}
                />
                <UserLimitAdder
                    visible={this.state.userLimitAdderVisible}
                    closeLimitAdder={this.closeUserLimitAdder}
                    userId={this.state.limitAdderId}
                    name={limitAdderName}
                    limit={userLimit}
                />
                <Depositer
                    visible={this.state.depositerVisible}
                    closeDepositer={this.closeDepositer}
                    users={users}
                    depositerId={this.state.depositerId}
                    self={users.get(selfId)}
                    balanceSheet={this.props.balanceSheet}
                />
                <CreditInForm 
                    visible={this.state.creditInVisible}
                    closeCreditInForm={this.closeCreditInForm}
                    users={users}
                    creditInId={this.state.creditInId}
                    self={users.get(selfId)}
                />
                <CreditOutForm 
                    visible={this.state.creditOutVisible}
                    closeCreditOutForm={this.closeCreditOutForm}
                    users={users}
                    creditOutId={this.state.creditOutId}
                    self={users.get(selfId)}
                />
                <SelectedUserInfo 
                    user={users.get(selectedUserId)} 
                    visible={this.state.visible}
                    toggleVisibility={this.toggleVisibility}
                />
            </div>
        )
    }
}

const mapStateToProps = store => ({
    users:  store.Users.get('users'),
    balanceSheet: store.Users.get('balanceSheet')
})
const mapDispatchToProps = dispatch =>
    bindActionCreators(UserActions,dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(UsersBar)
