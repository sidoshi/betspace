import React from 'react'

import Modal from 'Components/Modal'

import PasswordChangerForm from './PasswordChangerForm'
import './PasswordChanger.css'

const PasswordChanger = ({ visible, closePasswordChanger }) =>
    <Modal closeModal={closePasswordChanger} visible={visible}>
        <div className="PasswordChanger">
            <PasswordChangerForm
                closePasswordChanger={closePasswordChanger} 
                self={self} 
            />
        </div>
    </Modal>


export default PasswordChanger
