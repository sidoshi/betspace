import React from 'react'
import isAlphanumeric from 'validator/lib/isAlphanumeric'
import { Textfield, Button } from 'react-mdl'
import alertify from 'alertify.js'

import request from 'Lib/request'

class PasswordChangerForm extends React.Component {
    constructor() {
        super()
        this.state = {
            oldPassword: '',
            password: ''
        }
    }
    handleChange = event => {
        const target = event.target
        const value = target.type === 'checkbox'
            ? target.checked
            : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
    }
    changePassword = async () => {
        let error = alertify.error
        if(!this.state.oldPassword)
            return error('Please enter old password')
        if(!isAlphanumeric(this.state.password))
            return error('New Password should have letters and numbers')
        if(this.state.password.length < 6)
            return error('New password should contain atleast 6 characters')
        try{
            let res = await request.post('/api/users/change-password', {
                oldPassword: this.state.oldPassword,
                password: this.state.password
            })
            alertify.success(res.data)
            this.props.closePasswordChanger()
        }catch(err){
            error(err.response.data)
        }
    }
    render() {
        let { handleChange } = this
        return (
            <div>
                <Textfield
                    onChange={handleChange}
                    label="Old Password"
                    floatingLabel
                    value={this.state.oldPassword}
                    name="oldPassword"
                    type="password"
                /><br />
                <Textfield
                    onChange={handleChange}
                    label="New Password"
                    floatingLabel
                    value={this.state.password}
                    name="password"
                    type="password"
                /><br />
                <Button onClick={this.changePassword} raised colored>
                    Change
                </Button>
                <Button onClick={this.props.closePasswordChanger} raised colored>
                    Cancel
                </Button>
            </div>
        )
    }
}

export default PasswordChangerForm
