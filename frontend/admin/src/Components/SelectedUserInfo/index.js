import React from 'react'
import classnames from 'classnames'

import UserBody from './UserBody'
import './SelectedUserInfo.css'

const getClass = (visible) => classnames({
    'SelectedUserInfo': true,
    'visible': visible
})

class SelectedUserInfo extends React.Component {
    render(){
        let {user, visible, toggleVisibility} = this.props
        return (
            <div className={getClass(visible)}> 
                <div 
                    onClick={toggleVisibility}
                    className="userHeader"
                >
                    <span>User Info</span>
                    <span className="info mdi mdi-information"></span>
                </div>
                <UserBody user={user} />
            </div>
        )
    }
}

export default SelectedUserInfo