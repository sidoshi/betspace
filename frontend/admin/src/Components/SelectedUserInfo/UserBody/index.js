import React from 'react'

const UserBody = ({user}) => {
    if (!user) {
        return (
            <div className="userBody"></div>
        )
    }
    let userLimit = user.get('userLimit') || {}
    userLimit = (userLimit.toJS && userLimit.toJS()) || {}
    let min = userLimit.min || 0
    let max = userLimit.max || 'Infinite'
    let limit = (
        <tr>
            <td>Limit</td>
            <td>{min} - {max}</td>
        </tr>
    )
    return (
        <div className="userBody">
            <table>
                <tbody>
                    <tr>
                        <td>Username</td>
                        <td>{user.get('username')}</td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>{user.get('name') || '-'}</td>
                    </tr>
                    <tr>
                        <td>User Type</td>
                        <td>{user.get('userType')}</td>
                    </tr>
                    {
                        user.get('userType') === 'CLIENT' 
                            ? limit 
                            : null
                    }
                    <tr>
                        <td>Balance</td>
                        <td>{user.get('balance')}</td>
                    </tr>
                    <tr>
                        <td>Credit</td>
                        <td>{user.get('credit')}</td>
                    </tr>
                    <tr>
                        <td>Ratio</td>
                        <td>{user.get('availableShare')}%</td>
                    </tr>
                    <tr>
                        <td>Profit/Loss</td>
                        <td>{user.get('profitAndLoss')}</td>
                    </tr>
                    <tr>
                        <td>Credit Given</td>
                        <td>{user.get('childCredit')}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default UserBody