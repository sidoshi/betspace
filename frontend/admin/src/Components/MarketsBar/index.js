import React from 'react'
import classnames from 'classnames'

import EventList from 'Components/EventList'
import MarketContextMenu from 'Components/MarketContextMenu'
import LimitAdder from 'Components/LimitAdder'
import './MarketsBar.css'
import config from 'Lib/config'

let {eventTypes} = config

const getMarketsBarClass = modalVisible => classnames({
    MarketsBar: true,
    modalVisible: modalVisible
})

class MarketsBar extends React.Component {
    constructor(){
        super()
        this.state = {
            limitAdderVisible: false,
            modalVisible: false,
            limitAdderMarketId: ''
        }
    }
    openLimitAdder = marketId => {
        this.setState({
            limitAdderVisible: true,
            limitAdderMarketId: marketId,
            modalVisible: true
        })
    }
    closeLimitAdder = () => {
        this.setState({
            limitAdderVisible: false,
            modalVisible: false
        })
    }
    render(){
        let {
            activeMarkets,
            setWatchedMarkets,
            watchedMarkets,
            activeBets,
            blockedMarkets,
            limitedMarkets
        } = this.props
        return (
            <div className={getMarketsBarClass(this.state.modalVisible)}>
                {eventTypes.map(eventType => (
                    <EventList
                        key={eventType}
                        eventType={eventType}
                        activeMarkets={activeMarkets}
                        setWatchedMarkets={setWatchedMarkets}
                        watchedMarkets={watchedMarkets}
                        activeBets={activeBets}
                        blockedMarkets={blockedMarkets}
                    />
                ))}
                <MarketContextMenu 
                    openLimitAdder={this.openLimitAdder}
                />
                <LimitAdder 
                    visible={this.state.limitAdderVisible}
                    marketId={this.state.limitAdderMarketId}
                    closeLimitAdder={this.closeLimitAdder}
                    limit={limitedMarkets.get(''+this.state.limitAdderMarketId)}
                    market={activeMarkets.get(this.state.limitAdderMarketId)}
                />
            </div>
        )
    }
}

export default MarketsBar

MarketsBar.propTypes = {
    activeMarkets: React.PropTypes.object.isRequired,
    watchedMarkets: React.PropTypes.object.isRequired,
    setWatchedMarkets: React.PropTypes.func.isRequired
}
