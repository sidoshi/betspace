import React from 'react'
import classnames from 'classnames'

import jwtDecoded from 'Lib/jwtDecoded'
import './MarketBooks.css'

const getProjectionClass= projection => classnames({
    selfProjection: true,
    profit: projection > 0,
    loss: projection < 0
})


const MarketBooksHead = ({runners, projection, market}) => {
    return (
        <tr>
            <td></td>
            {market.get('sortedRunners').map( runnerId =>
                <td key={runnerId}>
                    {runners.getIn([''+runnerId, 'name'])}
                </td>
            )}
        </tr>
    )
}

const SelfBooks = ({projection, runners, market}) => {
    let self = projection.get('self')
    return (
        <tr>
            <td className="username">Self</td>
            {market.get('sortedRunners').map( runnerId =>{
                runnerId = ''+runnerId
                let projection = self.get(runnerId)
                let projectionClass = getProjectionClass(+projection)
                return(
                    <td key={runnerId} className={projectionClass}>
                        {projection}
                    </td>
                )
            })}
        </tr>
    )
}

const ParentBooks = ({projection, runners, market}) => {
    let parent = projection.get('parent')
    return (
        <tr>
            <td className="username">Parent</td>
            {market.get('sortedRunners').map( runnerId =>{
                runnerId = ''+runnerId
                let projection = parent.get(runnerId)
                let projectionClass = getProjectionClass(+projection)
                return(
                    <td key={runnerId} className={projectionClass}>
                        {projection}
                    </td>
                )
            })}
        </tr>
    )
}

const ChildBooks = ({runners, childId, projection, users, market}) => {
    return (
        <tr>
            <td className="username">
                {users.getIn([''+childId, 'username'])}
            </td>
            {market.get('sortedRunners').map( runnerId => {
                runnerId = ''+runnerId
                let currentProjection = projection.get(runnerId)
                let projectionClass = getProjectionClass(+currentProjection)
                return (
                    <td key={runnerId} className={projectionClass}>
                        {currentProjection}
                    </td>
                )
            })}
        </tr>
    )
}

const MarketBooks = ({projection, runners, users, market}) => {
    if(!projection) return <div></div>
    let children = projection.get('children')
    let self = users.get(jwtDecoded.userId)
    let hasParent = self.get('directParent')
    return (
        <div className="MarketBooks">
            <table>
                <thead>
                    <MarketBooksHead
                        runners={runners}
                        projection={projection}
                        market={market}
                    />
                </thead>
                <tbody>
                    {hasParent && (<ParentBooks
                        runners={runners}
                        projection={projection}
                        market={market}
                    />)}
                    <SelfBooks
                        runners={runners}
                        projection={projection}
                        market={market}
                    />
                    {children.entrySeq().map( ([childId, projection]) =>
                        <ChildBooks
                            runners={runners}
                            childId={childId}
                            projection={projection}
                            users={users}
                            key={childId}
                            market={market}
                        />
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default MarketBooks
