import React from 'react'

import './Logo.css'
import LogoImage from './Logo.svg'

const Logo = () =>
    <div className="Logo">
        <a>
            <span>
                <img src={LogoImage} className="LogoImage" alt="Logo"/>
                <span className="LogoText">Betspace</span>
            </span>
        </a>
    </div>

export default Logo
