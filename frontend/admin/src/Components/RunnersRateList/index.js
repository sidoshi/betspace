import React from 'react'

import RunnersRateListHead from './RunnersRateListHead'
import RunnerRate from './RunnerRate'
import './RunnersRateList.css'

class RunnersRateList extends React.Component {
    render(){
        let {runners, marketId, projection, market} = this.props
        return (
            <div className="RunnersRateList">
                <table>
                    <RunnersRateListHead />
                    <tbody>
                        {market.get('sortedRunners').map( runnerId =>
                            <RunnerRate
                                runner={runners.get(''+runnerId)}
                                marketId={marketId}
                                key={runnerId}
                                projection={projection}
                            />
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default RunnersRateList
