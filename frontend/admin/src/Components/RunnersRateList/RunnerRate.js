import React from 'react'
import classnames from 'classnames'
import {Chip} from 'react-mdl'

import RateCell from './RateCell'

const getSelfProjectionClass= projection => classnames({
    selfProjection: true,
    profit: projection > 0,
    loss: projection < 0
})

const RunnerRate = (props) => {
    let {runner, projection} = props
    let id = runner.get('selectionId')
    let selfProjection
    if(projection){
        selfProjection = projection.getIn(['self', ''+id])
    }
    selfProjection = +selfProjection || null
    let projectionClass = getSelfProjectionClass(selfProjection)
    return (
        <tr key={runner.get('selectionId')}>
            <td className="name">{runner.get('name')}</td>
            <td className={projectionClass}>
                {selfProjection ? <Chip>{selfProjection}</Chip> : null}
            </td>
            <RateCell type="availableToBack" element={2} {...props}/>
            <RateCell type="availableToBack" element={1} {...props}/>
            <RateCell type="availableToBack" element={0} {...props}/>
            <RateCell type="availableToLay" element={0} {...props}/>
            <RateCell type="availableToLay" element={1} {...props}/>
            <RateCell type="availableToLay" element={2} {...props}/>
        </tr>
    )
}


export default RunnerRate
