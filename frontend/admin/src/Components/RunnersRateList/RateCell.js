import React from 'react'
import classnames from 'classnames'

import { toFixed } from 'Lib/numberUtils'

const rateClass = valueType => classnames({
    price: valueType === 'price',
    size: valueType === 'size',
    rate: true
})

const cellClass = (element, betType) => classnames({
    RateCell: true,
    main: element === 0,
    back: betType === 'availableToBack',
    lay: betType === 'availableToLay',
})


const RateCell = ({
    type,
    element,
    runner,
    marketId
}) => {
    let price = runner.getIn([type, element, 'price']) || '-'
    let size = runner.getIn([type, element, 'size'])
    size = toFixed(size) || '-'
    let priceClass = rateClass('price')
    let sizeClass = rateClass('size')
    return(
        <td
            className={cellClass(element, type)}
        >
            <p className={priceClass}>{price}</p>
            <p className={sizeClass}>{size}</p>
        </td>
    )
}

export default RateCell
