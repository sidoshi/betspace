import React from 'react'
import co from 'co'
import {Textfield, Button} from 'react-mdl'
import alertify from 'alertify.js'

import request from 'Lib/request'

class WithdrawerForm extends React.Component {
    constructor(){
        super()
        this.state = {
            amount: ''
        }
    }
    handleChange = event => {
        const target = event.target
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
    }
    withdraw = () => {
        let userId = this.props.user.get('userId')
        let amount = this.state.amount
        let closeWithdrawer = this.props.closeWithdrawer
        let bettingBalance = this.props.user.get('bettingBalance')
        let userType = this.props.user.get('userType')
        let error = alertify.error
        if (userType === 'CLIENT' && bettingBalance < amount){
            error('User dosent have enough balance to withdraw that amount')
            return
        }
        if (amount > this.props.credit){
            error('Can only withdraw credited amount')
            return
        }
        co(function*(){
            let res = yield request.post('/api/users/withdraw',{
                userId: userId,
                amount: amount
            })
            if(res.status === 200) alertify.success('Withdrawn')
            else alertify.error('Failed to withdraw')
            closeWithdrawer()
        }).catch(console.log)
    }
    render(){
        let self = this.props.self
        return(
            <div className="WithdrawerForm">
                <Textfield
                    disabled
                    label="Username"
                    floatingLabel
                    value={this.props.user.get('username')}
                    className="Label"
                /><br />
                <Textfield
                    disabled
                    label={this.props.user.userType === 'CLIENT'
                        ? 'Betting Balance'
                        : 'Balance'
                    }
                    floatingLabel
                    value={this.props.user.userType === 'CLIENT'
                        ? this.props.user.get('bettingBalance')
                        : this.props.user.get('balance')
                    }
                    className="Label"
                /><br />
                <Textfield
                    disabled
                    label="Credit"
                    floatingLabel
                    value={this.props.credit}
                    className="Label"
                /><br />
                <Textfield
                    onChange={this.handleChange}
                    label="Amount"
                    floatingLabel
                    value={this.state.amount}
                    name="amount"
                    pattern="-?[0-9]*(\.[0-9]+)?"
                    error="Input is not a number!"
                /><br />
                <Textfield
                    disabled
                    label="Own Balance"
                    floatingLabel
                    value={+self.get('balance') + +this.state.amount}
                /><br />
                <Button onClick={this.withdraw} raised colored>Withdraw</Button>
                <Button onClick={this.props.closeWithdrawer} raised colored>
                    Cancel
                </Button>
            </div>
        )
    }
}

export default WithdrawerForm