import React from 'react'

import Modal from 'Components/Modal'
import WithdrawerForm from './WithdrawerForm'
import './WithdrawerForm.css'

const Withdrawer = ({
    visible, 
    closeWithdrawer, 
    users, 
    withdrawerId, 
    self,
    balanceSheet
}) =>
    <Modal closeModal={closeWithdrawer} visible={visible}>
        <div className="Withdrawer">
            <WithdrawerForm
                closeWithdrawer={closeWithdrawer}
                user={users.get(withdrawerId)}
                self={self}
                credit={balanceSheet.getIn(['credit', withdrawerId]) || 0}
            />
        </div>
    </Modal>

export default Withdrawer
