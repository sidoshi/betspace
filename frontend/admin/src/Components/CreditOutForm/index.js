import React from 'react'

import Modal from 'Components/Modal'
import Form from './Form'
import './CreditOutForm.css'

const CreditOutForm = ({ 
    visible, 
    closeCreditOutForm, 
    users, 
    creditOutId, 
    self 
}) =>
    <Modal closeModal={closeCreditOutForm} visible={visible}>
        <div className="CreditOutForm">
            <Form
                closeCreditOutForm={closeCreditOutForm}
                user={users.get(creditOutId)}
                self={self}
            />
        </div>
    </Modal>

export default CreditOutForm