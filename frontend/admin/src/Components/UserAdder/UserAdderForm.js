import React from 'react'
import co from 'co'
import isAlphanumeric from 'validator/lib/isAlphanumeric'
import {Textfield, Radio, RadioGroup, Button} from 'react-mdl'
import alertify from 'alertify.js'

import request from 'Lib/request'
import config from 'Lib/config'
import jwtDecoded from 'Lib/jwtDecoded'

let validChilds = config.validChilds[jwtDecoded.userType]

class UserAdderForm extends React.Component{
    constructor(){
        super()
        this.state = {
            username: '',
            password: '',
            credit: 0,
            userType: '',
            availableShare: 0,
            name: ''
        }
    }
    handleChange = event => {
        const target = event.target
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
    }
    addUser = () => {
        let state = this.state
        let error = alertify.error
        let closeUserAdder = this.props.closeUserAdder
        let self = this.props.self
        if(state.username.length < 3) return error('username too short')
        if(!isAlphanumeric(state.username)) return error('Invalid Username')
        if(!isAlphanumeric(state.password)) return error('Invalid Password')
        if(validChilds.indexOf(state.userType) < 0)
            return error('Invalid User Type')
        if(isNaN(Number(state.credit))) return error('Invalid Credit')
        if(state.credit > self.get('balance')) 
            return error('Insufficient Balance')
        if(state.availableShare > self.get('availableShare'))
            return error('Insufficient Ratio')
        co(function*() {
            try{
                let res = yield request.post('/api/users/create',{
                    username: state.username,
                    password: state.password,
                    userType: state.userType,
                    credit: state.credit,
                    availableShare: state.availableShare,
                    name: state.name
                })
                if(res.status === 200) alertify.success('User Created')
                else alertify.error('Failed to create user')
                closeUserAdder()
            }catch(err){
                console.log(err.response.data)
            }
        })
    }
    render(){
        let {handleChange} = this
        let {self} = this.props
        let smallInput = {
            width: '40%',
            marginRight: '5px'
        }
        return(
            <div>
                <Textfield
                    onChange={handleChange}
                    label="Username"
                    floatingLabel
                    value={this.state.username}
                    name="username"
                /><br />
                <Textfield
                    onChange={handleChange}
                    label="Name"
                    floatingLabel
                    value={this.state.name}
                    name="name"
                /><br />
                <Textfield
                    onChange={handleChange}
                    label="Password"
                    floatingLabel
                    value={this.state.password}
                    name="password"
                /><br />
                <Textfield
                    onChange={handleChange}
                    label="Credit"
                    floatingLabel
                    value={this.state.balance}
                    name="credit"
                    pattern="-?[0-9]*(\.[0-9]+)?"
                    error="Input is not a number!"
                /><br />
                <Textfield
                    disabled={this.state.userType === 'CLIENT'}
                    onChange={handleChange}
                    label="Ratio"
                    floatingLabel
                    value={this.state.availableShare}
                    name="availableShare"
                /><br />
                <Textfield
                    disabled
                    label="Own Balance"
                    floatingLabel
                    value={+self.get('balance') - +this.state.credit}
                    style={smallInput}
                />
                <Textfield
                    disabled
                    label="Own Ratio"
                    floatingLabel
                    value={
                        +self.get('availableShare') - +this.state.availableShare
                    }
                    style={smallInput}
                /><br />
                <RadioGroup 
                    name="userType" 
                    onChange={handleChange} 
                    value={this.state.userType}
                >
                    {validChilds.map( childType =>
                        <Radio key={childType} value={childType} ripple>
                            {childType}
                        </Radio>
                    )}
                </RadioGroup><br />
                <Button onClick={this.addUser} raised colored>
                    Add
                </Button>
                <Button onClick={this.props.closeUserAdder} raised colored>
                    Cancel
                </Button>
            </div>
        )
    }
}

export default UserAdderForm
