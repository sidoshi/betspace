import React from 'react'

import Modal from 'Components/Modal'

import UserAdderForm from './UserAdderForm'
import './UserAdder.css'

const UserAdder = ({visible, closeUserAdder, self}) =>
    <Modal closeModal={closeUserAdder} visible={visible}>
        <div className="UserAdder">
            <UserAdderForm closeUserAdder={closeUserAdder} self={self}/>
        </div>
    </Modal>


export default UserAdder
