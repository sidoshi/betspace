import React from 'react'

import Logo from 'Components/Logo'
import UserInfo from 'Components/UserInfo'
import './Header.css'

const Layout = () =>
    <div className="Header">
        <Logo />
        <UserInfo />
    </div>

export default Layout
