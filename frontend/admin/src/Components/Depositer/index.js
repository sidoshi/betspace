import React from 'react'

import Modal from 'Components/Modal'
import DepositerForm from './DepositerForm'
import './DepositerForm.css'

const Depositer = ({
    visible, 
    closeDepositer, 
    users, 
    depositerId, 
    self,
    balanceSheet
}) =>
    <Modal closeModal={closeDepositer} visible={visible}>
        <div className="Depositer">
            <DepositerForm
                closeDepositer={closeDepositer}
                user={users.get(depositerId)}
                self={self}
                debit={balanceSheet.getIn(['debit', depositerId]) || 0}
            />
        </div>
    </Modal>

export default Depositer
