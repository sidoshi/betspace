import React from 'react'
import co from 'co'
import {Textfield, Button} from 'react-mdl'
import alertify from 'alertify.js'

import request from 'Lib/request'

class DepositerForm extends React.Component {
    constructor(){
        super()
        this.state = {
            amount: ''
        }
    }
    handleChange = event => {
        const target = event.target
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        const name = target.name
        this.setState({
            [name]: value
        })
    }
    deposit = () => {
        let userId = this.props.user.get('userId')
        let amount = this.state.amount
        let self = this.props.self
        let error = alertify.error
        if(amount > self.get('balance')){
            error('You dont have enough balance')
            return
        }
        if (amount > this.props.debit) {
            error('Can only deposit debited amount')
            return
        }
        let closeDepositer = this.props.closeDepositer
        co(function*(){
            let res = yield request.post('/api/users/deposit',{
                userId: userId,
                amount: amount
            })
            if(res.status === 200) alertify.success('Deposited')
            else alertify.error('Failed to deposit')
            closeDepositer()
        }).catch(console.log)
    }
    render(){
        let self = this.props.self
        return(
            <div className="DepositerForm">
                <Textfield
                    disabled
                    label="Username"
                    floatingLabel
                    value={this.props.user.get('username')}
                    className="Label"
                /><br />
                <Textfield
                    disabled
                    label={this.props.user.userType === 'CLIENT'
                        ? 'Betting Balance'
                        : 'Balance'
                    }
                    floatingLabel
                    value={this.props.user.userType === 'CLIENT'
                        ? this.props.user.get('bettingBalance')
                        : this.props.user.get('balance')
                    }
                    className="Label"
                /><br />
                <Textfield
                    disabled
                    label="Debit"
                    floatingLabel
                    value={this.props.debit}
                    className="Label"
                /><br />
                <Textfield
                    onChange={this.handleChange}
                    label="Amount"
                    floatingLabel
                    value={this.state.amount}
                    name="amount"
                    pattern="-?[0-9]*(\.[0-9]+)?"
                    error="Input is not a number!"
                /><br />
                <Textfield
                    disabled
                    label="Own Balance"
                    floatingLabel
                    value={+self.get('balance') - +this.state.amount}
                /><br />
                <Button onClick={this.deposit} raised colored>
                    Deposit
                </Button>
                <Button onClick={this.props.closeDepositer} raised colored>
                    Cancel
                </Button>
            </div>
        )
    }
}

export default DepositerForm