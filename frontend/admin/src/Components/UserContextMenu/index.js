import React from 'react'
import { ContextMenu, Item, Separator } from 'react-contexify'
import 'react-contexify/dist/ReactContexify.min.css'
import co from 'co'
import alertify from 'alertify.js'

import request from 'Lib/request'

const blockUser = co.wrap(function*(id) {
    try{
        let res = yield request.post('/api/users/block',{
            userId: id
        })
        res.status === 200 && alertify.log('User Blocked')
    }catch(err){
        console.log(err.response.data)
    }
})

const unblockUser = co.wrap(function*(id) {
    try{
        let res = yield request.post('/api/users/unblock',{
            userId: id
        })
        res.status === 200 && alertify.log('User Unblocked')
    }catch(err){
        console.log(err.response.data)
    }
})


class UserContextMenu extends React.Component {
    onClick = (item,target) => {
        let {users, selfId} = this.props
        let self = users.get(selfId)
        let children = self.get('children')
        let error = alertify.error
        switch (item.props.label) {
            case 'Add User':
                this.props.openUserAdder()
                break
            case 'Block User':
                if(children.indexOf(target.id) < 0){
                    error('You cant block this user')
                    return
                }
                return blockUser(target.id)
            case 'Unblock User':
                if(children.indexOf(target.id) < 0){
                    error('You cant unblock this user')
                    return
                }
                return unblockUser(target.id)
            case 'Change Password':
                if (target.id !== selfId) {
                    error('You can only change your own password')
                    return
                }
                return this.props.openPasswordChanger()
            case 'Deposit':
                if(children.indexOf(target.id) < 0){
                    error('You cant account with this user')
                    return
                }
                return this.props.openDepositer(target.id)
            case 'Withdraw':
                if(children.indexOf(target.id) < 0){
                    error('You cant account with this user')
                    return
                }
                return this.props.openWithdrawer(target.id)
            case 'Credit-In':
                if(children.indexOf(target.id) < 0){
                    error('You cant account with this user')
                    return
                }
                return this.props.openCreditInForm(target.id)
            case 'Credit-Out':
                if(children.indexOf(target.id) < 0){
                    error('You cant account with this user')
                    return
                }
                return this.props.openUserLimitAdder(target.id)
            case 'Add Limit':
                let child = users.get(target.id)
                console.log(child.toJS())
                let childType = child.get('userType')
                if((children.indexOf(target.id) < 0) || childType !== 'CLIENT'){
                    error('You can only limit your direct Client')
                    return
                }
                return this.props.openUserLimitAdder(target.id)
            default:
                return
        }
    }
    render(){
        let {onClick} = this
        return (
            <ContextMenu id='user-context' animation='popIn' theme='dark'>
                <Item label="Add User" icon="mdi mdi-account-plus" onClick={onClick} />
                <Item label="Block User" icon="mdi mdi-account-off" onClick={onClick}/>
                <Item label="Unblock User" icon="mdi mdi-account-check" onClick={onClick}/>
                <Item label="Change Password" icon="mdi mdi-lock" onClick={onClick} />
                <Separator/>
                <Item label="Deposit" icon="mdi mdi-cash-usd" onClick={onClick}/>
                <Item label="Withdraw" icon="mdi mdi-cash-usd" onClick={onClick}/>
                <Item label="Credit-In" icon="mdi mdi-cash-usd" onClick={onClick}/>
                <Item label="Credit-Out" icon="mdi mdi-cash-usd" onClick={onClick}/>
                <Separator/>
                <Item label="Add Limit" icon="mdi mdi-database-plus" onClick={onClick}/>
            </ContextMenu>
        )
    }
}

export default UserContextMenu
