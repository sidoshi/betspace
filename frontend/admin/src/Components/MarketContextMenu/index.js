import React from 'react'
import { ContextMenu, Item, Separator } from 'react-contexify'
import alertify from 'alertify.js'

import request from 'Lib/request'
import jwtDecoded from 'Lib/jwtDecoded'

class MarketContextMenu extends React.Component {
    onClick = async (item, target) => {
        let res
        try{
            switch (item.props.label) {
                case 'Block Market':
                    res = await request.post('api/betfair/block-market', {
                        marketId: target.id
                    })
                    alertify.success(res.data)
                    return
                case 'Unblock Market':
                    res = await request.post('api/betfair/unblock-market',{
                        marketId: target.id
                    })
                    alertify.success(res.data)
                    return
                case 'Add Limit':
                    if (jwtDecoded.userType !== 'MAIN ADMIN')
                        return alertify.error('You can not add limits')
                    return this.props.openLimitAdder(target.id)
                default:
                    return
            }
        }catch(err){
            alertify.error(err.response.data)
        }
    }
    render(){
        let {onClick} = this
        return (
            <ContextMenu id="market-context" animation="popIn" theme='dark'>
                <Item label="Block Market" icon="mdi mdi-lock-outline" onClick={onClick} />
                <Item label="Unblock Market" icon="mdi mdi-lock-open-outline" onClick={onClick}/>
                <Separator />
                <Item label="Add Limit" icon="mdi mdi-database-plus" onClick={onClick} />
            </ContextMenu >
        )
    }
}

export default MarketContextMenu