import React from 'react'
import {Button} from 'react-mdl'
import {Link} from 'react-router'
import classnames from 'classnames'

import request from 'Lib/request'
import config from 'Lib/config'
import './UserInfo.css'
import socket from 'Lib/socket'


class UserInfo extends React.Component {
     constructor(){
        super()
        this.state = {
            connected: socket.connected
        }
    }
    handleSocketConnection = () => {
        socket.on('disconnect', () => {
            this.setState({
                connected: false,
            })
        })
        socket.on('connect', () => {
            this.setState({
                connected: true,
            })
        })  
    }
    componentDidMount(){
        this.setState({
            connected: socket.connected
        })
        this.handleSocketConnection()
    }
    logout(){
        request.post('/logout')
        .then( res => {
            let {url,port} = config
            window.location = `${url}:${port}`
        })
        .catch( err => console.log(err))
    }
    getConnectedClass = connected => {
        return classnames({
            mdi: true,
            'mdi-lan-connect': connected,
            'mdi-lan-disconnect': !connected,
            connected: connected,
            connection: true,
        })
    }
    render(){
        let connected = this.state.connected
        let connectedClass = this.getConnectedClass(connected)
        return (
            <div className="UserInfo">
                <i className={connectedClass} />
                <Link to="/" >HOME</Link>
                <Link to="/account" >ACCOUNT</Link>
                <Button onClick={this.logout} accent raised ripple>
                    <i className="mdi mdi-logout"></i>
                    logout
                </Button>
            </div>
        )
    }
}

export default UserInfo