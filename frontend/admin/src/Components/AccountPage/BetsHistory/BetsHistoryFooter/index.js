import React from 'react'

import request from 'Lib/request'

const addBets = async (addAllBets, count, length) => {
    if (length >= count) return
    try {
        let res = await request.post('/api/betfair/all-bets', {
            skip: length
        })
        let newBets = res.data
        addAllBets(newBets)
    } catch (err) {
        console.log(err)
    }
}

const BetsHistoryFooter = ({count, length, addAllBets}) => {
    return (
        <div className="BetsHistoryFooter">
            <button 
                disabled={length >= count}
                onClick={() => addBets(addAllBets, count, length)}
            >
                Show more bets
            </button>
        </div>
    )
}

export default BetsHistoryFooter