import React from 'react'

const BetsHistoryPanel = ({count, length}) => {
    return (
        <div className="BetsHistoryPanel">
            <p>Showing {length} of {count} bets</p>
        </div>
    )
}

export default BetsHistoryPanel