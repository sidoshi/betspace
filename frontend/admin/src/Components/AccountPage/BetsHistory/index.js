import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { Table, Column, Cell } from 'fixed-data-table'
import $ from 'jquery'
import 'fixed-data-table/dist/fixed-data-table.min.css'

import {addAllBets} from 'Actions/BetsActions'
import BetsHistoryPanel from './BetsHistoryPanel'
import BetsHistoryFooter from './BetsHistoryFooter'
import './BetsHistory.css'

const TextCell = ({ rowIndex, field, data, ...props }) => 
    <Cell {...props}>
        {''+data.getIn([rowIndex, field])}
    </Cell>

const DateCell = ({ rowIndex, field, data, ...props }) =>
    <Cell {...props}>
        {new Date(data.getIn([rowIndex, field])).toLocaleString()}
    </Cell>

class BetsHistory extends React.Component {
    render(){
        let width = Math.min($(window).width(), $(document).width()) - 10
        let height = Math.min($(window).height(), $(document).height()) - 205
        return (
            <div className="BetsHistory" >
                <BetsHistoryPanel 
                    count={this.props.allBetsCount}
                    length={this.props.allBets.size}
                />
                <div className="TableCenter">
                    <Table
                        rowsCount={this.props.allBets.size}
                        rowHeight={50}
                        headerHeight={50}
                        width={width}
                        height={height}
                    >
                        <Column
                            header={<Cell>Market</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="marketName"
                                />
                            }
                            width={209}
                            align="center"
                        />
                        <Column
                            header={<Cell>Sport</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="marketType"
                                />
                            }
                            width={80}
                            align="center"
                        />
                        <Column
                            header={<Cell>Rate</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="rate"
                                />
                            }
                            width={60}
                            align="center"
                        />
                        <Column
                            header={<Cell>Stake</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="stake"
                                />
                            }
                            width={80}
                            align="center"
                        />
                        <Column
                            header={<Cell>Profit</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="profit"
                                />
                            }
                            width={80}
                            align="center"
                        />
                        <Column
                            header={<Cell>Exposure</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="exposure"
                                />
                            }
                            width={80}
                            align="center"
                        />
                        <Column
                            header={<Cell>Type</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="betType"
                                />
                            }
                            width={60}
                            align="center"
                        />
                        <Column
                            header={<Cell>Matched</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="matched"
                                />
                            }
                            width={70}
                            align="center"
                        />
                        <Column
                            header={<Cell>Runner</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="runnerName"
                                />
                            }
                            width={70}
                            align="center"
                        />
                        <Column
                            header={<Cell>Runner Status</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="runnerStatus"
                                />
                            }
                            width={70}
                            align="center"
                        />
                        <Column
                            header={<Cell>Result</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="result"
                                />
                            }
                            width={80}
                            align="center"
                        />
                        <Column
                            header={<Cell>User Type</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="userType"
                                />
                            }
                            width={70}
                            align="center"
                        />
                        <Column
                            header={<Cell>User</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.allBets}
                                    field="username"
                                />
                            }
                            width={80}
                            align="center"
                        />
                        <Column
                            header={<Cell>Date</Cell>}
                            cell={
                                <DateCell
                                    data={this.props.allBets}
                                    field="createdAt"
                                />
                            }
                            width={150}
                            align="center"
                        />
                </Table>
                </div>
                <BetsHistoryFooter 
                    count={this.props.allBetsCount}
                    length={this.props.allBets.size}
                    addAllBets={this.props.addAllBets}
                />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => 
    bindActionCreators({
        addAllBets
    }, dispatch)

const mapStateToProps = store => ({
    allBets: store.Bets.get('allBets')
})

export default connect(mapStateToProps, mapDispatchToProps)(BetsHistory)