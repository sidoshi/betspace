import React from 'react'
import { Table, Column, Cell } from 'fixed-data-table'
import {connect} from 'react-redux'
import $ from 'jquery'

import './BalanceSheet.css'
import jwtDecoded from 'Lib/jwtDecoded'

const TextCell = ({ rowIndex, field, data, ...props }) => 
    <Cell {...props}>
        {'' + data[rowIndex][field]}
    </Cell>

const NameCell = ({ rowIndex, field, data, users, ...props }) => {
    let userId = data[rowIndex][field]
    if (userId === jwtDecoded.userId) return (
        <Cell {...props}>
            Self
        </Cell> 
    )
    return (
        <Cell {...props}>
            {'' + users.getIn([userId, 'username'])}
        </Cell>
    )
}

const BalanceTable = ({data, type, users}) => {
    let width = Math.min(
        $(window).width() / 2, $(document).width() / 2
        ) - 130
    let height = Math.min($(window).height(), $(document).height()) - 205
    return (
        <Table
            rowsCount={data.length}
            rowHeight={35}
            headerHeight={35}
            width={width}
            height={height}
        >
            <Column
                header={<Cell>Name</Cell>}
                cell={
                    <NameCell
                        data={data}
                        field="userId"
                        users={users}
                    />
                }
                width={300}
                align="center"
            />
            <Column
                header={<Cell>{type}</Cell>}
                cell={
                    <TextCell
                        data={data}
                        field="amount"
                    />
                }
                width={209}
                align="center"
            />
        </Table>
    )
}

class BalanceSheet extends React.Component {
    render(){
        let {debit, credit} = this.props
        return (
            <div className="BalanceSheet">
                <BalanceTable
                    data={credit}
                    type={'CREDIT'}
                    users={this.props.users}
                />
                <BalanceTable
                    data={debit}
                    type={'DEBIT'}
                    users={this.props.users}
                />
            </div>
        )
    }
}

const mapStateToProps = store => ({
    users: store.Users.get('users')
})

export default connect(mapStateToProps)(BalanceSheet)