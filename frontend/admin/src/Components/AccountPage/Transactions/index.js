import React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { Table, Column, Cell } from 'fixed-data-table'
import $ from 'jquery'

import request from 'Lib/request'
import {setTransactions, addTransactions} from 'Actions/UserActions'
import './Transactions.css'

const TextCell = ({ rowIndex, field, data, ...props }) => {
    return typeof field === 'string' 
        ? (
            <Cell {...props}>
                {'' + data.getIn([rowIndex, field])}
            </Cell>
        )
        : (
            <Cell {...props}>
                {'' + data.getIn([rowIndex, ...field])}
            </Cell> 
        )
}
    

const DateCell = ({ rowIndex, field, data, ...props }) =>
    <Cell {...props}>
        {new Date(data.getIn([rowIndex, field])).toLocaleString()}
    </Cell>

class Transactions extends React.Component {
    addTransactions = async () => {
        let length = this.props.transactions.size
        if (length >= this.state.count) return
        try {
            let res = await request.post('/api/users/transactions', {
                skip: length
            })
            let newTransactions = res.data
            this.props.addTransactions(newTransactions)
        } catch (err) {
            console.log(err)
        }
    }
    render() {
        let width = Math.min($(window).width(), $(document).width()) - 10
        let height = Math.min($(window).height(), $(document).height()) - 205
        let length = this.props.transactions.size
        return (
            <div className="Transactions" >
                <p>
                    Showing {this.props.transactions.size} of {this.props.count} 
                    {' '}Transactions
                </p>
                <div className="TableCenter">
                    <Table
                        rowsCount={this.props.transactions.size}
                        rowHeight={40}
                        headerHeight={40}
                        width={width}
                        height={height}
                    >
                        <Column
                            header={<Cell>Type</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.transactions}
                                    field="transactionType"
                                />
                            }
                            width={209}
                            align="center"
                        />
                        <Column
                            header={<Cell>Parent</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.transactions}
                                    field={['parent', 'username']}
                                />
                            }
                            width={209}
                            align="center"
                        />
                        <Column
                            header={<Cell>Child</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.transactions}
                                    field={['child', 'username']}
                                />
                            }
                            width={209}
                            align="center"
                        />
                        <Column
                            header={<Cell>Amount</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.transactions}
                                    field="amount"
                                />
                            }
                            width={209}
                            align="center"
                        />
                        <Column
                            header={<Cell>Ip Address</Cell>}
                            cell={
                                <TextCell
                                    data={this.props.transactions}
                                    field="ipAddress"
                                />
                            }
                            width={209}
                            align="center"
                        />
                        <Column
                            header={<Cell>Date</Cell>}
                            cell={
                                <DateCell
                                    data={this.props.transactions}
                                    field="createdAt"
                                />
                            }
                            width={209}
                            align="center"
                        />
                    </Table>
                </div>
                <div className="AddButton">
                    <button 
                        disabled={length >= this.props.count}
                        onClick={() => this.addTransactions()}
                    >
                        Show more transactions
                    </button>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch =>
    bindActionCreators({
        setTransactions,
        addTransactions
    }, dispatch)

const mapStateToProps = store => ({
    transactions: store.Users.get('transactions')
})

export default connect(mapStateToProps, mapDispatchToProps)(Transactions)