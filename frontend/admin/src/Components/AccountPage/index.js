import React from 'react'
import { Tabs, Tab } from 'react-mdl'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Transactions from './Transactions'
import BetsHistory from './BetsHistory'
import BalanceSheet from './BalanceSheet'
import './AccountPage.css'
import objectToArray from 'Lib/balanceSheetToArray'
import request from 'Lib/request'
import { 
    addTransactions, 
    setTransactions, 
    setBalanceSheet 
} from 'Actions/UserActions'
import {addAllBets, setAllBets} from 'Actions/BetsActions'

class AccountPage extends React.Component {
    constructor(){
        super()
        this.state = {
            activeTab: 0,
            transactionsCount: 0,
            allBetsCount: 0,        
        }
    }
    async componentDidMount(){
        try {
            let res = await request.post('/api/users/transactions')
            this.props.setTransactions(res.data)
            res = await request.get('/api/users/transactions-count') 
            let transactionsCount = res.data
            res = await request.post('/api/betfair/all-bets')
            let allBets = res.data
            this.props.setAllBets(allBets)
            res = await request.get('/api/betfair/all-bets-count')
            let allBetsCount = res.data
            res = await request.get('/api/users/balance-sheet')
            let balanceSheet = res.data
            let credit = balanceSheet.credit
            let debit = balanceSheet.debit
            this.props.setBalanceSheet({
                debit, 
                credit
            })
            this.setState({
                allBetsCount,
                transactionsCount
            })
        } catch (err) {
            console.log(err)
        }
    }
    render(){
        let tabs = ['BALANCESHEET', 'BETSHISTORY', 'TRANSACTIONS']
        let PageComponent
        let debit = this.props.balanceSheet.get('debit')
        let credit = this.props.balanceSheet.get('credit')
        switch(tabs[this.state.activeTab]){
            case 'BALANCESHEET':
                PageComponent = 
                    <BalanceSheet 
                    credit={objectToArray(credit.toJS())}
                    debit={objectToArray(debit.toJS())}
                    />
                break
            case 'BETSHISTORY':
                PageComponent = 
                    <BetsHistory 
                        allBetsCount={this.state.allBetsCount}
                    />
                break
            case 'TRANSACTIONS':
                PageComponent = 
                <Transactions 
                    count={this.state.transactionsCount} 
                />
                break
            default:
                PageComponent = <BalanceSheet />
        }
        return (
            <div className="AccountPage">
                <Tabs 
                    ripple 
                    activeTab={this.state.activeTab} 
                    onChange={(tabId) => this.setState({ activeTab: tabId })}
                > 
                    <Tab>Balance Sheet</Tab>
                    <Tab>Bets History</Tab>
                    <Tab>Transactions</Tab>
                </Tabs>
                {PageComponent} 
            </div>
        )
    }
}

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        addTransactions,
        addAllBets,
        setAllBets,
        setTransactions,
        setBalanceSheet
    }, dispatch)

const mapStateToProps = store => ({
    transactions: store.Users.get('transactions'),
    allBets: store.Bets.get('allBets'),
    balanceSheet: store.Users.get('balanceSheet')
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage)