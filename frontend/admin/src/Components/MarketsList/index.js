import React from 'react'

import Market from 'Components/Market'
import './MarketsList.css'

const MarketsList = ({
    activeMarkets,
    watchedMarkets,
    removeWatchedMarkets,
    activeBets,
    projections,
    users,
    blockedMarkets,
    limitedMarkets
}) => {
    return (
        <div className="MarketsList">
            {watchedMarkets.map( marketId => {
                let market = activeMarkets.get(marketId)
                if(!market) return <div key={marketId}></div>
                return (
                    <Market
                        market={activeMarkets.get(marketId)}
                        removeWatchedMarkets={removeWatchedMarkets}
                        key={marketId}
                        activeBets={activeBets}
                        projection={projections.get(marketId)}
                        users={users}
                        blockedMarkets={blockedMarkets}
                        limit={limitedMarkets.get(''+marketId)}
                    />
                )
            })}
        </div>
    )
}

MarketsList.propTypes = {
    activeMarkets: React.PropTypes.object.isRequired,
    watchedMarkets: React.PropTypes.object.isRequired,
    removeWatchedMarkets: React.PropTypes.func.isRequired
}

export default MarketsList
