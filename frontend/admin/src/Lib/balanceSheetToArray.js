import _ from 'lodash'

export default (obj) => {
    return _.map(obj, (value, key) => ({
        userId: key,
        amount: value
    }))
    .filter(entry => entry.amount !== 0)
}