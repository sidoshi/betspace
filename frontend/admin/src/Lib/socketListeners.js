import {bindActionCreators} from 'redux'
import alertify from 'alertify.js'

import socket from 'Lib/socket'
import {store} from 'Store'
import getProjection from './getProjection'
import * as MarketsPageActions from 'Actions/MarketsPageActions'
import * as BetsActions from 'Actions/BetsActions'
import * as UserActions from 'Actions/UserActions'
import * as ProjectionActions from 'Actions/ProjectionActions'

alertify.logPosition('bottom right')

let actions = bindActionCreators({
    ...BetsActions,
    ...MarketsPageActions,
    ...UserActions,
    ...ProjectionActions
},store.dispatch)

let {
    setActiveMarkets,
    updateMarket,
    removeWatchedMarkets,
    deleteMarket,
    addBet,
    updateBet,
    deleteBet,
    updateBalance,
    addUser,
    updateUser,
    deleteProjection,
    setProjection,
    setWatchedMarkets,
    addBlockedMarkets,
    removeBlockedMarkets,
    addLimitedMarkets,
    addUserLimit
} = actions


const getBetsAndMarket = bet => {
    let state = store.getState()
    let market = state.MarketsPage.getIn(['activeMarkets', bet.marketId])
    let bets = state.Bets.getIn(['activeBets','byMarket',bet.marketId])
    let byId = state.Bets.getIn(['activeBets','byId'])
    bets = bets.map( betId => byId.get(betId))
    return {market, bets}
}

const getUsers = () => {
    let state = store.getState()
    let users = state.Users.get('users')
    return users
}

const getMarket = marketId => {
    let state = store.getState()
    let market = state.MarketsPage.getIn(['activeMarkets', marketId])
    return market
}

const handleSocketListeners = () => {
    socket.on('MARKETS_UPDATE', markets => {
        setActiveMarkets(markets)
    })
    socket.on('MARKET_UPDATE', market => {
        updateMarket(market)
    })
    socket.on('MARKET_CLOSE', marketId => {
        deleteProjection(marketId)
        removeWatchedMarkets(marketId)
        deleteMarket(marketId)
    })
    socket.on('MARKET_CLOSE_WARN', market => {
        updateMarket(market)
    })
    socket.on('INPLAY_CHANGE', market => {
        let oldMarket = getMarket(market.marketId)
        if (!oldMarket.get('inplay')){
            alertify.success(`${market.name} inplay`)
            updateMarket(market)
        }
    })
    socket.on('BET_PLACED', bet => {
        addBet(bet)
        let {market, bets} = getBetsAndMarket(bet)
        let users = getUsers()
        let projection = getProjection(market, bets, users)
        setWatchedMarkets(bet.marketId)
        setProjection({
            marketId: bet.marketId,
            projection: projection
        })
        alertify.success('Bet Placed')
    })
    socket.on('BET_MATCHED', bet => {
        updateBet(bet)
        let {market, bets} = getBetsAndMarket(bet)
        let users = getUsers()
        let projection = getProjection(market, bets, users)
        setProjection({
            marketId: bet.marketId,
            projection: projection
        })
    })
    socket.on('REMOVED_UNMATCHED_BET', bet => {
        alertify.error('Bet Removed')
        deleteBet(bet)
    })
    socket.on('BET_RESULT', bet => {
        updateBet(bet)
    })
    socket.on('CANCEL_BET_FAILED', bet => {
        alertify.error('Bet cancelling failed')
    })
    socket.on('PLACE_BET_FAILED', bet => {
        alertify.error('Failed to place Bet')
    })
    socket.on('BALANCE_UPDATE', user => {
        updateBalance(user)
    })
    socket.on('ADD_USER', user => {
        addUser(user)
    })
    socket.on('UPDATE_USER', user => {
        updateUser(user)
    })
    socket.on('MARKET_BLOCKED', marketId => {
        addBlockedMarkets(marketId)
    })
    socket.on('MARKET_UNBLOCKED', marketId => {
        removeBlockedMarkets(marketId)
    })
    socket.on('ADD_MARKET_LIMIT', limit => {
        addLimitedMarkets(limit)
    })
    socket.on('ADD_USER_LIMIT', limit => {
        addUserLimit(limit)
    })
}

export default handleSocketListeners
