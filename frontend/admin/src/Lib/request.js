import axios from 'axios'

import jwtDecoded from 'Lib/jwtDecoded'

let request = axios.create({
    headers: {'Csrf-Token': jwtDecoded.csrfToken}
})

export default request
