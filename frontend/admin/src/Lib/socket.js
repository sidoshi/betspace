import io from 'socket.io-client'
import alertify from 'alertify.js'

import config from 'Lib/config'
import jwtDecoded from 'Lib/jwtDecoded'

let {url,port} = config
let socket = io(`${url}:${port}`, {'transports': ['websocket']})

socket.on('disconnect', data => {
    alertify.error('Disconnected from server')
})

socket.emit('Csrf-Token', jwtDecoded.csrfToken)


export default socket
