const toFixed = (value, precision) => {
    let power = Math.pow(10, precision || 2)
    return +(Math.round(value * power) / power)
}

const percentOf = (total, percent) => {
    let value = total * percent / 100
    return toFixed(value)
}

export {
    percentOf,
    toFixed
}