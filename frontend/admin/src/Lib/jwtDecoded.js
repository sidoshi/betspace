import decode from 'jwt-decode'
import cookie from 'js-cookie'

import config from "Lib/config"

let token = cookie.get('token')
if(!token) window.location = config.url + ':' + config.port
let jwtDecoded = token && decode(token)

export default jwtDecoded
