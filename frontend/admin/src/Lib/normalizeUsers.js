const normalizeUsers = users => {
    let normalized = {}
    users.forEach( user => {
        user.userId = user._id
        delete user._id
        normalized[user.userId] = user
    })
    return normalized
}

export default normalizeUsers
