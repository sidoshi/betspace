import _ from 'lodash'

import jwtDecoded from './jwtDecoded'
import { percentOf } from 'Lib/numberUtils'

/*
    ==>  Give runner status required to win the given type of bet.
    =>  For example: 
    =>  if the type of bet is BACK, then the runner status required to win the
        bet is WINNER. So statusToWin['BACK'] would return 'WINNER'.
    =>  Now if the status of runner betted on is 'WINNER' the bet is WINNER.
*/
const statusToWin = {
    'BACK': 'WINNER',
    'LAY':  'LOSER'
}

/*
    =>	Returns the result of the bet for a given runnersStatus and betType.
*/
const getBetResult = (runnerStatus, betType) => {
    if (runnerStatus !== 'WINNER' && runnerStatus !== 'LOSER'){
        return 'NEUTRAL'
    }
    if ( runnerStatus === statusToWin[betType]){
        return 'WINNER'
    }else{
        return 'LOSER'
    }
}

/*
    =>	Fill each runner Id with LOSER status, so afterwords we can 
        change a single runner to WINNER and calculate market projection
        on that probable market outcome 
*/
const getRunnersStatus = (runnerIds, runnersStatus) => {
    runnerIds.forEach( runnerId => {
        runnersStatus[runnerId] = 'LOSER'
    })
}


/*
    =>  For a given bet, check if the bet is WINNER or LOSER
    =>  If the bet is WINNER:
            remove profit from projection.self.runnerId
            add profit to projection.children.userId.runnerId
    =>  If the bet is LOSER:
            add exposure to projection.self.runnerId
            remove exposure from projection.children.userId.runnerId
*/
const calculateBet = (bet, runnerId, runnersStatus, projection, users) => {
    let runnerStatus = runnersStatus[bet.get('runnerId')]
    let result = getBetResult(runnerStatus, bet.get('betType'))
    let profit = +bet.get('profit')
    let exposure = +bet.get('exposure')
    let userId = bet.get('directChildId')
    let selfId = jwtDecoded.userId
    let self = users.get(selfId)
    let child = users.get(userId)
    let parentId = self.getIn(['directParent', 'userId'])
    let selfRatio = bet.getIn(['shares', selfId])
    let parentRatio = bet.getIn(['shares', parentId])
    let childRatio = bet.getIn(['shares', userId])
    if(child.get('userType') === 'CLIENT') childRatio = -100
    if (!bet.get('matched')) return 
    _.defaultsDeep( projection, {
        parent: { [runnerId]: 0 },
        self: { [runnerId]: 0 },
        children: { [userId]: { [runnerId]: 0 } }
    })
    let childProjection = projection.children[userId]
    switch (result) {
        case 'WINNER':
            projection.parent[runnerId] -= percentOf(profit, parentRatio)
            // take profit from admin
            projection.self[runnerId] -= percentOf(profit, selfRatio)
            // and give it to child 
            childProjection[runnerId] -= percentOf(profit, childRatio) 
            break
        case 'LOSER':
            projection.parent[runnerId] += percentOf(exposure, parentRatio) 
            // add profit to admin
            projection.self[runnerId] += percentOf(exposure, selfRatio) 
            // which is taken from child
            childProjection[runnerId] += percentOf(exposure, childRatio) 
            break
        default:
            console.log('neither winner nor looser')
            break
    }
}

/*
    =>	Iterate over each bets and calculate projection for that bet. 
*/
const calculateBets = (bets, runnerId, runnersStatus, projection, users) => {
    bets.forEach( bet => {
        calculateBet(bet, runnerId, runnersStatus, projection, users)
    })
}

const getProjection = (market, bets, users) => {
    /*
        ==>	The core projection object to return.
        =>  Example projecton object:
        =>  self: {
                [runnerId]: projection of that runner for self || 0
            },
            children: {
                [childrenId]: {
                    [runnerId]: projection of that runner for children || 0
                }
            }
    */
    let projection = {
        self: {},
        children: {}
    }

    /*
        =>  Runner statuses which holds probable market outcome for each
            senario.
        =>  At a given time one of each runner will be selected as WINNER and
            rest as LOSER.
    */
    let runnersStatus = {}
    let runners = market.get('runners') // get runners from the market.
    let runnerIds = runners.keySeq().toArray() // get Ids from runners.
    // Fill runnersStatus object with each runnerId as LOSER
    getRunnersStatus(runnerIds, runnersStatus)

    /*
        =>  Change one runner to WINNER, then calculateBets for that probable
            market outcome.
        =>  Then change that runner back to LOSER after calculating bets.
    */
    runnerIds.forEach( runnerId => {
        runnersStatus[runnerId] = 'WINNER'
        calculateBets(bets, runnerId, runnersStatus, projection, users)
        runnersStatus[runnerId] = 'LOSER'
    })
    return projection
}

export default getProjection

