let config = {
    url: 'http://localhost',
    port: '4000',
    eventTypes: ['Cricket','Soccer','Tennis','Horse Racing'],
    validChilds: {
        'MAIN ADMIN':       ['SUPER MASTER', 'MASTER'],
        'SUPER MASTER':     ['MASTER'],
        'MASTER':           ['DEALER', 'CLIENT'],
        'DEALER':           ['CLIENT'],
        'CLIENT':           []
    }
}
export default config
