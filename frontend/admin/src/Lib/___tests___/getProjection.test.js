import { fromJS } from 'immutable'

import market from './market'
import bets from './bets'
import getProjection from '../getProjection'


let Imarket = fromJS(market)
let Ibets = fromJS(bets)

it('should match snapshot', () => {
    expect(getProjection(Imarket, Ibets)).toMatchSnapshot()
})