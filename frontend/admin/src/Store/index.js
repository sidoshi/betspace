import { createStore } from 'redux'

import { reducer } from 'Reducers'

exports.store = createStore(reducer)
