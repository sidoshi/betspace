import { fromJS } from 'immutable'

let defaults = fromJS({
    activeBets: {
        byId: {},
        byMarket: {}
    },
    allBets: [],
    allBetsCount: 0,
    lastFetchedAll: 0 
})


const BetsReducer = (state=defaults, action) => {
    let payload = action.payload
    let index, activeBets, list, tree, byId
    switch ( action.type ) {
        case 'SET_ACTIVE_BETS':
            return state.set('activeBets', fromJS(payload))
        case 'SET_ALL_BETS': 
            return state.set('allBets', fromJS(payload))
        case 'ADD_ALL_BETS':
            let newBets = fromJS(payload)
            let allBets = state.get('allBets')
            allBets = allBets.concat(newBets)
            return state.set('allBets', allBets)
        case 'UPDATE_BET':
            byId = state.getIn(['activeBets','byId'])
            byId = byId.set(payload._id,fromJS(payload))
            return state.setIn(['activeBets','byId'], byId)
        case 'DELETE_BET':
            let id = payload._id
            let marketId = payload.marketId
            tree = ['activeBets','byId',id]
            activeBets = state.deleteIn(tree).get('activeBets')
            tree = ['byMarket',marketId]
            list = activeBets.getIn(tree)
            index = list.indexOf(id)
            index >= 0 && (list = list.delete(index))
            activeBets = activeBets.setIn(tree, list)
            return state.set('activeBets', activeBets)
        case 'ADD_BET':
            activeBets = state.get('activeBets')
            activeBets = activeBets.setIn(['byId',payload._id], fromJS(payload))
            list = activeBets.getIn(['byMarket',payload.marketId]) || fromJS([])
            list = list.push(payload._id)
            activeBets = activeBets.setIn(['byMarket',payload.marketId], list)
            return state.set('activeBets', activeBets)
        default:
            return state
    }
}

export default BetsReducer
