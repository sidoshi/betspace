import { fromJS } from 'immutable'

let defaults = fromJS({
    activeMarkets: {
        'Cricket': [],
        'Soccer': [],
        'Tennis': [],
        'Horse Racing': []
    },
    watchedMarkets: [],
    blockedMarkets: {
        byIds: {},
        allMarkets: []
    },
    limitedMarkets: {}
})

const getAllBlockedMarkets = byIds => {
    let allMarkets = []
    Object.keys(byIds).forEach(userId => 
        allMarkets.push(...byIds[userId])
    )
    allMarkets = [...new Set(allMarkets)]
    return allMarkets
}


const MarketsPageReducer = (state=defaults, action) => {
    let payload = action.payload
    let watchedMarkets, index, activeMarkets, marketType, list
    switch ( action.type ) {
        case 'SET_ACTIVE_MARKETS':
            return state.set('activeMarkets', fromJS(payload))
        case 'UPDATE_MARKET':
            activeMarkets = state.get('activeMarkets')
            activeMarkets = activeMarkets.set(payload.marketId,fromJS(payload))
            return state.set('activeMarkets', activeMarkets)
        case 'DELETE_MARKET':
            activeMarkets = state.get('activeMarkets')
            marketType = activeMarkets.getIn([payload, 'type'])
            activeMarkets = activeMarkets.delete(payload)
            list = activeMarkets.get(marketType)
            if(list) index = list.indexOf(payload)
            if(index >= 0) list = list.delete(index)
            activeMarkets = activeMarkets.set(marketType, list)
            return state.set('activeMarkets', activeMarkets)
        case 'SET_WATCHED_MARKETS':
            index = state.get('watchedMarkets').indexOf(payload)
            if (index >= 0) return state
            watchedMarkets = state.get('watchedMarkets').push(payload)
            return state.set('watchedMarkets', watchedMarkets)
        case 'REMOVE_WATCHED_MARKETS':
            index = state.get('watchedMarkets').indexOf(payload)
            if (index < 0) return state
            watchedMarkets = state.get('watchedMarkets').delete(index)
            return state.set('watchedMarkets',watchedMarkets)
        case 'CLOSE_MARKET':
            activeMarkets = state.get('activeMarkets')
            activeMarkets = activeMarkets.setIn([payload,'inplay'],false)
            return state.set('activeMarkets', activeMarkets)
        case 'ADD_BLOCKED_MARKETS':
            let byIds = state.getIn(['blockedMarkets', 'byIds']).toJS()
            Object.keys(payload).forEach(userId => {
                byIds[userId] = byIds[userId] || []
                byIds[userId].push(...payload[userId])
                byIds[userId] = [...new Set(byIds[userId])]
            })
            let allMarkets = getAllBlockedMarkets(byIds)
            return state.set('blockedMarkets', fromJS({byIds, allMarkets}))
        case 'REMOVE_BLOCKED_MARKETS':
            byIds = state.getIn(['blockedMarkets', 'byIds']).toJS()
            let userId = Object.keys(payload)[0]
            byIds[userId] = byIds[userId] || []
            index = byIds[userId].indexOf(payload[userId])
            index >=0 && byIds[userId].splice(index, 1)
            allMarkets = getAllBlockedMarkets(byIds)
            return state.set('blockedMarkets', fromJS({ byIds, allMarkets }))
        case 'ADD_LIMITED_MARKETS':
            let limitedMarkets = state.get('limitedMarkets').toJS()
            Object.keys(payload).forEach( marketId => {
                limitedMarkets[marketId] = payload[marketId]
            })
            return state.set('limitedMarkets', fromJS(limitedMarkets))
        default:
            return state
    }
}

export default MarketsPageReducer
