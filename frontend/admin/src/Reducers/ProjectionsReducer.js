import { fromJS } from 'immutable'

let defaults = fromJS({})

const ProjectionsReducer = (state=defaults, action) => {
    let payload = action.payload
    switch(action.type){
        case 'SET_PROJECTION':
            return state.set(payload.marketId, fromJS(payload.projection))
        case 'DELETE_PROJECTION':
            return state.delete(payload)
        default:
            return state
    }
}


export default ProjectionsReducer
