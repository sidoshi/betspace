import { combineReducers } from 'redux'

import MarketsPageReducer from './MarketsPageReducer'
import BetsReducer from './BetsReducer'
import UsersReducer from './UsersReducer'
import ProjectionsReducer from './ProjectionsReducer'

let reducer = combineReducers({
    MarketsPage: MarketsPageReducer,
    Bets: BetsReducer,
    Users: UsersReducer,
    Projections: ProjectionsReducer
})

exports.reducer = reducer
