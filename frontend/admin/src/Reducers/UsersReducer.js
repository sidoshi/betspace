import { fromJS } from 'immutable'

let defaults = fromJS({
    users: {

    },
    transactions: [],
    balanceSheet: {
        credit: {},
        debit: {}
    }
})

const UserReducer = (state=defaults, action) => {
    let payload = action.payload
    let user
    switch(action.type){
        case 'UPDATE_BALANCE':
            user = state.getIn(['users', payload.userId])
            user = user || fromJS(payload)
            user = user.set('balance', payload.balance)
            return state.setIn(['users', payload.userId], user)
        case 'SET_USERS':
            return state.set('users', fromJS(payload))
        case 'ADD_USER':
            return state.setIn(['users', payload.userId], fromJS(payload))
        case 'UPDATE_USER':
            return state.setIn(['users', payload.userId], fromJS(payload))
        case 'SET_TRANSACTIONS':
            return state.set('transactions', fromJS(payload))
        case 'ADD_TRANSACTIONS':
            let newTransactions = fromJS(payload)
            let allTransactions = state.get('transactions')
            allTransactions = allTransactions.concat(newTransactions)
            return state.set('transactions', allTransactions)
        case 'SET_BALANCESHEET':
            return state.set('balanceSheet', fromJS(payload))
        case 'ADD_USER_LIMIT':
            let userId = Object.keys(payload)[0]
            user = state.getIn(['users', userId]) 
            if (!user) return state
            user = user.toJS()
            user.userLimit = {
                min: payload[userId].min,
                max: payload[userId].max
            }
            return state.setIn(['users', userId], fromJS(user))
        default:
            return state
    }
}


export default UserReducer
