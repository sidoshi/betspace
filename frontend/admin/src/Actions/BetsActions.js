const setActiveBets = payload => ({
    type: 'SET_ACTIVE_BETS',
    payload
})

const setAllBets = payload => ({
    type: 'SET_ALL_BETS',
    payload
})

const addAllBets = payload => ({
    type: 'ADD_ALL_BETS',
    payload
})

const updateBet = payload => ({
    type: 'UPDATE_BET',
    payload
})

const deleteBet = payload => ({
    type: 'DELETE_BET',
    payload
})

const addBet = payload => ({
    type: 'ADD_BET',
    payload
})


export {
    setActiveBets,
    setAllBets,
    updateBet,
    deleteBet,
    addBet,
    addAllBets
}
