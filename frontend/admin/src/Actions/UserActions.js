const updateBalance = payload => ({
    type: 'UPDATE_BALANCE',
    payload
})

const setUsers = payload => ({
    type: 'SET_USERS',
    payload
})

const addUser = payload => ({
    type: 'ADD_USER',
    payload
})

const updateUser = payload => ({
    type: 'UPDATE_USER',
    payload
})

const addTransactions = payload => ({
    type: 'ADD_TRANSACTIONS',
    payload
})

const setTransactions = payload => ({
    type: 'SET_TRANSACTIONS',
    payload
})

const setBalanceSheet = payload => ({
    type: 'SET_BALANCESHEET',
    payload
})

const addUserLimit = payload => ({
    type: 'ADD_USER_LIMIT',
    payload
})

export {
    updateBalance, 
    setUsers, 
    updateUser, 
    setTransactions,
    addUser, 
    setBalanceSheet,
    addTransactions,
    addUserLimit
}
