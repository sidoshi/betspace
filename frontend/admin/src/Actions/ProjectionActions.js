const setProjection = payload => ({
    type: 'SET_PROJECTION',
    payload
})

const deleteProjection = payload => ({
    type: 'DELETE_PROJECTION',
    payload
})

export {setProjection, deleteProjection}
