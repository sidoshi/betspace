#!/bin/bash

rm -rf build
npm run build
cd build
html2jade index.html
rm index.html
mv index.jade admin.pug
mv admin.pug ../../../backend/slave/_views/
rm -rf ../../../backend/slave/_static/admin/*
mv * ../../../backend/slave/_static/admin/
cd ..
rm -rf build

