let mongoose = require('mongoose')

let Schema          = mongoose.Schema
mongoose.Promise    = global.Promise

let BalanceSheetSchema = new Schema({
    userId: Schema.Types.ObjectId,
    debit: { type: Object, default: {} },
    credit: { type: Object, default: {}}
},{
    strict: false,
    collection: 'balanceSheets',
    minimize: false
})

let BalanceSheet = mongoose.model('balanceSheet', BalanceSheetSchema)

module.exports = BalanceSheet