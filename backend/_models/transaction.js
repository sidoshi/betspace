let mongoose = require('mongoose')

let Schema = mongoose.Schema
mongoose.Promise = global.Promise

let UserSchema = new Schema({
    username: String,
    userId: Schema.Types.ObjectId
},{
    _id: false
})

let TransactionSchema = new Schema({
    transactionType: String,
    parent: UserSchema,
    child: UserSchema,
    amount: Number,
    ipAddress: String
},{
    timestamps: true,
})

let Transaction = mongoose.model('Transaction', TransactionSchema)

module.exports = Transaction