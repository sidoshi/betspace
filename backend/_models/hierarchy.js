let mongoose = require('mongoose')

let Schema          = mongoose.Schema
mongoose.Promise    = global.Promise

let HierarchySchema = new Schema({
    endpoint:       Schema.Types.ObjectId,
    shares:         Object,
    users:          [Schema.Types.ObjectId]
},{
    strict: false,
    collection: 'hierarchies'
})

let Hierarchy = mongoose.model('Hierarchy', HierarchySchema)

module.exports = Hierarchy