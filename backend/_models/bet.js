let mongoose                = require('mongoose')

let Parent = require('./parent')

let Schema          = mongoose.Schema
mongoose.Promise    = global.Promise

let betSchema  = new Schema({
    marketId:               { type: String, required: true },
    marketName:             { type: String, required: true },
    marketStatus:           { type: String, required: true },
    marketType:             { type: String, required: true },
    runnerId:               { type: String, required: true },
    runnerName:             { type: String, required: true },
    runnerStatus:           { type: String, required: true },
    stake:                  { type: Number, required: true },
    rate:                   { type: Number, required: true },
    matched:                { type: Boolean, required: true },
    betType:                { type: String, required: true },
    result:                 { type: String, default: 'PENDING' },
    userId:                 { type: Schema.Types.ObjectId, required: true },
    username:               { type: String, required: true },
    userType:               { type: String, required: true },
    parents:                [Parent],
    ipAddress:              [String],
    profit:                 { type: Number, required: true },
    exposure:               { type: Number, required: true },
    shares:                 Object
},{
    timestamps: true,
    strict: false
})

let Bet = mongoose.model('bet', betSchema)

module.exports = Bet
