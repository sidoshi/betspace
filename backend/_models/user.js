let mongoose                = require('mongoose')

let Parent = require('./parent')

let Schema          = mongoose.Schema
mongoose.Promise    = global.Promise

let limitedMarket = new Schema({
    marketId: String,
    min: Number,
    max: Number
},{
    _id: false
})

let userLimitSchema = new Schema({
    min: { type: Number, default: 0 },
    max: { type: Number, default: Infinity }
},{
    _id: false
})

let userSchema  = new Schema({
    username:       { type: String, required: true, unique: true },
    password:       { type: String, required: true },
    userType:       { type: String, required: true },
    availableShare: { type: Number, required: true },
    credit:         { type: Number, default: 0 },
    balance:        { type: Number, default: 0 },
    profitAndLoss:  { type: Number, default: 0 },
    bettingBalance: { type: Number, default: 0 },
    exposure:       { type: Number, default: 0 },
    children:       { type: [Schema.Types.ObjectId], default: [] },
    directParent:   { type: Parent, default: null },
    grandParent:    { type: Parent, default: null },
    parents:        { type: [Parent], default: null },
    blocked:        { type: Boolean, default: false },
    parentBlocked:  { type: Boolean, default: false },
    online:         { type: Boolean, default: false},
    ipAddress:      [String],
    childCredit:    { type: Number, default: 0 },
    name:           String,
    blockedMarkets: [String],
    limitedMarkets: [limitedMarket],
    userLimit:      userLimitSchema
},{
    timestamps: true
})

let User = mongoose.model('user', userSchema)

module.exports = User
