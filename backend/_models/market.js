let mongoose        = require('mongoose')

let Schema          = mongoose.Schema
mongoose.Promise    = global.Promise

// let runnerSchema = new Schema({
//     selectionId: String,
//     name: String,
//     availableToBack: Object,
//     availableToLay: Object,
//     status: String,
//     sortPriority: Number
// }, {
//     _id: false,
//     strict: false
// })

let marketSchema  = new Schema({
    marketId:       String,
    name:           String,
    type:           String,
    runners:        Object,
    inplay:         {type: Boolean, default: false},
    sortedRunners:  [Number],
    betsPlaced:     {type: Number, default: 0},
    status:         String
},{
    strict: false
})

let Market = mongoose.model('market', marketSchema)

module.exports = Market
