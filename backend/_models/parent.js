let mongoose = require('mongoose')

let Schema          = mongoose.Schema
mongoose.Promise    = global.Promise

let Parent = new Schema({
    userId:     Schema.Types.ObjectId,
    userType:   String,
    username:   String
},{
    _id: false
})

module.exports = Parent
