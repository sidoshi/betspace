const socketDos = socket => {
    const limit = callback => {
        let count = 0
        let safe  = true
        const limited = data => {
            wait('1s', () => {
                if(socket.connected){
                    count = 0
                    safe = true
                }
            })
            if(safe){
                callback(data)
                safe = false
            }else{
                count++
                if(count > 10){
                    socket.emit('ATTACK', 'WATCHING YOU.')
                    socket.disconnect()
                }
            }
        }
        return limited
    }
    return limit
}


module.exports = socketDos
