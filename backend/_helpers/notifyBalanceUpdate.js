const notifyBalanceUpdate = (user, io) => {
    user = (user.toObject && user.toObject()) || user
    user.password && delete user.password
    io.to(user.userId).emit('BALANCE_UPDATE', user)
    if (!user.parents) return
    let parents = [...user.parents]
    parents.reverse()
    parents.forEach((parent, index) => {
        index <= 1 &&
            io.to(parent.userId).emit('BALANCE_UPDATE', user)
    })
}

module.exports = notifyBalanceUpdate