let modifyBet = require('_helpers/modifyBet')


const emitConcernedUsers = (io, bet, event) => {
    let userModifiedBet = modifyBet(bet.userId, bet)
    io.to(bet.userId).emit(event, userModifiedBet)
    bet.parents.forEach(parent => {
        let parentModifiedBet = modifyBet(parent.userId, bet)
        io.to(parent.userId).emit(event, parentModifiedBet)
    })
}

module.exports = emitConcernedUsers