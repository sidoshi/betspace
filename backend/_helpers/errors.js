let catchError = (req, res, next) => {
    let err = new Error('Not Found')
    err.status = 404
    next(err)
}

let handleError = (err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
}

module.exports.catchError = catchError
module.exports.handleError = handleError
