let User = require('_models/user')

const getBlockedMarketsMap = async userId => {
    let blockedMarkets = {}
    let user = await User.findById(userId)
    blockedMarkets[userId] = user.blockedMarkets
    let parentIds = user.parents.map(parent => parent.userId)
    let parentUsers = await User.find({
        _id: { $in: parentIds }
    })
    parentUsers.forEach(user => blockedMarkets[user._id] = user.blockedMarkets)
    return blockedMarkets
}

module.exports = async (userId, map=false) => {
    if(map) return await getBlockedMarketsMap(userId)
    let blockedMarkets = []
    let user = await User.findById(userId)
    blockedMarkets.push(...user.blockedMarkets)
    let parentIds = user.parents.map(parent => parent.userId)
    let parentUsers = await User.find({
        _id: { $in: parentIds }
    })
    parentUsers.map(user => blockedMarkets.push(...user.blockedMarkets))
    return [...new Set(blockedMarkets)]
}