const getDirectChild = (bet, userId) => {
    if (userId === bet.userId) return null
    if (userId === bet.parents[bet.parents.length - 1].userId) return {
        userId: bet.userId,
        username: bet.username,
        userType: 'CLIENT'
    }
    else {
        let userIndex = bet.parents.findIndex(bet => bet.userId === userId)
        if (userIndex < 0) return null
        else return bet.parents[userIndex + 1]
    } 
}

const getDirectParent = (bet, userId) => {
    if (userId === bet.userId) return bet.parents[bet.parents.length - 1]
    else if (userId === bet.parents[0].userId) return null
    else {
        let userIndex = bet.parents.findIndex( bet => bet.userId === userId)
        if (userIndex <= 0) return null
        else return bet.parents[userIndex - 1]
    } 
}

const getDirectParentFromClient = (client, userId) => {
    if (userId === client._id) return client.parents[client.parents.length - 1]
    else if (userId === client.parents[0].userId) return null
    else {
        let userIndex = client.parents.findIndex( p => p.userId === userId)
        if (userIndex <= 0) return null
        else return client.parents[userIndex - 1]
    } 
}

const getDirectChildFromClient = (client, userId) => {
    if (userId === client._id) return null
    if (userId === client.parents[client.parents.length - 1].userId) return {
        userId: client._id,
        username: client.username,
        userType: 'CLIENT'
    }
    else {
        let userIndex = client.parents.findIndex(p => p.userId === userId)
        if (userIndex < 0) return null
        else return client.parents[userIndex + 1]
    } 
}

module.exports = {
    getDirectParent,
    getDirectChild,
    getDirectParentFromClient,
    getDirectChildFromClient
}