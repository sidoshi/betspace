let BalanceSheet = require('_models/balanceSheet')
let {makeDebitEntry, makeCreditEntry} = require('./makeEntry')


module.exports = async ({amount, parent, child}) => {
    let userIds = [parent._id, child._id]
    let updatePromisies = userIds.map( async userId => {
        let balanceSheet = await BalanceSheet.findOne({ userId })
        makeDebitEntry(balanceSheet, amount, parent._id)
        makeCreditEntry(balanceSheet, amount, child._id)
        await balanceSheet.save()
    })
    await Promise.all(updatePromisies)
}
