let Promise = require('bluebird')
let BalanceSheet = require('_models/balanceSheet')
let { makeDebitEntry, makeCreditEntry } = require('./makeEntry')
let { 
    getDirectChildFromClient, 
    getDirectParentFromClient 
} = require('./getRelatives')

module.exports = async ({ commission, client }) => {
    let directParentId = client.directParent.userId
    let balanceSheet = await BalanceSheet.findOne({ userId: client._id })
    if(!balanceSheet) throw new Error('No balance sheet found')
    let amount = commission
    makeDebitEntry(balanceSheet, amount, client._id)
    makeCreditEntry(balanceSheet, amount, directParentId)
    await balanceSheet.save()
    let parents = [...client.parents].reverse()
    await Promise.each( parents, async parent => {
        let selfId = parent.userId
        let balanceSheet = await BalanceSheet.findOne({ userId: selfId})
        let directChild = getDirectChildFromClient(client, selfId)
        let directParent = getDirectParentFromClient(client, selfId)
        let directChildId = directChild && directChild.userId
        let directParentId = directParent && directParent.userId
        makeDebitEntry(balanceSheet, amount, directChildId)
        if (directParentId) 
            makeCreditEntry(balanceSheet, amount, directParentId)
        else 
            makeCreditEntry(balanceSheet, amount, selfId)
        await balanceSheet.save()
    })
}
