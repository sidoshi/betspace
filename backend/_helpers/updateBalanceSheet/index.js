let depositBalanceSheet = require('./depositBalanceSheet')
let withdrawBalanceSheet = require('./withdrawBalanceSheet')
let betLossBalanceSheet = require('./betLossBalanceSheet')
let betWonBalanceSheet = require('./betWonBalanceSheet')
let passCommission = require('./passCommission')

const updateBalanceSheet = async (data) => {
    try{
        switch (data.entryType) {
            case 'DEPOSIT':
                await depositBalanceSheet(data)
                break
            case 'WITHDRAW':
                await withdrawBalanceSheet(data)
                break
            case 'BET_LOST':
                await betLossBalanceSheet(data)
                break
            case 'BET_WON':
                await betWonBalanceSheet(data)
                break
            case 'PASS_COMMISSION':
                await passCommission(data)
                break
            default:
                throw new Error('Invalid entry type')
        }
    }catch(err){
        console.log(err)
    }
}

module.exports = updateBalanceSheet