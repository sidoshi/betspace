let Promise = require('bluebird')

let BalanceSheet = require('_models/balanceSheet')
let { makeDebitEntry, makeCreditEntry } = require('./makeEntry')
let { getDirectChild, getDirectParent } = require('./getRelatives')
let { percentOf } = require('_helpers/numberUtils')

module.exports = async ({ bet }) => {
    let directParentId = getDirectParent(bet, bet.userId).userId
    let balanceSheet = await BalanceSheet.findOne({ userId: bet.userId })
    if (!balanceSheet) throw new Error('No balance sheet found')
    let totalAmount = bet.profit
    let remainingParentsAmount = bet.profit
    makeDebitEntry(balanceSheet, totalAmount, directParentId)
    makeCreditEntry(balanceSheet, totalAmount, bet.userId)
    await balanceSheet.save()
    let parents = [...bet.parents].reverse()
    await Promise.each( parents, async parent => {
        let selfId = parent.userId
        let balanceSheet = await BalanceSheet.findOne({ userId: selfId })
        let directChild = getDirectChild(bet, selfId)
        let directParent = getDirectParent(bet, selfId)
        let directChildId = directChild && directChild.userId
        let directParentId = directParent && directParent.userId
        let selfRatio = bet.shares[selfId]
        let selfAmount = percentOf(totalAmount, selfRatio)
        makeCreditEntry(balanceSheet, remainingParentsAmount, directChildId)
        makeDebitEntry(balanceSheet, selfAmount, selfId)
        if (directParentId) {
            remainingParentsAmount -= selfAmount
            makeDebitEntry(
                balanceSheet,
                remainingParentsAmount,
                directParentId
            )
        }
        await balanceSheet.save()
    })
}