let makeDebitEntry = (balanceSheet, amount, userId) => {
    if (!balanceSheet) throw new Error('You must provide balance sheet')
    if (!balanceSheet.debit) balanceSheet.debit = {}
    if (!balanceSheet.credit) balanceSheet.credit = {}
    balanceSheet.debit[userId] = +balanceSheet.debit[userId] || 0
    balanceSheet.credit[userId] = +balanceSheet.credit[userId] || 0
    if (!balanceSheet.credit[userId]) {
        balanceSheet.debit[userId] += amount
        delete balanceSheet.credit[userId]
    }
    else if (balanceSheet.credit[userId] >= amount) {
        balanceSheet.credit[userId] -= amount
        delete balanceSheet.debit[userId]
    }
    else if (balanceSheet.credit[userId] <= amount) {
        amount -= balanceSheet.credit[userId]
        delete balanceSheet.credit[userId]
        balanceSheet.debit[userId] = amount
    }
    balanceSheet.markModified('credit')
    balanceSheet.markModified('debit')
}

let makeCreditEntry = (balanceSheet, amount, userId) => {
    if (!balanceSheet) throw new Error('You must provide balance sheet')
    if (!balanceSheet.debit) balanceSheet.debit = {}
    if (!balanceSheet.credit) balanceSheet.credit = {}
    balanceSheet.debit[userId] = +balanceSheet.debit[userId] || 0
    balanceSheet.credit[userId] = +balanceSheet.credit[userId] || 0
    if (!balanceSheet.debit[userId]) {
        balanceSheet.credit[userId] += amount
        delete balanceSheet.debit[userId]
    }
    else if (balanceSheet.debit[userId] >= amount) {
        balanceSheet.debit[userId] -= amount
        delete balanceSheet.credit[userId]
    }
    else if (balanceSheet.debit[userId] <= amount) {
        amount -= balanceSheet.debit[userId]
        delete balanceSheet.debit[userId]
        balanceSheet.credit[userId] = amount
    }
    balanceSheet.markModified('credit')
    balanceSheet.markModified('debit')
}

module.exports = {
    makeCreditEntry,
    makeDebitEntry
}