function debug() {
    if ( process.env.NODE_ENV !== 'development' ) return
    var args = Array.prototype.slice.call(arguments)
    console.log.apply(console, args)
}

module.exports = debug