const addToArray = (element, array = []) => {
    array.indexOf(element) < 0 && array.push(element)
    return array
}

const removeFromArray = (element, array = []) => {
    let index = array.indexOf(element)
    index >= 0 && array.splice(index, 1)
    return array
}

module.exports = {
    addToArray,
    removeFromArray
}