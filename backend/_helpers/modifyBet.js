let _       = require('lodash')

const secureBet = bet => {
    delete bet.parents
    if(!bet.directChildId) bet.directChildId = bet.userId
    if(!bet.directChildName) bet.directChildName = bet.username
    return bet
}

const modifyBetForParent = (user, bet) => {
    bet.directChildName  = user.username
    bet.directChildId    = user.userId
}

const modifyBet = (userId, betToModify) => {
    let bet = betToModify.toObject
        ? betToModify.toObject()
        : _.extend({}, betToModify)
    let parentIndex = bet.parents.findIndex( parent => {
        return ''+parent.userId === ''+userId
    })
    let length = bet.parents.length
    if(length === 0) return secureBet(bet)
    if( (parentIndex === -1) && (''+bet.userId !== userId) ){
        return console.log('inappropriate user asked for bet')
    }
    if ( !((''+bet.userId === userId) || (parentIndex === length - 1)) ){
        modifyBetForParent(bet.parents[parentIndex+1], bet)
    }
    secureBet(bet)
    return bet
}

module.exports = modifyBet
// modify bet according to the given user.
