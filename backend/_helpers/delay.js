const delay = ms => new Promise( (resolve, reject) => {
    if (typeof ms !== 'number' || isNaN(ms)){
        reject(new Error('Please provide milli-seconds in number'))
    } 
    setTimeout(resolve, ms)   
})

module.exports = delay