const disconnectUsers = (userId, io) => {
    let room = io.sockets.adapter.rooms[userId]
    if (!room) return
    let sockets = room.sockets
    for (let clientId in sockets) {
        let client = io.sockets.connected[clientId] 
        client && client.disconnect()
    }
}

module.exports = disconnectUsers
