let master = require('_api/socket/master')

const getReceivers = user => {
    let selfId = user._id || user.userId
    let receivers = [selfId]
    user.parents.forEach( parent => receivers.push(parent.userId) )
    return receivers
}

const notifyUserUpdate = (user, event) => {
    let receivers = getReceivers(user)
    user = (user.toObject && user.toObject()) || user
    delete user.password
    user.userId = user._id
    master.emit('NOTIFY', {
        receivers,
        event: event,
        data: user
    })
}


module.exports = notifyUserUpdate
