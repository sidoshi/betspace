let User = require('_models/user')

module.exports = async (mainAdminId, marketId=null) => {
    let mainAdmin = await User.findById(mainAdminId)
    let limitedMarkets = mainAdmin.toObject().limitedMarkets
    let marketLimits = limitedMarkets[marketId] || {
        min: 0,
        max: Infinity
    }
    return marketLimits
}