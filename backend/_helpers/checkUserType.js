let _ = require('lodash')

const checkUserType = (req, res, userType) => {
    if (req.user.userType !== userType) {
        let url = _.kebabCase(req.user.userType)
        res.redirect(`/dashboard/${url}`)
        return false
    }
    return true
}

module.exports = checkUserType