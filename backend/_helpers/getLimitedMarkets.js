let User = require('_models/user')

const findLimitedMarket = (limitedMarkets, marketId) => {
    let index = limitedMarkets.findIndex( m => m.marketId === marketId)
    if (index < 0) return {marketId, min: 0, max: Infinity}
    return limitedMarkets[index]
}

module.exports = async (mainAdminId, marketId=null) => {
    let mainAdmin = await User.findById(mainAdminId)
    let limitedMarkets = mainAdmin.toObject().limitedMarkets
    return marketId === null
        ? limitedMarkets
        : findLimitedMarket(limitedMarkets, marketId)
}