#!/bin/bash

clear
echo SLAVE
NODE_ENV=test mocha slave/test  || echo
clear
echo MASTER
NODE_ENV=test mocha master/test || echo