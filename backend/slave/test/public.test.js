require('app-module-path').addPath(`${__dirname} /../`)
require('app-module-path').addPath(__dirname + '/../../')

let clear = require('clear')
let User = require('_models/user')
let expect = require('chai').expect
let app = require('app')
let request = require('supertest').agent(app)
let hash = require('sha256')
let mongoose = require('mongoose')
let cookie = require('cookie')
let decode = require('jwt-decode')
let config = require('_config')

clear()
let env = process.env.NODE_ENV

const getUser = () => new User({
    username: "testuser",
    password: hash('password'),
    userType: "MAIN ADMIN",
    blocked: false,
    parents: [],
    directParent: null,
    children: [],
    balance: 25000,
    ipAddress: [],
    availableShare: 100
})

before('connect to test database', async () => {
    if (mongoose.connection.readyState === 0) {
        await mongoose.connect(config.mongo.url[env], config.mongo.options)
    }
})

describe('public api', () => {
    let decoded
    before('create a test user', async () => {
        let user = getUser()
        await mongoose.connection.db.dropDatabase()
        await user.save()
    })
    after('Clear database', async () => {
        await mongoose.connection.db.dropDatabase()
    })
    it('can give a login page on GET /', (done) => {
        request
            .get('/')
            .expect(200)
            .expect('Content-Type', /html/)
            .end((err, res) => {
                if (err) return done(err)
                done()
            })
    }).timeout(5000)

    it('can login a user on correct username/password', (done) => {
        request
            .post('/login')
            .send({username: 'testuser', password: 'password'})
            .expect(200)
            .expect('set-cookie', /token/)
            .end( (err, res) => {
                if (err) return done(err)
                expect(res.text).to.equal('login success')
                let {token} = cookie.parse(res.header['set-cookie'][0])
                decoded = decode(token)
                done()
            })
    })

    it('can logout a logged in user', (done) => {
        request
            .post('/logout')
            .set('Csrf-Token', decoded.csrfToken)
            .expect(302) // redirect found status
            .expect('set-cookie', /token=;/) //deletes the cookie
            .end( (err, res) => {
                if (err) return done(err)
                expect(res.text).to.equal('Found. Redirecting to /')
                done()
            })
    })
})