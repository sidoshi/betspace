require('app-module-path').addPath(`${__dirname} /../`)
require('app-module-path').addPath(__dirname + '/../../')

let sinon = require('sinon')
let clear = require('clear')
let mongoose = require('mongoose')
let expect = require('chai').expect

let placeBet = require('_lib/apiFunctions/placeBet')
let config = require('_config')
let Hierarchy = require('_models/hierarchy')
let User = require('_models/user')
let hash = require('sha256')
let redisClient = require('_lib/redisClient')
let markets = require('./objects/markets.json')

clear()
let env = process.env.NODE_ENV

let user = new User({
    username: "testuser",
    password: hash('password'),
    userType: "CLIENT",
    blocked: false,
    parents: [],
    directParent: null,
    children: [],
    balance: 10000,
    ipAddress: [],
    credit: 10000,
    bettingBalance: 10000,
    exposure: 0,
    profitAndLoss: 0,
    availableShare: 0
})

let mainAdmin = new User({
    username: 'mainAdmin',
    password: hash('password'),
    userType: 'MAIN ADMIN',
    blocked: false,
    parents: [],
    directParent: null,
    children: [],
    balance: 10000,
    ipAddress: [],
    credit: 10000,
    bettingBalance: 10000,
    exposure: 0,
    profitAndLoss: 0,
    availableShare: 0,
})


const createMainAdminHierarchy = async mainAdmin => {
    let hierarchy = {
        endpoint: mainAdmin._id,
        users: [mainAdmin._id],
        shares: {
            [mainAdmin._id]: 100
        }
    }
    await new Hierarchy(hierarchy).save()
}

const getBet = () => {
    let marketId = markets['Cricket'][0]
    let market = markets[marketId]
    let runnerId = market.sortedRunners[0]
    let runner = market.runners[runnerId]
    let rate = runner.availableToBack[0].price
    return {
       stake: 1000,
       rate: rate,
       betType: 'BACK',
       runnerId: runnerId,
       marketId: marketId,
       username: user.username,
       userId: user._id,
       userType: user.userType,
       ipAddress: '127.0.0.1',
       marketName: market.name,
       marketType: market.type,
       runnerName: runner.name,
       runnerStatus: runner.status,
       marketStatus: market.status,
       matched: true,
       result: 'PENDING'
   }
}

let socketEmitter, masterEmitter

let socket = {
    decoded_token: {
        username: 'testuser',
        userId: user._id,
        userType: 'MAIN ADMIN'
    },
    request: {
        connection: { remoteAddress: '127.0.0.1' }
    },
    emit: socketEmitter
}


let master = {
    decoded_token: {
        username: 'testuser',
        userId: '58d0ddf892380e3cb3957e17',
        userType: 'MAIN ADMIN'
    },
    request: {
        connection: { remoteAddress: '127.0.0.1' }
    },
    connected: true,
    emit: masterEmitter
}



before('connect to test database', async () => {
    if(mongoose.connection.readyState === 0){
        await mongoose.connect(config.mongo.url[env], config.mongo.options)
    }
    await mongoose.connection.db.dropDatabase()
    mainAdmin = await mainAdmin.save()
    user.parents = [{
        userId: mainAdmin._id,
        userType: mainAdmin.userType,
        username: mainAdmin.username
    }]
    user = await user.save()
    await createMainAdminHierarchy(user)
    await redisClient.setAsync('markets', JSON.stringify(markets))
})
after('clear database', async () => {
    await mongoose.connection.db.dropDatabase()
    await redisClient.setAsync('markets', JSON.stringify(null))
})

beforeEach( () => {
    socketEmitter = sinon.spy()
    masterEmitter = sinon.spy()
    socket.emit = socketEmitter
    master.emit = masterEmitter
})

it('places a bets properly when there is sufficient balance', async () => {
    let bet = getBet()
    let {rate} = bet
    await placeBet(socket, master, bet)
    bet = masterEmitter.args[0][1]
    expect(bet).to.be.an.object
    expect(bet.exposure).to.be.equal(1000)
    expect(bet.profit).to.equal( (rate - 1) * 1000 )
    expect(masterEmitter.args[0][0]).to.equal('PLACE_BET')
    bet.betType = 'LAY'
    await placeBet(socket, master, bet)
    bet = masterEmitter.args[0][1]
    expect(bet).to.be.an.object
    expect(bet.exposure).to.be.equal((rate - 1) * 1000)
    expect(bet.profit).to.equal(1000)
    expect(masterEmitter.args[0][0]).to.equal('PLACE_BET')
})


/* eslint no-unused-expressions: "off" */