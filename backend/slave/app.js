require('app-module-path').addPath(`${__dirname} /../`)
require('app-module-path').addPath(__dirname)
require('waitjs')
let express         = require('express')
let path            = require('path')
let favicon         = require('serve-favicon')
let logger          = require('morgan')
let cookieParser    = require('cookie-parser')
let bodyParser      = require('body-parser')
let helmet          = require('helmet')
let mongoose        = require('mongoose')

let app             = require('_api/socket/io').app
let server          = require('_api/socket/io').server

let index           = require('_api/routes')
let users           = require('_api/routes/users')
let dashboard       = require('_api/routes/dashboard')
let betfair         = require('_api/routes/betfair')
let errors          = require('_helpers/errors')
let createSocketApi = require('_api/socket').createSocketApi
let config          = require('_config')
let redisClient     = require('_lib/redisClient')
global.debug        = require('_helpers/debug')

let secret          = config.jwt.secret

try{
  app.settings.env === 'production' && require('log-timestamp')
  createSocketApi()
  mongoose.Promise = global.Promise
  if (app.settings.env !== 'test') {
    mongoose.connect(config.mongo.url[app.get('env')], config.mongo.options)
  }
}catch(err){
  console.log(err)
  if (app.settings.env !== 'test') {
    mongoose.connect(config.mongo.url[app.get('env')], config.mongo.options)
  }
}

// view engine setup
app.set('views', path.join(__dirname, '_views'))
app.set('view engine', 'pug')

app.use(helmet())
app.use(favicon(path.join(__dirname, '_static/public', 'betspace.png')))
app.get('env') === 'development' && app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser(secret))
app.use(express.static(path.join(__dirname, '_static/public')))
app.use('/admin', express.static(path.join(__dirname, '_static/admin')))
app.use('/client', express.static(path.join(__dirname, '_static/client')))
app.enable('trust proxy')

const limiter = require('express-limiter')(app, redisClient)

// Limit requests to 150 per hour per ip address.
let limit = limiter({
  lookup: ['ip'],
  total: 150,
  expire: 1000 * 60 * 60,
  onRateLimited: () => debug('rate limited')
})

app.use('/', limit, index)
app.use('/dashboard', limit, dashboard)
app.use('/api/users', limit, users)
app.use('/api/betfair', limit, betfair)

// catch 404 and forward to error handler
app.use(errors.catchError)
app.use(errors.handleError)


server.listen(config.slave.port)
server.on('listening', () => {
  let addr = server.address()
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port
  debug('Listening on ' + bind)
})

module.exports = app
