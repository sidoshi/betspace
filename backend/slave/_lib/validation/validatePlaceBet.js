const possibleBetTypes = ['BACK', 'LAY']

const validatePlaceBet = (bet, markets) => {
    if(!bet.marketId) return
    let market  = markets[bet.marketId]
    if ( !market ) return false
    if ( market.status !== 'OPEN' ) return false
    if ( !market.runners ) return false
    if ( !bet.runnerId ) return false
    let runner = market.runners[bet.runnerId]
    if ( possibleBetTypes.indexOf(bet.betType) < 0 ) return false
    if ( bet.stake <= 0  ) return false
    if ( typeof bet.stake !== 'number' ) return false
    if ( typeof bet.rate  !== 'number' ) return false
    bet.marketName      = market.name
    bet.marketType      = market.type
    bet.runnerName      = runner.name
    bet.runnerStatus    = runner.status
    bet.marketStatus    = market.status
    bet.matched         = false
    bet.result          = 'PENDING'
    return true
}

module.exports = validatePlaceBet
