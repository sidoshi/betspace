let User = require('_models/user')
let master = require('_api/socket/master')

module.exports = async (req, res) => {
    try{
        if(!req.body.userId)
            throw new Error('You need to provide a marketId')
        let min = +req.body.min || 0
        let max = +req.body.max || Infinity
        if (req.body.max === 0 || req.body.max === '0') max = 0
        let userId = req.body.userId
        let user = await User.findById(req.body.userId)
        if (!user)
            throw new Error('No user found')
        if (user.userType !== 'CLIENT')
            throw new Error('You can only limit Client users')
        let directParentId = ''+user.toObject().directParent.userId
        if (directParentId !== req.user.userId)
            throw new Error('You need to be direct parent of user to limit')
        if (isNaN(+min) || isNaN(+max)) 
            throw new Error('Please provide a valid number')
        if (max <= min) 
            throw new Error('Max limit should be more than min')
        let userLimit = user.toObject().userLimit || {}
        userLimit = {min, max}
        user.userLimit = userLimit
        user = await user.save()
        res.end('User Limited')
        let receivers = [
            userId, 
            user.directParent.userId, 
            user.grandParent.userId
        ]
        let data = {[userId]: {min: min, max: max}}
        master.emit('NOTIFY', {
            receivers,
            event: 'ADD_USER_LIMIT',
            data
        })
    }catch(err){
        res.status(400).end(err.message)
    }
}