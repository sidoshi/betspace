let redisClient         = require('_lib/redisClient')

const getActiveMarkets = async (req, res) => {
    try {
        let markets = await redisClient.getAsync('markets')
        res.end(markets)
    } catch (err) {
        debug(err)
        res.status(400).end(err.message)
    }
}

module.exports = getActiveMarkets