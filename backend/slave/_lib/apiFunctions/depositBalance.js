let User                = require('_models/user')
let notifyUserUpdate    = require('_helpers/notifyUserUpdate')
let updateBalanceSheet  = require('_helpers/updateBalanceSheet')
let Transaction         = require('_models/transaction')
let BalanceSheet        = require('_models/balanceSheet')

const validateDepositBalance = (user,body) => {
    if (isNaN(+body.amount) || !body.amount) throw new Error('No amount given')
    if (body.amount <= 0) throw new Error('Amount should be more than 0')
    if (!body.userId) throw new Error('No user given')
    if (user.children.indexOf(body.userId) < 0) {
        throw new Error('You dont have rights')
    }
    if (user.balance < body.amount) {
        throw new Error('Insufficient balance')
    }
}

const getOwedToUs = async (parent, child) => {
    let balanceSheet = await BalanceSheet.findOne({ userId: parent._id })
    return balanceSheet.debit[child._id] || 0
}

const depositBalance = async (req, res) => {
    try{
        let user = await User.findById(req.user.userId)
        validateDepositBalance(user, req.body)
        let child = await User.findById(req.body.userId)
        let owedToUs = await getOwedToUs(user, child)
        let amount = +req.body.amount
        if (amount > owedToUs) 
            throw new Error('Can only deposit debited amount')
        child.balance += amount
        child.profitAndLoss += amount
        if (child.userType === 'CLIENT') {
            child.bettingBalance = child.balance - child.exposure
        }
        user.balance -= amount
        user.profitAndLoss -= amount
        let parent = user
        await updateBalanceSheet({
            entryType: 'DEPOSIT', 
            amount: amount,
            parent,
            child
        })
        user = await user.save()
        child = await child.save()
        await new Transaction({
            transactionType: 'DEPOSIT',
            parent: {
                username: parent.username,
                userId: parent._id
            },
            child: {
                username: child.username,
                userId: child._id
            },
            amount: amount,
            ipAddress: '' + req.ip
        }).save()
        notifyUserUpdate(user, 'UPDATE_USER')
        notifyUserUpdate(child, 'UPDATE_USER')
        res.json(`Deposited ${amount} to ${child.username}`)
    }catch(err){
        console.log(err)
        res.status(400).end(err.message)
    }
}

module.exports = depositBalance

// save this transaction somewhere