let User = require('_models/user')
let redisClient = require('_lib/redisClient')
let master = require('_api/socket/master')

module.exports = async (req, res) => {
    try{
        if(req.user.userType !== 'MAIN ADMIN')
            throw new Error('Only MAIN ADMIN can limit markets')
        if(!req.body.marketId)
            throw new Error('You need to provide a marketId')
        let min = +req.body.min || 0
        let max = +req.body.max || Infinity
        if (req.body.max === 0 || req.body.max === '0') max = 0
        let marketId = req.body.marketId
        let markets = await redisClient.getAsync('markets')
        markets = JSON.parse(markets)
        if (!markets[marketId]) 
            throw new Error('Please provide a vaild marketId')
        if (isNaN(+min) || isNaN(+max)) 
            throw new Error('Please provide a valid number')
        if (max <= min) 
            throw new Error('Max limit should be more than min')
        let user = await User.findById(req.user.userId)
        let limitedMarkets = user.toObject().limitedMarkets || []
        let index = limitedMarkets.findIndex( m => m.marketId === marketId )
        index >= 0 && limitedMarkets.splice(index, 1)
        limitedMarkets.push({
            marketId,
            min,
            max
        })
        user.limitedMarkets = limitedMarkets
        user = await user.save()
        res.end('Market Limited')
        let receivers = await User.find({
            $or: [
                { 'parents.userId': req.user.userId },
                { _id: req.user.userId }
            ]
        }, '_id')
        receivers = receivers.map(receiver => receiver._id)
        let data = {[marketId]: {min: min, max: max}}
        master.emit('NOTIFY', {
            receivers,
            event: 'ADD_MARKET_LIMIT',
            data
        })
    }catch(err){
        res.status(400).end(err.message)
    }
}