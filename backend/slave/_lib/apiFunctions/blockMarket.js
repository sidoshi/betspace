let User = require('_models/user')
let redisClient = require('_lib/redisClient')
let {addToArray} = require('_helpers/arrayUtils')
let master = require('_api/socket/master')


module.exports = async (req, res) => {
    try{
        if (req.user.userType === 'CLIENT') 
            throw new Error('Client can\'t block market')
        let markets = await redisClient.getAsync('markets')
        markets = JSON.parse(markets)
        if (!req.body.marketId)
            return res.status(400).end('Please Provide a marketId')
        if (!markets[req.body.marketId])
            return res.status(400).end('Invalid market Id provided')
        if (req.user.userType === 'CLIENT')
            return res.status(400).end('Client can not block market')
        let user = await User.findById(req.user.userId)
        let marketToBlock = req.body.marketId
        addToArray(marketToBlock, user.blockedMarkets)
        await user.save()
        res.end('Market Blocked')
        let receivers = await User.find({
            $or: [
                { 'parents.userId': req.user.userId },
                { _id: req.user.userId }
            ]
        }, '_id')
        receivers = receivers.map(receiver => receiver._id)
        master.emit('NOTIFY', {
            receivers,
            event: 'MARKET_BLOCKED',
            data: {[req.user.userId]: [req.body.marketId]}
        })
    }catch(err){
        res.status(400).end(err.message)
    }
}

