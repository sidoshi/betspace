let getLimitedMarkets = require('_helpers/getLimitedMarkets')
let User = require('_models/user')

const findMainAdminId = async userId => {
    let user = await User.findById(userId)
    return user.toObject().parents[0].userId
}

module.exports = async (req, res) => {
    try{
        let mainAdminId = req.user.userType === 'MAIN ADMIN'
            ? req.user.userId
            : await findMainAdminId(req.user.userId)
        let limitedMarkets = await getLimitedMarkets(mainAdminId)
        res.json(limitedMarkets)
    }catch(err){
        res.status(400).end(err.message)
    }
}