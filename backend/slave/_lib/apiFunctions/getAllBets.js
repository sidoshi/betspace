let Bet = require('_models/bet')

const getAllBets = async (req, res) => {
    try {
        let skip = req.body.skip || 0
        let limit = 20
        let bets = await Bet.find({
            $or: [
                { 'parents.userId': req.user.userId },
                { userId: req.user.userId }
            ],
        })
        .sort('-createdAt')
        .skip(skip)
        .limit(limit)
        return res.json(bets)
    }catch(err){
        return res.status(400).end(err.message)
    }
}

module.exports = getAllBets