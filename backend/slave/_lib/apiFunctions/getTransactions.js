let Transaction = require('_models/transaction')

module.exports = async (req, res) => {
    try {
        let skip = req.body.skip || 0
        let limit = 20
        let transactions = await Transaction.find({
            $or: [
                { 'parent.userId': req.user.userId },
                { 'child.userId': req.user.userId }
            ]
        })
        .sort('-createdAt')
        .skip(skip)
        .limit(limit)
        res.json(transactions)
    }catch(err){
        res.status(400).end(err.message)
    }
}