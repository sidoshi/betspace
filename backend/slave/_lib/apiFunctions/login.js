let User            = require('_models/user')  
let hash            = require('sha256')
let uuid            = require('node-uuid')
let secret          = require('_config').jwt.secret
let jwt             = require('jsonwebtoken')
let maxAge          = require('_config').cookie.maxAge

let cookieConfig = {
    maxAge: maxAge
}

const validateUserBeforeLogin = (user,body) => {
    if (!user) {
        throw new Error('Invalid username or password')
    }
    if (user.blocked) {
        throw new Error('You are blocked')
    }
    if (!body.password) throw new Error('No Password')
    let hashed = hash(body.password)
    if (user.password !== hashed) {
        throw new Error('Invalid username or password')
    }
}

const getClaim = user => {
    let csrfToken = uuid.v4()
    let claim = {
        username: user.username,
        userId: user._id,
        userType: user.userType,
        csrfToken: csrfToken
    }
    return claim
}

const getToken = claim => new Promise((resolve, reject) => {
    jwt.sign(claim, secret, { expiresIn: '8h' }, (err, token) => {
        resolve(token)
    })
}) 

const login = async (req, res) => {
    try {
        // get user and validate
        let user = await User.findOne({ username: req.body.username })
        validateUserBeforeLogin(user,req.body)
        // get the ip address of user and store it if it dosen't exist
        let index = user.ipAddress.indexOf(req.ip)
        index < 0 && user.ipAddress.push(req.ip)
        user.save()
        let claim = getClaim(user)
        let token = await getToken(claim)
        res.cookie('token', token, cookieConfig)
        res.end('login success')
    } catch (err) {
        res.status(400).end(err.message)
    }
}

module.exports = login