let redisClient = require('_lib/redisClient')

const subscribeMarket = async (socket, master, marketId) => {
    try {
        let markets = await redisClient.getAsync('markets')
        markets = JSON.parse(markets)
        if (markets[marketId]) socket.join(marketId)
    } catch (err) {
        return
    }
}

module.exports = subscribeMarket