let BalanceSheet = require('_models/balanceSheet')

module.exports = async (req, res) => {
    try {
        let balanceSheet = await BalanceSheet.findOne({
            userId: req.user.userId
        })
        return res.json(balanceSheet)
    } catch (err) {
        return res.status(400).end(err.message)
    }
}
