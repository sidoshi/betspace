const cancelBet = (socket, master, bet) => {
    if (bet.userId !== socket.decoded_token.userId) {
        socket.emit('CANCEL_BET_FAILED', bet)
        return
    }
    master.emit('CANCEL_BET', bet)
}

module.exports = cancelBet