let User                = require('_models/user')
let Hierarchy           = require('_models/hierarchy')
let redisClient         = require('_lib/redisClient')
let getBlockedMarkets   = require('_helpers/getBlockedMarkets')
let getLimitedMarkets   = require('_helpers/getLimitedMarkets')
let { toFixed }         = require('_helpers/numberUtils')

const possibleBetTypes = ['BACK', 'LAY']

const Throw = message => {
    throw new Error(message)
}

const validatePlaceBet = async (user, bet, markets) => {
    if (!user) Throw('No user found')
    if (user.userType !== 'CLIENT') Throw('Invalid user type')
    if (!bet.marketId) Throw('No market Id found')
    let market = markets[bet.marketId]
    if (!market) Throw('No market found')
    if (market.status !== 'OPEN') Throw('Market Closed')
    if (!market.runners) Throw('Runners not found')
    if (!bet.runnerId) Throw('Bet does not have runnerId')
    if (possibleBetTypes.indexOf(bet.betType) < 0) Throw('Invalid bet type')
    if (bet.stake <= 0) Throw('Stake should be more than 0')
    if (typeof bet.stake !== 'number') Throw('Stake should be a number')
    if (typeof bet.rate !== 'number') Throw('rate should be a number')
    let blockedMarkets = await getBlockedMarkets(user._id)
    if (blockedMarkets.indexOf(bet.marketId) >= 0) Throw('Market is blocked')
    let mainAdminId = user.toObject().parents[0].userId
    let marketLimits = await getLimitedMarkets(mainAdminId, bet.marketId)
    let min = marketLimits.min || 0
    let max = marketLimits.max || Infinity
    let limitFactor = 'exposure' // it can also be stake
    if (bet[limitFactor] < min) Throw(limitFactor + ' less than minimum limit')
    if (bet[limitFactor] > max) Throw(limitFactor + ' more than maximum limit')
    let userLimit = user.toObject().userLimit || {}
    min = userLimit.min || 0
    max = userLimit.max || Infinity
    if (bet[limitFactor] < min) Throw(limitFactor + ' less than user minimum')
    if (bet[limitFactor] > max) Throw(limitFactor + ' more than user maximum')
}

const addUserInfo = (bet, socket, user) => {
    bet.username = socket.decoded_token.username
    bet.userId = socket.decoded_token.userId
    bet.userType = socket.decoded_token.userType
    bet.ipAddress = socket.request.connection.remoteAddress
    bet.parents = user.parents
}

const addMarketInfo = (bet, markets) => {
    let market = markets[bet.marketId]
    let runner = market.runners[bet.runnerId]
    bet.marketName = market.name
    bet.marketType = market.type
    bet.runnerName = runner.name
    bet.runnerStatus = runner.status
    bet.marketStatus = market.status
    bet.matched = false
    bet.result = 'PENDING'
}

const addShareInfo = async (bet, user) => {
    let hierarchy = await Hierarchy.findOne({ endpoint: bet.userId })
    let shares = hierarchy.toObject().shares
    bet.shares = shares
}

const getProfitAndExposure = bet => {
    if (bet.betType === 'BACK') {
        bet.profit = (bet.stake * bet.rate) - bet.stake
        bet.exposure = bet.stake
    } else {
        bet.profit = bet.stake
        bet.exposure = (bet.stake * bet.rate) - bet.stake
    }
    bet.profit = toFixed(bet.profit)
    bet.exposure = toFixed(bet.exposure)
}

const placeBet = async (socket, master, bet) => {
    try {
        let user = await User.findById(socket.decoded_token.userId)
        addUserInfo(bet, socket, user)
        await addShareInfo(bet, user)
        getProfitAndExposure(bet)
        let markets = await redisClient.getAsync('markets')
        markets = JSON.parse(markets)
        await validatePlaceBet(user, bet, markets)
        addMarketInfo(bet, markets)
        if (!master.connected) throw new Error('Not connected to master')
        master.emit('PLACE_BET', bet)
    } catch (err) {
        socket.emit('PLACE_BET_FAILED', err.message)
    }
}

module.exports = placeBet