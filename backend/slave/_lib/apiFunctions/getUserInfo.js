let User    = require('_models/user')

const getUserInfo = async (req, res) => {
    try{
        let user = await User.findById(req.user.userId)
        if (!user) throw new Error('No user found')
        user = user.toObject()
        delete user.password
        res.json(user)
    } catch(err){
        res.status(400).end(err.message)
    }
}

module.exports = getUserInfo