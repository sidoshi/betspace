let notifyUserUpdate    = require('_helpers/notifyUserUpdate')
let User                = require('_models/user')

const unblockChildrens = async (user) => {
    let users = await User.find({ 'parents.userId': user._id })
    users.forEach(async (user) => {
        user.parentBlocked = false
        user = await user.save()
        notifyUserUpdate(user, 'UPDATE_USER')
    })
}

const validateUserBeforeUnblock = (user, body) => {
    if (!user) throw new Error('new user found')
    if (!body.userId) throw new Error('no user given')
    let userToBlock = body.userId
    let index = user.children.indexOf(userToBlock)
    if (index < 0) {
        throw new Error('You dont have rights')
    }
}

const unblockUser = async (req, res, next) => {
    try{

        let user = await User.findById(req.user.userId)
        validateUserBeforeUnblock(user, req.body)

        //get user to unblock
        user = await User.findById(req.body.userId)
        if (!user) throw new Error('No user found')
        user.blocked = false
        user = await user.save()

        // notify clients and block childrens
        notifyUserUpdate(user, 'UPDATE_USER')
        unblockChildrens(user)

        // give success response
        res.json(`user: ${user.username} unblocked`)
    }catch(err){
        res.status(400).end(err.message)
    }
}

module.exports = unblockUser