let notifyUserUpdate    = require('_helpers/notifyUserUpdate')
let User                = require('_models/user')
let master              = require('_api/socket/master')

const blockChildrens = async (user) => {
    let users = await User.find({ 'parents.userId': user._id })
    users.forEach(async (user) => {
        user.parentBlocked = true
        user = await user.save()
        notifyUserUpdate(user, 'UPDATE_USER')
        master.emit('SHARE', {
            event: 'DISCONNECT_USER',
            data: user._id
        })
    })
}

const validateUserBeforeBlock = (user,body) => {
    if (!user) throw new Error('new user found')
    if (!body.userId) throw new Error('no user given')
    let userToBlock = body.userId
    let index = user.children.indexOf(userToBlock)
    if (index < 0) {
        throw new Error('You dont have rights')
    }
}

const blockUser = async (req, res, next) => {
    try{

        let user = await User.findById(req.user.userId)
        validateUserBeforeBlock(user, req.body)

        // get user to block
        user = await User.findById(req.body.userId)
        if (!user) throw new Error('No user found')
        user.blocked = true
        user = await user.save()

        // notify clients and block childrens and disconnect user if connected
        notifyUserUpdate(user, 'UPDATE_USER')
        blockChildrens(user)
        master.emit('SHARE', {
            event: 'DISCONNECT_USER',
            data: user._id
        })

        // give success responce
        res.json(`user: ${user.username} blocked`)
    }catch(err){
        res.status(400).end(err.message)
    }
}

module.exports = blockUser