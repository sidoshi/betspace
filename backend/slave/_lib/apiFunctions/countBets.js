let Bet = require('_models/bet')

let countBets = async (req, res) => {
    try{
        let count = await Bet.find({
            $or: [
                { 'parents.userId': req.user.userId },
                { userId: req.user.userId }
            ],
        }).count()
        return res.json(count)
    }catch(err){
        res.status(400).end(err.message)
    }
}
module.exports = countBets