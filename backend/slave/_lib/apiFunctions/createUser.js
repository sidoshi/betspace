let hash                = require('sha256')
let validator           = require('validator')

let userTypes           = require('_lib/objects').userTypes
let userAccessRights    = require('_lib/objects').userAccessRights
let User                = require('_models/user')
let Hierarchy           = require('_models/hierarchy')
let BalanceSheet        = require('_models/balanceSheet')
let notifyUserUpdate    = require('_helpers/notifyUserUpdate')
let Transaction = require('_models/transaction')

const createHierarchy = async child => {
    // there will be only one hierarchy for one endpoint
    let parentHierarchy = await Hierarchy.findOne({
        endpoint: child.directParent.userId
    })
    // users of hierarchy consists all users in the hierarchy
    // so add new created user to all users of parent
    let users = parentHierarchy.users
    users.push(child._id) 
    // get the shares of parent hierarchy
    // deduct the share parent allocated to child from parent
    // set child's available share
    let shares = parentHierarchy.shares
    shares[parentHierarchy.endpoint] -= child.availableShare
    shares[child._id] = child.availableShare
    let hierarchy = {
        endpoint: child._id,
        users: users,
        shares: shares
    }
    await new Hierarchy(hierarchy).save()
}

const createBalanceSheet = async userId => {
    let balanceSheet = {
        userId,
        debit: {},
        credit: {}
    }
    await new BalanceSheet(balanceSheet).save()
}

const getParentAndChild = async (user,body) => {
    let parent = await User.findById(user.userId)
    parent = parent.toObject()
    parent.userId = parent._id
    let child = {
        username: body.username,
        password: body.password,
        userType: body.userType,
        credit: body.credit || 0,
        availableShare: body.availableShare || 0
    }
    body.name && (child.name = body.name)
    if (child.userType === 'CLIENT') child.availableShare = 0
    return [parent,child]
}

const validateChild = child => {
    if (!validator.isAlphanumeric(child.username)) return false
    if (child.username.length < 3) return false
    if (!validator.isAlphanumeric(child.password)) return false
    if (child.password.length < 5) return false
    if (userTypes.indexOf(child.userType) < 0) return false
    if (isNaN(Number(child.credit))) return false
    return true
}

const validateCreateUser = async (parent,child) => {
    let rights = userAccessRights[parent.userType].indexOf(child.userType)
    if (rights < 0) {
        throw new Error('You cant create this type of user')
    }
    if (child.availableShare > parent.availableShare){
        throw new Error('You dont have enough available share')
    }
    if (!validateChild(child)) {
        throw new Error('Invalid user provided')
    }
    let user = await User.findOne({ username: child.username })
    if (user) throw new Error('Username exists')
}


const updateParent = async (parent,child) => {
    let user = await User.findById(parent.userId)
    user.children.push(child._id)
    let credit = +child.credit
    if (isNaN(credit)) throw new Error('Credit must be a number')
    if (credit > +user.balance)
        throw new Error('You dont have enough balance')
    user.balance -= credit
    user.childCredit += credit
    await user.save()
    notifyUserUpdate(user, 'UPDATE_USER')
    return user
}


const createUser = async (req, res) => {
    try{
        // extract parent child and validate
        let [parent, child] = await getParentAndChild(req.user, req.body)
        await validateCreateUser(parent,child)
        // create new user and hash password and validate with schema
        let newUser = new User(child)
        newUser.password = hash(newUser.password)
        let errs = newUser.validateSync()
        if (errs) throw new Error('Error validating user')
        child.userId = newUser._id
        // update child with parents
        newUser.parents = parent.parents
        newUser.parents.push(parent) // add current user to users parent
        newUser.directParent = parent // current user is child's direct parent
        newUser.grandParent = parent.directParent
        newUser.credit = +child.credit
        newUser.balance = +child.credit
        newUser.bettingBalance = child.userType === 'CLIENT' 
            ? +child.credit 
            : 0
        newUser.exposure = 0
        newUser.profitAndLoss = 0
        // create a hierarchy with new user as endpoint
        await createHierarchy(newUser)
        await createBalanceSheet(newUser._id)
        child = await newUser.save()
        await new Transaction({
            transactionType: 'CREATE USER',
            parent: {
                username: parent.username,
                userId: parent._id
            },
            child: {
                username: child.username,
                userId: child._id
            },
            amount: child.credit,
            ipAddress: '' + req.ip
        }).save()
        // update parent with new child
        await updateParent(parent,child)
        child.password && delete child.password
        res.json(child)
        notifyUserUpdate(child, 'ADD_USER')
    } catch(err){
        res.status(400).end(err.message)
    }
}

module.exports = createUser
