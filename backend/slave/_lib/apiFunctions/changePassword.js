let User            = require('_models/user')
let hash            = require('sha256')

const validateChangePassword = (user,body) => {
    if (!user) throw new Error('new user found')
    if (!body.password) throw new Error('no password given')
    if (!body.oldPassword) throw new Error('no old password given')
}

const changePassword = async (req, res) => {
    try{
        // get user and validate
        let user = await User.findById(req.user.userId)
        validateChangePassword(user,req.body)

        // if password matches change password else throw 
        let hashedOld = hash(req.body.oldPassword)
        if (hashedOld === user.password) {
            user.password = hash(req.body.password)
            await user.save()
            res.json('password changed')
        } else {
            throw new Error('invalid old password')
        }
    } catch(err){
        res.status(400).end(err.message)
    }
}

module.exports = changePassword