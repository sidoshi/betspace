let Bet         = require('_models/bet')
let modifyBet   = require('_helpers/modifyBet')

const getActiveBets = async (req, res) => {
    try {
        let bets = await Bet.find({
            $or: [{ 'parents.userId': req.user.userId }, { userId: req.user.userId }],
            marketStatus: 'OPEN'
        })
        let modifiedBets = []
        bets.map(bet => modifiedBets.push(modifyBet(req.user.userId, bet)))
        res.json(modifiedBets)
    } catch (err) {
        res.status(400).end(err.message)
    }
}

module.exports = getActiveBets

