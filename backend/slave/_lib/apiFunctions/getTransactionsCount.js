let Transaction = require('_models/transaction')

module.exports = async (req, res) => {
    try {
        let transactionsCount = await Transaction.find({
            $or: [
                { 'parent.userId': req.user.userId },
                { 'child.userId': req.user.userId }
            ]
        }).count()
        res.json(transactionsCount)
    } catch (err) {
        res.status(400).end(err.message)
    }
}