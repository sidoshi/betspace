let User                = require('_models/user')  
let notifyUserUpdate    = require('_helpers/notifyUserUpdate')

const logout = async (req, res, next) => {
    try {
        let user = await User.findById(req.user.userId)
        user.online = false
        if (user){
            notifyUserUpdate(user, 'UPDATE_USER')
            user.save()
        }
        res.clearCookie('token')
        res.redirect('/')
    } catch (err) {
        res.status(400).end(err.message)
    }
}

module.exports = logout