let User            = require('_models/user')

const getChildren = async (req, res) => {
    try{
        let user = await User.findById(req.user.userId)
        let children = await User.find(
            { _id: { $in: user.children } },
            'username _id userType balance online blocked'
        )
        res.json(children)
    } catch(err){
        res.status(400).end(err.message)
    }
}

module.exports = getChildren