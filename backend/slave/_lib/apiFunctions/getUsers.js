let User = require('_models/user')

const getUsers = async (req, res) => {
    try {
        let users = await User.find({
            $or: [
                { 'directParent.userId': req.user.userId },
                { 'grandParent.userId': req.user.userId },
                { '_id': req.user.userId },
                { 'children': req.user.userId }
            ]
        }, { password: 0 })
        res.json(users)
    } catch (err) {
        debug(err)
        res.status(500).end(err.message)
    }
}

module.exports = getUsers