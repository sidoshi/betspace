let User = require('_models/user')
let notifyUserUpdate = require('_helpers/notifyUserUpdate')
let Transaction = require('_models/transaction')

const validateCreditIn = (body, parent, child) => {
    if (!body.userId || !body.credit) return false
    if (parent.children.indexOf(child._id) < 0) {
        return false
    }
    if (body.credit <= 0) return false
    if (body.credit > parent.balance) return false
    return true
}

const creditIn = async (req, res) => {
    try{
        let [parent, child] = await Promise.all([
            User.findById(req.user.userId),
            User.findById(req.body.userId)
        ])
        if (!parent || !child) return res.status(400).end('User not found')
        if (!validateCreditIn(req.body, parent, child))
            return res.status(400).end('User validation failed')
        let credit = +req.body.credit
        parent.balance -= credit
        parent.childCredit = parent.childCredit || 0
        parent.childCredit += credit
        child.credit += credit
        child.balance = 
            child.credit + (+child.profitAndLoss) - child.childCredit
        if (child.userType === 'CLIENT') {
            child.bettingBalance = child.balance - child.exposure
        }
        await Promise.all([
            parent.save(),
            child.save()
        ])
        await new Transaction({
            transactionType: 'CREDIT IN',
            parent: {
                username: parent.username,
                userId: parent._id
            },
            child: {
                username: child.username,
                userId: child._id
            },
            amount: credit,
            ipAddress: ''+req.ip
        }).save()
        res.end('Credited In')
        notifyUserUpdate(parent, 'UPDATE_USER')
        notifyUserUpdate(child, 'UPDATE_USER')
    }catch(err){
        return res.status(400).end(err.message)
    }
    // save this transaction somewhere
}

module.exports = creditIn