let jwt         = require('jsonwebtoken')
let secret      = require('_config').jwt.secret

const getLoginPage = (req, res) => {
    jwt.verify(req.cookies.token, secret, (err, decoded) => {
        if (err) {
            res.render('index')
            return
        }
        res.redirect('/dashboard')
    })
}

module.exports = getLoginPage