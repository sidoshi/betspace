let getBlockedMarkets   = require('_helpers/getBlockedMarkets')

module.exports = async (req, res) => {
    try{
        let map = true
        let blockedMarkets = await getBlockedMarkets(req.user.userId, map)
        res.json(blockedMarkets)
    }catch(err){
        res.status(400).end(err.message)
    }
}