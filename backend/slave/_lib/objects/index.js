let objects = {
    userAccessRights: {
        'MAIN ADMIN':       ['SUPER MASTER', 'MASTER'],
        'SUPER MASTER':     ['MASTER'],
        'MASTER':           ['DEALER', 'CLIENT'],
        'DEALER':           ['CLIENT'],
        'CLIENT':           []
    },

    userTypes: ['SUPER MASTER', 'MASTER', 'DEALER', 'CLIENT']
}
// wrap objects inside an object to keep track of
// objects even if refrence to that object changes

module.exports = objects
