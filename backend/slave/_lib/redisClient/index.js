let redis       = require('redis')
let Promise     = require('bluebird')

let config =    require('_config')

let client = redis.createClient(config.redis)

module.exports = Promise.promisifyAll(client)
