let express             = require('express')

let expressAuth         = require('_api/middlewares/expressAuth')
let blockedProtect      = require('_api/middlewares/blockedProtect')
let csrfProtect         = require('_api/middlewares/csrfProtect')


let createUser          = require('_lib/apiFunctions/createUser')
let getUsers            = require('_lib/apiFunctions/getUsers')
let withdrawBalance     = require('_lib/apiFunctions/withdrawBalance')
let depositBalance      = require('_lib/apiFunctions/depositBalance')
let getChildren         = require('_lib/apiFunctions/getChildren')
let changePassword      = require('_lib/apiFunctions/changePassword')
let blockUser           = require('_lib/apiFunctions/blockUser')
let unblockUser         = require('_lib/apiFunctions/unblockUser')
let getUserInfo         = require('_lib/apiFunctions/getUserInfo')
let creditIn            = require('_lib/apiFunctions/creditIn')
let creditOut           = require('_lib/apiFunctions/creditOut')
let getTransactions     = require('_lib/apiFunctions/getTransactions')
let getTransactionsCount= require('_lib/apiFunctions/getTransactionsCount')
let getBalanceSheet     = require('_lib/apiFunctions/getBalanceSheet')
let limitUser           = require('_lib/apiFunctions/limitUser')

let router              = express.Router()

router.use(expressAuth)
router.use(blockedProtect)


router.get('/', getUsers)

router.post('/create', csrfProtect, createUser) 

router.post('/credit-in', csrfProtect, creditIn)

router.post('/credit-out', csrfProtect, creditOut)

router.post('/withdraw', csrfProtect, withdrawBalance)

router.post('/deposit', csrfProtect, depositBalance)

router.post('/transactions', getTransactions)

router.get('/balance-sheet', getBalanceSheet)

router.get('/transactions-count', getTransactionsCount)

router.get('/children', getChildren)

router.post('/change-password', csrfProtect, changePassword)

router.post('/block', csrfProtect, blockUser)

router.post('/unblock', csrfProtect, unblockUser)

router.post('/limit-user', csrfProtect, limitUser)

router.get('/user-info', getUserInfo)

module.exports = router
