let express     = require('express')

let csrfProtect                 = require('_api/middlewares/csrfProtect')
let expressAuth                 = require('_api/middlewares/expressAuth')

let getLoginPage                = require('_lib/apiFunctions/getLoginPage')
let login                       = require('_lib/apiFunctions/login')
let logout                      = require('_lib/apiFunctions/logout')

var router      = express.Router()

router.get('/', getLoginPage)

router.post('/login', login)

router.post('/logout', expressAuth, csrfProtect, logout)

module.exports = router
