let express     = require('express')
let _           = require('lodash')

let expressAuth         = require('_api/middlewares/expressAuth')
let blockedProtect      = require('_api/middlewares/blockedProtect')
let checkUserType       = require('_helpers/checkUserType')

let router      = express.Router()

router.use(expressAuth)
router.use(blockedProtect)

router.get('/', (req, res) => {
    let url = _.kebabCase(req.user.userType)
    res.redirect(`/dashboard/${url}`)
})

router.get('/main-admin', (req,res) => {
    checkUserType(req,res,'MAIN ADMIN') &&
    res.render('admin')
})

router.get('/super-master', (req,res) => {
    checkUserType(req, res, 'SUPER MASTER') &&
    res.render('admin')
})

router.get('/master', (req,res) => {
    checkUserType(req, res, 'MASTER') &&
    res.render('admin')
})

router.get('/dealer', (req,res) => {
    checkUserType(req, res, 'DEALER') &&
    res.render('admin')
})

router.get('/client', (req,res) => {
    checkUserType(req, res, 'CLIENT') &&
    res.render('client')
})


module.exports = router
