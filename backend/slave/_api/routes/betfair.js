let express             = require('express')

let expressAuth         = require('_api/middlewares/expressAuth')
let blockedProtect      = require('_api/middlewares/blockedProtect')
let csrfProtect         = require('_api/middlewares/csrfProtect')

let getActiveBets       = require('_lib/apiFunctions/getActiveBets')
let getActiveMarkets    = require('_lib/apiFunctions/getActiveMarkets')
let getAllBets          = require('_lib/apiFunctions/getAllBets')
let countBets           = require('_lib/apiFunctions/countBets')
let blockMarket         = require('_lib/apiFunctions/blockMarket')
let unblockMarket       = require('_lib/apiFunctions/unblockMarket')
let getBlockedMarkets   = require('_lib/apiFunctions/getBlockedMarkets')
let limitMarket         = require('_lib/apiFunctions/limitMarket')
let getLimitedMarkets   = require('_lib/apiFunctions/getLimitedMarkets')

let router              = express.Router()

router.use(expressAuth)
router.use(blockedProtect)



router.get('/active-bets', getActiveBets)

router.get('/active-markets', getActiveMarkets)

router.post('/all-bets', getAllBets)

router.post('/block-market', csrfProtect, blockMarket)

router.get('/blocked-markets', getBlockedMarkets)

router.post('/unblock-market', csrfProtect, unblockMarket)

router.get('/all-bets-count', countBets)

router.post('/limit-market', csrfProtect, limitMarket)

router.get('/limited-markets', getLimitedMarkets)

module.exports = router
