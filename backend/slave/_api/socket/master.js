let socketClient                = require('socket.io-client')

let config                      = require('_config')

let secret                      = config.jwt.secret
let {port,host}                 = config.master
let master                      = socketClient(`${host}:${port}`,{
    query: `secret=${secret}`,
    transports: ['websocket']
})

module.exports = master
