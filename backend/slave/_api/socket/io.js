let express          = require('express')
let http             = require('http')
let socket           = require('socket.io')

let app              = express()
let server           = http.createServer(app)
let io               = socket(server)

module.exports.io       = io
module.exports.server   = server
module.exports.app      = app
