let placeBet                = require('_lib/apiFunctions/placeBet')
let cancelBet               = require('_lib/apiFunctions/cancelBet')
let subscribeMarket         = require('_lib/apiFunctions/subscribeMarket')
let unsubscribeMarket       = require('_lib/apiFunctions/unsubscribeMarket')

module.exports = (socket, master, limit) => {

    // validate and place bet
    socket.on('PLACE_BET', limit( bet => {
        placeBet(socket, master, bet)
    }))
    
    // cancel unmatched bet
    socket.on('CANCEL_BET',limit( bet => {
        cancelBet(socket, master, bet)
    }))

    // subscribe a user to a given market
    socket.on('SUBSCRIBE',  marketId => {
        subscribeMarket(socket, master, marketId)
    })

    // unsubscribe user from market
    socket.on('UNSUBSCRIBE', limit( marketId => {
        unsubscribeMarket(socket, master, marketId)
    }))
    
}
