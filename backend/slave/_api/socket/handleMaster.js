let emitConcernedUsers  = require('_helpers/emitConcernedUsers')
let notifyUserUpdate = require('_helpers/notifyUserUpdate')
let disconnectUsers     = require('_helpers/disconnectUsers')


let handleMaster = (io, master) => {

    master.on('MARKET_UPDATE', market => {
        io.to(market.marketId).emit('MARKET_UPDATE', market)
    })

    master.on('MARKETS_UPDATE', markets => {
        io.emit('MARKETS_UPDATE', markets)
    })

    master.on('INPLAY_CHANGE', market => {
        io.emit('INPLAY_CHANGE', market)
    })

    master.on('BALANCE_UPDATE', user => {
        notifyUserUpdate(user, 'UPDATE_USER')
    })
    master.on('BET_PLACED', bet => {
        emitConcernedUsers(io, bet, 'BET_PLACED')
    })
    master.on('PLACE_BET_FAILED', bet => {
        io.to(bet.userId).emit('PLACE_BET_FAILED', bet)
    })
    master.on('BET_MATCHED', bet => {
        emitConcernedUsers(io, bet, 'BET_MATCHED')
    })
    master.on('REMOVED_UNMATCHED_BET', bet => {
        emitConcernedUsers(io, bet, 'REMOVED_UNMATCHED_BET')
    })
    master.on('CANCEL_BET_FAILED', bet => {
        io.to(bet.userId).emit('CANCEL_BET_FAILED', bet)
    })
    master.on('BET_RESULT', bet => {
        emitConcernedUsers(io, bet, 'BET_RESULT')
    })
    master.on('MARKET_CLOSE', marketId => {
        io.emit('MARKET_CLOSE', marketId)
    })
    master.on('MARKET_CLOSE_WARN', market => {
        io.emit('MARKET_CLOSE_WARN', market)
    })
    master.on('DISCONNECT_USER', userId => {
        disconnectUsers(userId, io)
    })
    master.on('NOTIFY', data => {
        data.receivers.forEach( receiver => 
            io.to(receiver).emit(data.event, data.data)
        )
    })
}

module.exports = handleMaster
