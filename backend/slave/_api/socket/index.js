let cookieParser                = require('socket.io-cookie-parser')

let handleBetfair               = require('./handleBetfair')
let handleMaster                = require('./handleMaster')
let io                          = require('./io').io
let handleSocketConnection      = require('./handleSocketConnection')
let socketAuth                  = require('_api/middlewares/socketAuth')
let socketDos                   = require('_helpers/socketDos')
let master                      = require('./master')
let User                        = require('_models/user')
let notifyUserUpdate            = require('_helpers/notifyUserUpdate')



io.use(cookieParser())
io.use(socketAuth)

const changeUserOnlineStatus = async (userId,online) => {
    let user = await User.findById(userId)
    user.online = online
    notifyUserUpdate(user,'UPDATE_USER')
    user.save()
}

const handleTrustedSocket = (socket) => {
    let userId = socket.decoded_token.userId
    changeUserOnlineStatus(userId, true)
    let limit = socketDos(socket)
    socket.join(socket.decoded_token.userId) // keep track of client sockets
    handleBetfair(socket, master, limit) // handle socket events for betfair
    socket.on('disconnect', () => clear(socket.id))
    wait('8h', () => socket.disconnect(), socket.id)
}

const createSocketApi = () => {
    handleSocketConnection(io, master) // handle connection related config
    handleMaster(io, master) // handel the events from master
    io.on( 'connection', socket => {
        socket.csrfProtected = false
        socket.on('Csrf-Token', csrfToken => {
            if (socket.decoded_token.csrfToken === csrfToken){
                socket.csrfProtected = true
                handleTrustedSocket(socket)
            }else{
                socket.disconnect()
            }
        })
        socket.on('disconnect', () => {
            let userId = socket.decoded_token.userId
            changeUserOnlineStatus(userId, false)
        })
        wait('10s', () => !socket.csrfProtected && socket.disconnect())
    })
}

module.exports.createSocketApi          = createSocketApi

// disconnect if the user is blocked or parentblocked
// when blocking a connected user disconnect him