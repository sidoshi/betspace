const handleSocketConnection = (io, master) => {
    master.on('connect', () => {
        debug('connected to the master server')
    })
    master.on('connect_error', err => {
        // console.error(`error connecting to master server: ${err}`)
    })
    master.on('connect_timeout', err => {
        // console.error('time-out connecting to master server')
    })
    master.on('disconnect', () => {
        console.warn('disconnected from the master server')
    })
    master.on('reconnect_attempt', attempt => {
        // debug(`reconnecting to the master server, attempt: ${attempt}`)
    })
}

module.exports = handleSocketConnection
