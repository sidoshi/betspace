const csrfProtect = (req, res, next) => {
    if( req.user.csrfToken === req.get('Csrf-Token') ){
        return next()
    }else{
        return next(new Error('No right'))
    }
}

module.exports = csrfProtect
