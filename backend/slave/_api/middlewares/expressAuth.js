let jwt         = require('jsonwebtoken')

let secret      = require('_config').jwt.secret

const expressAuth = (req, res, next) => {
    jwt.verify( req.cookies.token, secret, (err, decoded) => {
        if (err) {
            res.redirect('/')
            return
        }
        req.user  = decoded
        next()
    })
}

module.exports = expressAuth
