let jwt                         = require('jsonwebtoken')

let secret                      = require('_config').jwt.secret

const socketAuth = (socket, next) => {
    let cookies = socket.request.cookies
    if(cookies){
        let token = cookies.token
        jwt.verify(token, secret, (err, decoded) => {
            if (err) return socket.disconnect()
            socket.decoded_token = decoded
            return next()
        })
    }else{
        socket.disconnect()
    }
}

module.exports = socketAuth
