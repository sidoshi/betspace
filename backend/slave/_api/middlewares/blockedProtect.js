let User        = require('_models/user')

const blockedProtect = async (req, res, next) => {
    try{
        if(!req.user) return res.redirect('/')
        let user = await User.findById(req.user.userId)
        if(!user) throw new Error('no user found')
        if(user.blocked) return res.end('You are blocked')
        if(user.parentBlocked) return res.end('Your parent is blocked')
        next()
    }catch(err){
        res.status(500).json(err.message)
    }
}

module.exports = blockedProtect
