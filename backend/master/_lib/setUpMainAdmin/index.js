let User = require('_models/user')
let Hierarchy = require('_models/hierarchy')
let BalanceSheet = require('_models/balanceSheet')
let defaultMainAdmins = require('_config').defaultMainAdmins

const createMainAdminHierarchy = async mainAdmin => {
    console.log(`Creating Hierarchy: ${mainAdmin._id}`)
    let hierarchy = {
        endpoint: mainAdmin._id,
        users: [mainAdmin._id],
        shares: {
            [mainAdmin._id]: 100
        }
    }
    await new Hierarchy(hierarchy).save()
}

const createMainAdminBalanceSheet = async mainAdmin => {
    console.log(`Creating BalanceSheet: ${mainAdmin._id}`)
    let bs = {
        userId: mainAdmin._id,
        debit: {},
        credit: {}
    }
    bs = new BalanceSheet(bs)
    await bs.save()
}

const setUpMainAdmin = async () => {
    let mainAdmins = await User.find({ userType: 'MAIN ADMIN' })
    if (!mainAdmins || mainAdmins.length <= 0){
        await Promise.all(
            defaultMainAdmins.map( async (mainAdmin) => {
                let newMainAdmin = new User(mainAdmin)
                await newMainAdmin.save()
            })
        )
        mainAdmins = await User.find({ userType: 'MAIN ADMIN' })
    }
    mainAdmins.forEach( async mainAdmin => {
        let hierarchy = await Hierarchy.findOne({ endpoint: mainAdmin._id })
        if (!hierarchy) await createMainAdminHierarchy(mainAdmin)
        let balanceSheet = await BalanceSheet.findOne({ userId: mainAdmin._id })
        if (!balanceSheet) await createMainAdminBalanceSheet(mainAdmin)
    })
}

module.exports = setUpMainAdmin