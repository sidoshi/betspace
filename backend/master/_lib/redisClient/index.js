let redis =     require('redis')
let Promise = require('bluebird')

let config =    require('_config')

let client = Promise.promisifyAll(redis.createClient(config.redis))

client.getParsed = async (key) => {
    try{
        let value = await client.getAsync(key)
        return JSON.parse(value)
    }catch(err){
        console.log(err)
    }
}

module.exports = client
