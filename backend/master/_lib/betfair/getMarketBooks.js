let session     = require('./session')
let config       = require('_config').betfair


const getMarketBooks = marketIds => new Promise( (resolve, reject) => {
    let {priceProjection} = config
    session.listMarketBook({ marketIds, priceProjection}, ( err, res ) => {
        if (err) return reject(err)
        res && resolve(res.result)
    })
})


module.exports = getMarketBooks
