let moment = require('moment')

let objects         = require('./objects')
let deleteFromArray = require('./helpers').deleteFromArray
let findMarketType  = require('./helpers').findMarketType
let redisClient     = require('_lib/redisClient')
let io              = require('_api/socket/io').io

// template for new market object
const makeNewMarket = market => ({
    marketId: market.marketId,
    type: market.eventType.name,
    name: market.event.name,
    openDate: moment(market.event.openDate).format('LLL'),
    runners: {},
    inplay: null,
    status: null,
    sortedRunners: []
})

// templete for new runner object
const makeNewRunner = runner => ({
    selectionId: runner.selectionId,
    name: runner.runnerName,
    availableToBack: null,
    availableToLay: null,
    status: null,
    lastPriceTraded: null,
    sortPriority: runner.sortPriority
})

const addMarket = async (market) => {
    let markets = objects.markets
    let marketIds = objects.marketIds

    if (!(market.eventType && market.event && market.runners)) return
    let newMarket = makeNewMarket(market)
    let { type, marketId } = newMarket

    market.runners.forEach(runner => {
        let newRunner = makeNewRunner(runner)
        newMarket.runners[runner.selectionId] = newRunner
        let index = newMarket.sortedRunners.indexOf(runner.selectionId)
        index < 0 && newMarket.sortedRunners.push(runner.selectionId)
    })

    let runners = newMarket.runners
    newMarket.sortedRunners.sort((a, b) => {
        return runners[a].sortPriority - runners[b].sortPriority
    })
    markets[marketId] = newMarket
    markets[type].indexOf(marketId) < 0 && markets[type].push(marketId)
    marketIds.indexOf(marketId) < 0 && marketIds.push(marketId)
}

// pass the intrested markets returned form api and add them to our markets
// object in organised way
const addMarkets = async (marketsToAdd) => {
    let markets     = objects.markets
    await Promise.all(
        marketsToAdd.map(async (market) => {
            await addMarket(market)
        })
    )
    await redisClient.setAsync('markets', JSON.stringify(markets))
    io.emit('MARKETS_UPDATE', markets)
}

// get a book from api and store it in the associated market
const addBook = book => {
    let markets = objects.markets
    let market = markets[book.marketId]
    if(!market) return
    let runners = market.runners

    market.inplay = book.inplay
    market.status = book.status
    book.runners.forEach( runner => {
        let id = runner.selectionId
        runners[id].status = runner.status
        runners[id].availableToBack = runner.ex.availableToBack
        runners[id].availableToLay = runner.ex.availableToLay
    })
}

// delete market from the markets object and marketIds
const deleteMarket = ( id ) => {
    let markets     = objects.markets
    let marketIds   = objects.marketIds
    setTimeout( () => {
        let type = findMarketType( id )
        if( !markets[id] ) return
        deleteFromArray( id, marketIds )
        deleteFromArray( id, markets[type] )
        delete markets[id]
        io.emit('MARKET_CLOSE', id)
        redisClient.set('markets', JSON.stringify(markets))
   }, 1000 * 60 * 5)
}

const deleteMarketInstantly = ( id ) => {
    let markets     = objects.markets
    let marketIds   = objects.marketIds
    let type = findMarketType( id )
    if( !markets[id] ) return
    deleteFromArray( id, marketIds )
    deleteFromArray( id, markets[type] )
    delete markets[id]
    io.emit('MARKET_CLOSE', id)
    redisClient.set('markets', JSON.stringify(markets))
}


module.exports.addMarkets = addMarkets
module.exports.addBook = addBook
module.exports.deleteMarket = deleteMarket
module.exports.deleteMarketInstantly = deleteMarketInstantly
