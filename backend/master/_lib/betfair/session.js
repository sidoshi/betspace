const betfair = require('betfair')
const config = require('_config').betfair

module.exports = new betfair.BetfairSession(config.applicationKey)
