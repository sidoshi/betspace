let config = require('_config').betfair

let login = ( session, attempt ) => {
    debug(`Login attempt: ${attempt}`)
    let {username, password} = config
    return new Promise(( resolve, reject ) => {
        session.login( username, password, error => {
            if ( error ) {
                return reject(error)
            }
            debug('Login Success')
            return resolve()
        })
    })
}

module.exports = login
