let moment      = require('moment')

let config       = require('_config').betfair
let session     = require('./session')

const getNewMarkets = () => {
    let today = moment()
    let {marketProjection,maxResults,filter} = config
    filter.marketStartTime = {
        to: today.add(1, 'day').format()
    }
    return new Promise( (resolve, reject) => {
        session.listMarketCatalogue({
            filter,
            marketProjection,
            maxResults
        },( error, response ) => {
            if (error) {
                debug(`Error on getting new markets: \n ${error}`)
                return reject(error)
            }
            response && resolve(response.result)
        })
    })
}

module.exports = getNewMarkets
