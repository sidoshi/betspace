let redisClient = require('_lib/redisClient')

let objects = {
    // main markets object that holds all the markets organised by types
    markets : {
        'Cricket': [],
        'Soccer': [],
        'Tennis': [],
        'Horse Racing': []
    },
    // hold all the marketIds synced with markets objects
    marketIds : [],
    // hold jobs of updating the markets
    jobs: [],
    // hold all the unmatchedBets objects until the bets are matched or the
    // associated market closes
    unmatchedBets: {},
    marketsToClose: [],
    lockedUsers: {}
}

async function restoreObjects(){
    let markets = await redisClient.getAsync('markets') 
    objects.markets = JSON.parse(markets) || objects.markets
    let unmatchedBets = await redisClient.getAsync('unmatchedBets')
    objects.unmatchedBets = JSON.parse(unmatchedBets) || objects.unmatchedBets
    let marketsToClose = await redisClient.getAsync('marketsToClose')
    objects.marketsToClose = JSON.parse(marketsToClose) || objects.marketsToClose
}
process.env.NODE_ENV !== 'test' && restoreObjects()


module.exports = objects
