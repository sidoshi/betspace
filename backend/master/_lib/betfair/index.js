let _                       = require('lodash')


let session                 = require('./session')
let addMarkets              = require('./markets').addMarkets
let objects                 = require('./objects')
let createJobs              = require('./jobs').createJobs
let settleClosedMarkets     = require('./settleClosedMarkets')
let betfairUtils            = require('./betfairUtils')


// no. of login attempts made when application starts.
let attempt = 1

// whole betfair process cycle which gets repeated every 4 hours
let betfairCycle = async () => {
    try{
        let marketIds = objects.marketIds
        let marketsToAdd = await betfairUtils.getNewMarkets()
        debug(`Got ${marketsToAdd.length} new markets`)
        addMarkets(marketsToAdd)
        let chunks = _.chunk(marketIds, 20)
        createJobs(chunks)
    }catch(error){
        debug(error)
        return betfairCycle()
    }
}

// exposed function that starts the whole process.

// log in to the session and then run cycle once and make a job to run the
// cycle and keepAlive repeatedly
let startCycle = async () => {
    let env = process.env.NODE_ENV
    try{
        await betfairUtils.login(session, attempt)
        env !== 'test' && await settleClosedMarkets(betfairCycle)
        await betfairCycle()
        repeat('4h', () => betfairCycle())
        repeat('3h', () => session.keepAlive())
        return true
    }catch(err){
        debug(err)
        attempt++
        return wait('5s', () => startCycle())
    }
}

module.exports.startCycle = startCycle
