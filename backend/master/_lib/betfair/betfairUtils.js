let login = require('./login')
let getNewMarkets = require('./getNewMarkets')
let getMarketBooks = require('./getMarketBooks')

module.exports.login = login
module.exports.getNewMarkets = getNewMarkets
module.exports.getMarketBooks = getMarketBooks
