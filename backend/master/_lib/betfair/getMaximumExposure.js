const getRunnersStatus = (runnerIds, runnersStatus) => {
    runnerIds.forEach(runnerId => {
        runnersStatus[runnerId] = 'LOSER'
    })
}

const statusToWin = {
    'BACK': 'WINNER',
    'LAY': 'LOSER'
}

const getBetResult = (runnerStatus, betType) => {
    if (runnerStatus !== 'WINNER' && runnerStatus !== 'LOSER') {
        return 'NEUTRAL'
    }
    if (runnerStatus === statusToWin[betType]) {
        return 'WINNER'
    } else {
        return 'LOSER'
    }
}

const calculateExposure = (bets, runnerId, runnersStatus) => {
    let exposure = 0
    bets.forEach( bet => {
        let runnerStatus = runnersStatus[bet.runnerId]
        let result = getBetResult(runnerStatus, bet.get('betType'))
        switch(result){
            case 'WINNER':
                if(!bet.matched){
                    // if bet is not matched then user is not going to get any 
                    // profit, so don't deduct it from exposure
                    // unmatched winning bet dosent have any effect on exposure.
                    break
                }
                exposure -= +bet.profit
                break;
            case 'LOSER':
                exposure += +bet.exposure
                break;
            default:
                console.log('nethier winner or loser')
                break
        }
    })
    return exposure
}

const getMaximumExposure = (market, bets) => {
    let exposure = 0

    let runnersStatus = {}
    let runners = market.runners
    let runnerIds = Object.keys(runners)
    getRunnersStatus(runnerIds, runnersStatus)

    runnerIds.forEach(runnerId => {
        runnersStatus[runnerId] = 'WINNER'
        let currnetExposure = calculateExposure(
            bets,
            runnerId, 
            runnersStatus
        )
        if(currnetExposure > exposure) exposure = currnetExposure
        runnersStatus[runnerId] = 'LOSER'
    })
    return exposure
}

module.exports = getMaximumExposure