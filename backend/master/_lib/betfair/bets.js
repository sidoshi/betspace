let objects                     = require('./objects')
let io                          = require('_api/socket/io').io
let User                        = require('_models/user')
let Bet                         = require('_models/bet')
let Market                      = require('_models/market')
let redisClient                 = require('_lib/redisClient')
let Promise                     = require('bluebird')
let getMaximumExposure          = require('./getMaximumExposure')
let updateBalanceSheet          = require('_helpers/updateBalanceSheet')
let { percentOf } = require('_helpers/numberUtils')

const isBetMatched = bet => {
    let markets = objects.markets
    let market = markets[bet.marketId]
    if (!market) throw new Error('No market found')
    let runner = market.runners[bet.runnerId]
    if (!runner) throw new Error('No runner found')
    if (!runner.availableToBack) throw new Error('No runner found')
    if (!runner.availableToLay)  throw new Error('No runner found')
    let intrestedRate = bet.betType === 'BACK'
        ? runner.availableToBack[0] && runner.availableToBack[0].price
        : runner.availableToLay[0] && runner.availableToLay[0].price
    return bet.rate == intrestedRate
}

const organiseBetsByMarketId = bets => {
    let organisedBets = {}
    bets.forEach(bet => {
        if (!organisedBets[bet.marketId]) {
            organisedBets[bet.marketId] = []
        }
        organisedBets[bet.marketId].push(bet)
    })
    return organisedBets
}

const calculateUserExposure = async (includeNewBet=false, userIdOrBet) => {
    let userId = includeNewBet ? userIdOrBet.userId : userIdOrBet 
    let bets = await Bet.find({
        userId: userId,
        marketStatus: 'OPEN',
        result: 'PENDING'
    })
    includeNewBet && bets.push(userIdOrBet)
    let exposure = 0
    let organisedBets = organiseBetsByMarketId(bets)
    Object.keys(organisedBets).forEach( marketId => {
        let market = objects.markets[marketId]
        exposure += getMaximumExposure(market, organisedBets[marketId])
    })
    return exposure
}

const restoreBalanceOnBetDelete = async (bet) => {
    try{
        let user = await User.findById(bet.userId)
        if (!user) return debug('no user found to restore')
        user.exposure = await calculateUserExposure(false, bet.userId)
        user.bettingBalance = user.balance - user.exposure
        user = await user.save()
        user = user.toObject()
        user.userId = user._id
        io.emit('BALANCE_UPDATE', user)
    }catch(err){
        restoreBalanceOnBetDelete(bet)
    }
}


const cancelBet = async (bet) => {
    let unmatchedBets = objects.unmatchedBets
    if (!unmatchedBets[bet.marketId]){
        io.emit('CANCEL_BET_FAILED', bet)
        return false
    }
    let index = unmatchedBets[bet.marketId].findIndex( unmatchedBet => {
        return unmatchedBet._id == bet._id
    })
    if(index < 0){
        io.emit('CANCEL_BET_FAILED', bet)
        return false
    }
    try{
        bet = await Bet.findById(bet._id)
        bet.remove()
        await restoreBalanceOnBetDelete(bet)
        io.emit('REMOVED_UNMATCHED_BET', bet)
        unmatchedBets[bet.marketId].splice(index, 1)
        unmatchedBets[bet.marketId].length === 0 
        && delete unmatchedBets[bet.marketId]
        redisClient.set('unmatchedBets', JSON.stringify(unmatchedBets))
        return bet
    }catch(error){
        cancelBet(bet)
    }
}

const isUserLocked = (userId) => {
    if(objects.lockedUsers[userId]) return true
    return false
}

const lockUser = (userId) => {
    objects.lockedUsers[userId] = 'LOCKED'
}

const unlockUser = (userId) => {
    if(objects.lockedUsers[userId])
        delete objects.lockedUsers[userId]
}

const placeBet = async (bet) => {
    let unmatchedBets = objects.unmatchedBets
    if (isUserLocked(bet.userId)) return 
    lockUser(bet.userId)
    try{
        bet.matched = isBetMatched(bet)
    }catch(err){
        debug(err)
        debug(bet)
        unlockUser(bet.userId)
        return io.emit('PLACE_BET_FAILED', err.message)
    }
    let newBet = new Bet(bet)
    if(!bet.matched){
        if(!unmatchedBets[bet.marketId]) unmatchedBets[bet.marketId] = []
        unmatchedBets[bet.marketId].push(newBet)
        await redisClient.setAsync(
            'unmatchedBets', 
            JSON.stringify(unmatchedBets)
        )
    }
    try{
        let user = await User.findById(bet.userId)
        user.exposure = await calculateUserExposure(true, newBet)
        if(user.exposure > user.balance) {
            unlockUser(bet.userId)
            return io.emit('PLACE_BET_FAILED', bet)
        }
        user.bettingBalance = user.balance - user.exposure
        await user.save()
        user = user.toObject()
        user.userId = user._id
        io.emit('BALANCE_UPDATE', user)
        bet = await newBet.save()
        io.emit('BET_PLACED', bet)
    } catch(error){
        debug(error)
        io.emit('PLACE_BET_FAILED', bet)
    }
    unlockUser(bet.userId)
    return bet
}

const matchBet = async (bet, index) => {
    let unmatchedBets = objects.unmatchedBets
    if(!bet) return console.log(bet)
    if(!bet.marketId) return console.log(1, bet)
    try {
        unmatchedBets[bet.marketId].splice(index, 1)
        await redisClient.setAsync(
            'unmatchedBets', 
            JSON.stringify(unmatchedBets)
        )
        bet = await Bet.findOneAndUpdate({ _id: bet._id }, { matched: true })
        let user = await User.findById(bet.userId)
        user.exposure = await calculateUserExposure(false, bet.userId)
        user.bettingBalance = user.balance - user.exposure
        user = await user.save()
        user = user.toObject()
        user.userId = user._id
        io.emit('BALANCE_UPDATE', user)
        if(!bet) return
        bet.matched = true
        io.emit('BET_MATCHED', bet)
    } catch (err) {
        debug(err)
        matchBet(bet)
    }
}

const matchUnmatchedBets = marketId => {
    let unmatchedBets = objects.unmatchedBets
    let betsToMatch = unmatchedBets[marketId]
    betsToMatch.forEach( ( bet, index ) => {
        if (isBetMatched(bet)){
            matchBet(bet, index)
        }
    })
}

const statusToWin = {
    'BACK': 'WINNER',
    'LAY':  'LOSER'
}

const getBetResult = (bet, market) => {
    if (!market) return 'NETURAL'
    let runner = market.runners[bet.runnerId]
    if (!runner) return 'NETURAL'
    if (runner.status !== 'WINNER' && runner.status !== 'LOSER'){
        return 'NEUTRAL'
    }
    if ( runner.status === statusToWin[bet.betType]){
        return 'WINNER'
    }else{
        return 'LOSER'
    }
}

const shareParentProfit = async (bet) => {
    // add exposure of bet to parents according to their share
    bet = bet.toObject()
    let parents = bet.parents
    let parentsUpdatePromises = parents.map( async (parent) => {
        let share = bet.shares[parent.userId]
        let amount = (bet.exposure * share) / 100 
        let user = await User.findById(parent.userId)
        user.balance += amount
        user.profitAndLoss+= amount
        await user.save()
    })
    await Promise.all(parentsUpdatePromises)
}

const shareParentLoss = async (bet) => {
    // remove profit from parents according to their share
    bet = bet.toObject()
    let parents = bet.parents
    let parentsUpdatePromises = parents.map( async (parent) => {
        let share = bet.shares[''+parent.userId]
        let amount = (bet.profit * share) / 100 
        let user = await User.findById(parent.userId)
        user.balance -= amount
        user.profitAndLoss -= amount
        await user.save()
    })
    await Promise.all(parentsUpdatePromises)
}

const calculateBalances = async (bet) => {
    try{
        if (bet.result !== 'WINNER') {
            let user = await User.findById(bet.userId)
            user.balance -= bet.exposure
            user.profitAndLoss -= bet.exposure
            await updateBalanceSheet({
                entryType: 'BET_LOST',
                bet: bet
            })
            await shareParentProfit(bet) // parents have won as user has lost
            user = await user.save()
            return -bet.exposure
        } 
        let user = await User.findById(bet.userId)
        await updateBalanceSheet({
            entryType: 'BET_WON',
            bet: bet
        })
        await shareParentLoss(bet) // parents have lost as user has won
        user.balance += bet.profit
        user.profitAndLoss += bet.profit
        user = await user.save()
        return bet.profit
    }catch(err){
        console.log(err)
    }
}


const addNumOfBets = async (numOfBets, marketId) => {
    let newMarket = await Market.findOne({ marketId: marketId })
    newMarket.betsPlaced = numOfBets
    newMarket.save()
}


async function closeBet (bet) {
    let market = await Market.findOne({ marketId: bet.marketId })
    market.toObject()
    if (!bet.matched) {
        bet.remove()
        io.emit('REMOVED_UNMATCHED_BET', bet)
        debug(`bet id ${bet.id} removed`)
        return 0
    }
    let runners         = market.runners
    bet.result          = getBetResult(bet, market)
    bet.marketStatus    = market.status
    let runnerId        = bet.runnerId
    bet.runnerStatus    = runners[runnerId].status
    try{
        bet = await bet.save()
        io.emit('BET_RESULT', bet)
        return await calculateBalances(bet)
    }catch(err){
        debug(err)
        closeBet(bet)
    }
}

const findMainAdmin = async user => {
    user = (user.toObject && user.toObject()) || user
    let mainAdminId = user.parents[0].userId
    let mainAdmin = User.findById(mainAdminId)
    return mainAdmin
}

const calculateOnMarketClose = async (userIds, userProfitAndLoss) => {
    await Promise.each(userIds, async (userId) => {
        let user = await User.findById(userId)
        let hasUserWon = userProfitAndLoss[userId] > 0
        // find if user has won on market close
        // if he has take 2% from is profit, remove it from his balance
        // add it to mainAdmin's balance
        // update the balanceSheet so that user has to give mainAdmin 2%
        if (hasUserWon){
            let profit = userProfitAndLoss[userId]
            let commissionPercent = 2
            let commission = percentOf(profit, commissionPercent)
            user.balance -= commission
            user.profitAndLoss -= commission
            let mainAdmin = await findMainAdmin(user)
            mainAdmin.balance += commission
            mainAdmin.profitAndLoss += commission
            mainAdmin = await mainAdmin.save()
            io.emit('BALANCE_UPDATE', mainAdmin)
            await updateBalanceSheet({
                entryType: 'PASS_COMMISSION',
                commission: commission,
                client: user
            })
        }
        user.exposure = await calculateUserExposure(false, userId)
        user.bettingBalance = user.balance - user.exposure
        await user.save()
        user = user.toObject()
        user.userId = user._id
        io.emit('BALANCE_UPDATE', user)
    })
}

const closeBets = async (marketId) => {
    let unmatchedBets = objects.unmatchedBets
    unmatchedBets[marketId] &&  delete unmatchedBets[marketId]
    // all the users who betted on this market
    let usersToCalculate = [] 
    let userProfitAndLoss = {}
    try {
        await redisClient.setAsync(
            'unmatchedBets', 
            JSON.stringify(unmatchedBets)
        )
        let bets = await Bet.find({ marketId: marketId })
        let numOfBets = bets.length
        // map through each bet of the market and close it
        await Promise.each(bets, async (bet) => {
            !bet.matched && numOfBets--
            let profitAndLoss = await closeBet(bet)
            userProfitAndLoss[bet.userId] = userProfitAndLoss[bet.userId] || 0
            userProfitAndLoss[bet.userId] += profitAndLoss
            let index = usersToCalculate.indexOf(''+bet.userId)
            if(index < 0) usersToCalculate.push(''+bet.userId)
        })
        await calculateOnMarketClose(usersToCalculate, userProfitAndLoss)
        addNumOfBets(numOfBets, marketId)
    } catch(err){
        console.error(err)
    }
}


module.exports.placeBet = placeBet
module.exports.matchUnmatchedBets = matchUnmatchedBets
module.exports.closeBets = closeBets
module.exports.cancelBet = cancelBet


/* eslint eqeqeq: "off" */
