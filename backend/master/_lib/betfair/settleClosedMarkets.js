let _                       = require('lodash')
let Promise                 = require('bluebird')

let objects                 = require('./objects')
let redisClient             = require('_lib/redisClient')
let getMarketBooks          = require('./getMarketBooks')
let deleteMarketInstantly   = require('./markets').deleteMarketInstantly
let closeBets               = require('./bets').closeBets
let Market                  = require('_models/market')
let io                      = require('_api/socket/io').io
let addBook                 = require('./markets').addBook
let removeBlockedMarket = require('./helpers').removeBlockedMarket
let removeMarketLimits = require('./helpers').removeMarketLimits

let ignoredKeys = ['Cricket','Soccer','Horse Racing','Tennis']
let response = 0
let length, markets

const removeAfterSettling = async (marketId) => {
    let markets = objects.markets
    let market = await Market.findOne({ marketId: marketId })
    if (!market) throw new Error('Market not found')
    market.runners = markets[marketId].runners
    market.status = 'CLOSED'
    market = await market.save()
    debug(`Market '${markets[marketId].name}' Closed`)
    await removeBlockedMarket(marketId)
    await removeMarketLimits(marketId)
    await closeBets( marketId )
    deleteMarketInstantly(marketId)
    io.emit('MARKET_CLOSE', marketId)
}

const settleMarket = async (market, marketId) => {
    if (ignoredKeys.indexOf(marketId) >= 0) return
    try {
        let book = await getMarketBooks([marketId])
        addBook(book[0])
        if (book[0].status === 'CLOSED') {
            await removeAfterSettling(marketId)
        }
        response++
        debug(`${response} markets settled`)
        if (response >= length) {
            debug(`completed settling ${length} old markets`)
            let remaining = Object.keys(markets).length - 4
            debug(`Remaining ${remaining} open markets`)
            await redisClient.setAsync('markets', JSON.stringify(markets))
        }
    } catch(err){
        response++
        debug(`Error at ${response} market: ${err}`)
        response--
        await settleMarket(market, marketId)
    }
}

const settleClosedMarkets = async () => {
    markets = objects.markets
    let marketIds = Object.keys(markets)
    length = marketIds.length - 4
    debug(`${length} old markets`)
    if (length <= 0) return
    await Promise.all(
        _.map(markets, async (market, marketId) => {
            await settleMarket(market, marketId)
        })
    )
}

module.exports = settleClosedMarkets
