let uuid                    = require('node-uuid')

let betfairUtils            = require('./betfairUtils')
let addBook                 = require('./markets').addBook
let deleteMarket            = require('./markets').deleteMarket
let io                      = require('_api/socket/io').io
let matchUnmatchedBets      = require('./bets').matchUnmatchedBets
let closeBets               = require('./bets').closeBets
let objects                 = require('./objects')
let redisClient             = require('_lib/redisClient')
let Market                  = require('_models/market')
let removeBlockedMarket     = require('./helpers').removeBlockedMarket
let removeMarketLimits      = require('./helpers').removeMarketLimits


const clearJobs = () => {
    let jobs = objects.jobs
    clear(jobs)
    jobs = []
}

const deleteJob = ( chunk, marketId ) => {
    let index = chunk.indexOf(marketId)
    index >= 0 && chunk.splice(index, 1)
}

const onMarketClose = async ( chunk, id ) => {
    let markets = objects.markets
    let market = await Market.findOne({ marketId: id })
    if (!market) return
    market.status = 'CLOSED'
    market.runners = markets[id].runners
    await market.save()
    debug(`Market '${markets[id].name}' Closed`)
    await removeBlockedMarket(id)
    await removeMarketLimits(id)
    closeBets( id )
    deleteMarket( id )
    debug(`Remaining ${chunk.length} markets in this group`)
}

const job = async (chunk, jobId) => {
    let markets         = objects.markets
    let unmatchedBets   = objects.unmatchedBets
    try {
        let books = await betfairUtils.getMarketBooks(chunk)
        if (!books) return
        books.forEach(book => {
            let inplay = markets[book.marketId].inplay
            addBook(book)
            // if inplay has changed and changed inplay is true
            if (inplay !== book.inplay && book.inplay) {
                io.emit('INPLAY_CHANGE', markets[book.marketId])
            }
            io.emit('MARKET_UPDATE', markets[book.marketId])
            if (book.status === 'OPEN' && unmatchedBets[book.marketId]){
                matchUnmatchedBets(book.marketId)
            }
            if (book.status === 'CLOSED') {
                onMarketClose(chunk, book.marketId)
                deleteJob(chunk, book.marketId)
                io.emit('MARKET_CLOSE_WARN', markets[book.marketId])
            }
        })
        await redisClient.setAsync('markets', JSON.stringify(markets))
        restartJob(chunk, jobId)
    } catch (error) {
        debug(error)
        restartJob(chunk, jobId)   
    }
}

const getBookOnce = async (chunk) => {
    let markets = objects.markets
    try {
        let books = await betfairUtils.getMarketBooks(chunk)
        if (!books) return
        books.forEach(async (book) => {
            addBook(book)
            let market = await Market.findOne({ marketId: book.marketId })
            if (market) return
            await new Market(markets[book.marketId]).save()
        })
        await redisClient.setAsync('markets', JSON.stringify(markets))
    } catch (error) {
        debug(error)
        await getBookOnce(chunk)
    }
}

function restartJob( chunk, jobId ) {
    let jobs = objects.jobs
    if ( jobs.indexOf(jobId) >= 0 ){
        wait('1s', () => job(chunk, jobId), jobId)
    }
}

const createJobs = chunks => {
    let jobs = objects.jobs
    clearJobs()
    chunks.forEach( chunk => {
        let jobId = uuid.v4()
        jobs.push(jobId)
        getBookOnce(chunk) // get book once for each chunk to store in db
        wait('1s', () => job(chunk, jobId), jobId)
    })
}


module.exports.createJobs = createJobs
module.exports.onMarketClose = onMarketClose
