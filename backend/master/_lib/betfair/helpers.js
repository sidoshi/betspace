let objects = require('./objects')
let User = require('_models/user')

const findMarketType = id => {
    let markets = objects.markets
    if (!markets[id]) return false
    return markets[id].type
}

const deleteFromArray = ( element, array ) => {
    let index = array.indexOf(element)
    index > -1 && array.splice(index,1)
}

const removeBlockedMarket = async marketId => {
    await User.update({
        blockedMarkets: marketId
    },{
        $pullAll: { blockedMarkets: [marketId] }
    },{
        multi: true
    })
}

const removeMarketLimits = async marketId => {
    await User.update({
            'limitedMarkets.marketId': marketId,
        },
        {
            $pull: { limitedMarkets: { marketId: marketId } },
        },
        {
            multi: true,
        })
}

module.exports.findMarketType = findMarketType
module.exports.deleteFromArray = deleteFromArray
module.exports.removeBlockedMarket = removeBlockedMarket
module.exports.removeMarketLimits = removeMarketLimits
