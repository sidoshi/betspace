require('app-module-path').addPath(`${__dirname} /../`)
require('app-module-path').addPath(__dirname + '/../../')
require('waitjs')
require('console.table')
global.debug = require('_helpers/debug')

let clear = require('clear')
let mongoose = require('mongoose')
let expect = require('chai').expect
let sinon = require('sinon')

let betfairUtils = require('_lib/betfair/betfairUtils')
let User = require('_models/user')
let Bet = require('_models/bet')
let Market = require('_models/market')
let BalanceSheet = require('_models/balanceSheet')
let betfair = require('_lib/betfair')
let objects = require('_lib/betfair/objects')
let bets = require('_lib/betfair/bets')
let config  = require('_config')
let delay = require('_helpers/delay')
let marketBooks = require('./objects/marketBooks.json')
let newMarkets = require('./objects/newMarkets.json')
let organisedMarkets = require('./objects/organisedMarkets')
let organisedMarketsWithBooks = require('./objects/organisedMarketsWithBooks')
let redisClient = require('_lib/redisClient')
let Promise = require('bluebird')
let assertBalanceSheet = require('./helpers/assertBalanceSheet')
let { percentOf } = require('_helpers/numberUtils')

// let displayUser = require('./helpers/displayUser')
let setUpUsers = require('./helpers/setUpUsers')

let env = process.env.NODE_ENV
clear()

let user, client, mainAdmin, master, superMaster

const getShare = () => ({
    [mainAdmin._id]: 30,
    [superMaster._id]: 40,
    [master._id]: 30,
    [user._id]: 0
})

const assertParentLoss = async loss => {
    let users = await User.find({})
    users.forEach( user => {
        if(user.userType === 'CLIENT') return
        let share = getShare()
        share = share[user._id]
        let sharedLoss = percentOf(loss, share)
        let commission = percentOf(loss, 2)
        if(user.userType === 'MAIN ADMIN'){
            expect(user.profitAndLoss).to.equal(
                mainAdmin.profitAndLoss - sharedLoss + commission
            )
            expect(user.balance).to.equal(
                mainAdmin.balance - sharedLoss + commission
            )
        }
        if(user.userType === 'SUPER MASTER'){
            expect(user.profitAndLoss).to.equal(
                superMaster.profitAndLoss - sharedLoss
            )
            expect(user.balance).to.equal(
                superMaster.balance - sharedLoss
            )
        }
        if(user.userType === 'MASTER'){
            expect(user.profitAndLoss).to.equal(
                master.profitAndLoss - sharedLoss
            )
            expect(user.balance).to.equal(
                master.balance - sharedLoss
            )
        }
    })
}

const assertParentProfit = async profit => {
    let users = await User.find({})
    users.forEach( user => {
        if(user.userType === 'CLIENT') return
        let share = getShare()
        share = share[user._id]
        let sharedprofit = percentOf(profit, share)
        if(user.userType === 'MAIN ADMIN'){
            expect(user.profitAndLoss).to.equal(
                mainAdmin.profitAndLoss + sharedprofit
            )
            expect(user.balance).to.equal(
                mainAdmin.balance + sharedprofit
            )
        }
        if(user.userType === 'SUPER MASTER'){
            expect(user.profitAndLoss).to.equal(
                superMaster.profitAndLoss + sharedprofit
            )
            expect(user.balance).to.equal(
                superMaster.balance + sharedprofit
            )
        }
        if(user.userType === 'MASTER'){
            expect(user.profitAndLoss).to.equal(
                master.profitAndLoss + sharedprofit
            )
            expect(user.balance).to.equal(
                master.balance + sharedprofit
            )
        }
    })
}

const assertParentBalance = async pl => {
    try{
        if(pl > 0) await assertParentLoss(pl)
        else await assertParentProfit(-pl)
    }catch(err){
        throw err
    }
}

const getProfitAndExposure = bet => {
    if (bet.betType === 'BACK') {
        bet.profit = (bet.stake * bet.rate) - bet.stake
        bet.exposure = bet.stake
    } else {
        bet.profit = bet.stake
        bet.exposure = (bet.stake * bet.rate) - bet.stake
    }
}



const getBet = (marketId, market, runnerId, runner, rate) => {
    let bet = {
        stake: 1000,
        rate: rate,
        betType: 'BACK',
        runnerId: runnerId,
        marketId: marketId,
        username: user.username,
        userId: user._id,
        userType: user.userType,
        ipAddress: '127.0.0.1',
        marketName: market && market.name,
        marketType: market && market.type,
        runnerName: runner && runner.name,
        runnerStatus: runner && runner.status,
        marketStatus: market && market.status,
        matched: false,
        result: 'PENDING',
        parents: user.parents,
        shares: getShare()
    }
    getProfitAndExposure(bet)
    return bet
}

const updateMarketBookRate = () => {
    let backRate = marketBooks[0].runners[1].ex.availableToBack
    let temp = backRate[0].price
    backRate[0].price = backRate[1].price
    backRate[1].price = temp
}

const closeAMarket = () => {
    marketBooks[0].status = 'CLOSED'
    marketBooks[0].runners.forEach(( runner, index) => {
        index === 0
            ? runner.status = 'WINNER'
            : runner.status = 'LOSER'
    })
}

const mockMarketClose = async (marketId) => {
    let markets = await Market.find({marketId})
    let market = markets[0]
    market.status = 'CLOSED'
    market.sortedRunners.forEach( (runnerId, index) => {
        let runner = market.runners[runnerId]
        if (index === 0) return runner.status = 'WINNER'
        runner.status = "LOSER"
    })
    await Market.findOneAndUpdate({ _id: market._id }, market)
    return market
}

const restoreMarketMock = async (marketId) => {
    let markets = await Market.find({ marketId })
    let market = markets[0]
    market.status = 'OPEN'
    market.sortedRunners.forEach(runnerId => {
        let runner = market.runners[runnerId]
        runner.status = "ACTIVE"
    })
    await Market.findOneAndUpdate({ _id: market._id }, market)
    return market
}


before('connect to test database', async () => {
    if (mongoose.connection.readyState === 0) {
        await mongoose.connect(config.mongo.url[env], config.mongo.options)
    }
    await mongoose.connection.db.dropDatabase()
    sinon.stub(betfairUtils, 'login')
    sinon.stub(betfairUtils, 'getNewMarkets')
        .returns(newMarkets)
    sinon.stub(betfairUtils, 'getMarketBooks', async (chunk) => {
        await delay(500)
        let booksToReturn = []
        chunk.forEach( marketId => {
            let i = marketBooks.findIndex( book => marketId === book.marketId)
            booksToReturn.push(marketBooks[i])
        })
        return booksToReturn
    })
})
after('clear database', async () => {
    await mongoose.connection.db.dropDatabase()
    await redisClient.setAsync('markets', JSON.stringify({}))
})
beforeEach('create new user', async () => {
    ({
        client,
        master,
        superMaster,
        mainAdmin
    } = await setUpUsers() )
    user = client
})
afterEach('delete user and unmatchedBets', async () => {
    await User.remove({})
    await Bet.remove({})
    await BalanceSheet.remove({})
    await redisClient.setAsync('unmatchedBets', '{}')
    objects.unmatchedBets = {}
})

it('should login and start betfair-cycle', async () => {
    let cycleStarted = await betfair.startCycle()
    expect(cycleStarted).to.equal(true)
})

it('should organise markets after fetching them', async () => {
    expect(objects.markets).to.eql(organisedMarkets)
    await delay(600) // wait till books are added to the market
    expect(objects.markets).to.eql(organisedMarketsWithBooks)
})

it('should place bet and match if rate is acceptable and reduce user balance', 
    async () => {
        let marketId = marketBooks[0].marketId
        let market = objects.markets[marketId]
        let runnerId = market.sortedRunners[0]
        let runner = market.runners[runnerId]
        let bet = getBet(
            marketId,
            market,
            runnerId,
            runner,
            runner.availableToBack[0].price
        )
        bet = await bets.placeBet(bet)
        let _user = await User.findById(user._id)
        let _bet = await Bet.findById(bet._id)
        expect(objects.unmatchedBets).to.be.empty
        expect(_bet).to.not.be.empty
        expect(_bet).to.be.an('object')
        expect(_bet.matched).to.be.true
        expect(_user.bettingBalance).to.equal(user.balance - bet.exposure)
        expect(_user.balance).to.be.equal(user.balance)
        expect(_user.exposure).to.be.equal(bet.exposure)
    }
)
it('should not match bet if rate is not acceptable', async () => {
    let marketId = marketBooks[0].marketId
    let market = objects.markets[marketId]
    let runnerId = market.sortedRunners[0]
    let runner = market.runners[runnerId]
    let bet = getBet(
        marketId,
        market,
        runnerId,
        runner,
        runner.availableToBack[1].price
    )
    bet = await bets.placeBet(bet)
    let _user = await User.findById(user._id)
    let _bet = await Bet.findById(bet._id)
    let _unmatchedBets = await redisClient.getParsed('unmatchedBets')
    expect(objects.unmatchedBets).to.not.be.empty
    expect(objects.unmatchedBets[marketId].length).to.equal(1)
    expect(_unmatchedBets[marketId].length).to.equal(1)
    expect(_bet).to.not.be.empty
    expect(_bet).to.be.an('object')
    expect(_bet.matched).to.be.false
    expect(_user.bettingBalance).to.equal(user.balance - bet.exposure)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
})

it('should eventually match the bet if its rate is acceptable', async () => {
    let marketId = marketBooks[0].marketId
    let market = objects.markets[marketId]
    let runnerId = market.sortedRunners[1]
    let runner = market.runners[runnerId]
    let bet = getBet(
        marketId,
        market,
        runnerId,
        runner,
        runner.availableToBack[1].price
    )
    bet = await bets.placeBet(bet)
    let _user = await User.findById(user._id)
    let _bet = await Bet.findById(bet._id)
    let _unmatchedBets = await redisClient.getParsed('unmatchedBets')
    expect(objects.unmatchedBets).to.not.be.empty
    expect(objects.unmatchedBets[marketId].length).to.equal(1)
    expect(_unmatchedBets[marketId].length).to.equal(1)
    expect(_bet).to.not.be.empty
    expect(_bet).to.be.an('object')
    expect(_bet.matched).to.be.false
    expect(_user.bettingBalance).to.equal(user.balance - bet.exposure)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
    updateMarketBookRate()
    await delay(1800) // wait for bet to get matched
    _unmatchedBets = await redisClient.getParsed('unmatchedBets')
    _bet = await Bet.findById(bet._id)
    _user = await User.findById(user._id)
    expect(objects.unmatchedBets[marketId].length).to.equal(0)
    expect(_unmatchedBets[marketId].length).to.equal(0)
    expect(_bet.matched).to.be.true
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
})

it('should place a back bet with proper account calculations', async () => {
    let marketId = marketBooks[0].marketId
    let market = objects.markets[marketId]
    let runnerId = market.sortedRunners[0]
    let runner = market.runners[runnerId]
    let bet = getBet(
        marketId,
        market,
        runnerId,
        runner,
        runner.availableToBack[0].price
    )
    bet = await bets.placeBet(bet)
    let _user = await User.findById(user._id)
    let _bet = await Bet.findById(bet._id)
    let profit = (bet.stake * bet.rate) - bet.stake
    let exposure = bet.stake
    expect(_bet.profit).to.equal(profit)
    expect(_bet.exposure).to.equal(exposure)
    expect(_user.bettingBalance).to.equal(user.balance - exposure)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
})

it('should place a lay bet with proper account calculations', async () => {
    let marketId = marketBooks[0].marketId
    let market = objects.markets[marketId]
    let runnerId = market.sortedRunners[1]
    let runner = market.runners[runnerId]
    let bet = getBet(
        marketId,
        market,
        runnerId,
        runner,
        runner.availableToLay[0].price
    )
    bet.betType = 'LAY'
    getProfitAndExposure(bet)
    bet = await bets.placeBet(bet)
    let _user = await User.findById(user._id)
    let _bet = await Bet.findById(bet._id)
    let profit = bet.stake
    let exposure = (bet.stake * bet.rate) - bet.stake
    expect(_bet.profit).to.equal(profit)
    expect(_bet.exposure).to.equal(exposure)
    expect(_user.bettingBalance).to.equal(user.balance - exposure)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
})

it('should let user cancel an unmatched bet and return the balance', 
    async () => {
        let marketId = marketBooks[0].marketId
        let market = objects.markets[marketId]
        let runnerId = market.sortedRunners[1]
        let runner = market.runners[runnerId]
        let bet = getBet(
            marketId,
            market,
            runnerId,
            runner,
            runner.availableToBack[1].price
        )
        let _unmatchedBets = await redisClient.getParsed('unmatchedBets')
        expect(_unmatchedBets).to.be.empty
        expect(objects.unmatchedBets).to.be.empty
        bet = await bets.placeBet(bet)
        let _user = await User.findById(user._id)
        expect(_user.bettingBalance).to.equal(user.balance - bet.exposure)
        expect(_user.balance).to.be.equal(user.balance)
        expect(_user.exposure).to.be.equal(bet.exposure)
        let oldBalance = user.balance - bet.exposure
        _unmatchedBets = await redisClient.getParsed('unmatchedBets')
        expect(_unmatchedBets).to.not.be.empty
        expect(objects.unmatchedBets).to.not.be.empty
        let _unmatchedBet = _unmatchedBets[marketId][0]
        let unmatchedBet = objects.unmatchedBets[marketId][0]
        expect(_unmatchedBet).to.not.be.empty
        expect(unmatchedBet).to.not.be.empty
        bet = await bets.cancelBet(unmatchedBet)
        _unmatchedBets = await redisClient.getParsed('unmatchedBets')
        let _bets = await Bet.find({matched: false})
        expect(_bets).to.be.empty
        expect(_unmatchedBets).to.be.empty
        expect(objects.unmatchedBets).to.be.empty
        _user = await User.findById(bet.userId)
        expect(_user.bettingBalance).to.equal(oldBalance + bet.exposure)
        expect(_user.balance).to.be.equal(user.balance)
        expect(_user.exposure).to.be.equal(0)
    }
)

it('should not let user cancel matched bet', async () => {
    let marketId = marketBooks[0].marketId
    let market = objects.markets[marketId]
    let runnerId = market.sortedRunners[1]
    let runner = market.runners[runnerId]
    let bet = getBet(
        marketId,
        market,
        runnerId,
        runner,
        runner.availableToBack[1].price
    )
    bet = await bets.placeBet(bet)
    let _bets = await Bet.find({})
    bet = _bets[0]
    let _bet = await bets.cancelBet(bet)
    expect(_bet).to.be.false
    _bet = await Bet.findById(bet._id)
    expect(_bet).to.not.be.empty
    let _user = await User.findById(_bet.userId)
    expect(_user.bettingBalance).to.equal(user.balance - bet.exposure)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
})

it('should delete unmatchedBets on market close', async () => {
    let marketId = marketBooks[0].marketId
    let market = objects.markets[marketId]
    let runnerId = market.sortedRunners[1]
    let runner = market.runners[runnerId]
    let bet = getBet(
        marketId,
        market,
        runnerId,
        runner,
        runner.availableToBack[0].price * 2 // to unmatch the bet
    )
    let _user = await User.findById(user._id)
    let oldBalance = _user.balance
    bet = await bets.placeBet(bet)
    _user = await User.findById(bet.userId)
    let _bet = await Bet.findById(bet._id)
    expect(bet.matched).to.be.false
    expect(_bet).to.not.be.empty
    expect(_user.bettingBalance).to.equal(user.balance - bet.exposure)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.exposure).to.be.equal(bet.exposure)
    await mockMarketClose(bet.marketId)
    await bets.closeBets(bet.marketId)
    _bet = await Bet.findById(bet._id)
    expect(_bet).to.be.null
    _user = await User.findById(user._id)
    expect(_user.balance).to.be.equal(user.balance)
    expect(_user.bettingBalance).to.equal(oldBalance)
    expect(_user.exposure).to.be.equal(0)
    expect(_user.profitAndLoss).to.be.equal(0)
    await restoreMarketMock(bet.marketId)
})

it('should give proper bet results and calculate proper balance',
    async () => {
        let winnerBets =  []
        let loserBets = []
        let winningAmount = 0
        await Promise.each([1, 2, 3, 4, 5], async (i) => {
            let marketId = marketBooks[0].marketId
            let market = objects.markets[marketId]
            let runnerId = i <= 3
                ? market.sortedRunners[0]
                : market.sortedRunners[1]
            let runner = market.runners[runnerId]
            let bet = getBet(
                marketId,
                market,
                runnerId,
                runner,
                runner.availableToBack[0].price
            )
            bet = await bets.placeBet(bet)
            if (i <= 3) {
                winnerBets.push(bet._id)
                winningAmount += bet.profit
            } else {
                loserBets.push(bet._id)
                winningAmount -= bet.exposure
            }
        })
        let _user = await User.findById(user._id) 
        let oldBalance = _user.balance
        expect(_user.balance).to.equal(user.balance)
        expect(_user.exposure).to.equal(5000)
        expect(_user.bettingBalance).to.equal(user.balance - _user.exposure)
        expect(_user.profitAndLoss).to.equal(0)
        let _bets = await Bet.find({})
        expect(_bets.length).to.be.equal(5)
        _bets.forEach( bet => expect(bet.result).to.be.equal('PENDING') )
        await mockMarketClose(_bets[0].marketId)
        await bets.closeBets(_bets[0].marketId)
        _bets = await Bet.find({_id: { $in: winnerBets }})
        _user = await User.findById(_bets[0].userId)
        expect(_bets.length).to.be.equal(3)
        _bets.forEach( bet => {
            expect(bet.result).to.equal('WINNER')
        })
        _bets = await Bet.find({_id: { $in: loserBets }})
        expect(_bets.length).to.be.equal(2)
        _bets.forEach( bet => {
            expect(bet.result).to.equal('LOSER')
        })
        _user = await User.findById(_bets[0].userId)
        let _mainAdmin = await User.findById(mainAdmin._id)
        let commission = percentOf(winningAmount, 2)
        let profitAndLoss = winningAmount
        expect(_user.profitAndLoss).to.equal(profitAndLoss - commission)
        expect(_user.balance).to.be.equal(oldBalance + profitAndLoss - commission)
        expect(_user.exposure).to.equal(0)
        expect(_user.bettingBalance).to.equal(_user.balance)
        try{
            await assertParentBalance(profitAndLoss)
        }catch(err){
            throw err
        }
        let _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, profitAndLoss, _bet.shares, commission
        )
        await restoreMarketMock(_bet.marketId)
    }
)

it('should only place one bet of the same userId at a time', async () => {
    let _user = await User.findById(user._id)
    expect(_user.balance).to.equal(10000)
    let betPromises = [1, 2, 3, 4].map(async (i) => {
        let marketId = marketBooks[0].marketId
        let market = objects.markets[marketId]
        let runnerId = i <= 3
            ? market.sortedRunners[0]
            : market.sortedRunners[1]
        let runner = market.runners[runnerId]
        let bet = getBet(
            marketId,
            market,
            runnerId,
            runner,
            runner.availableToBack[0].price
        )
        await bets.placeBet(bet)
    })
    let _bets = await Bet.find()
    expect(_bets.length).to.equal(0)
    await Promise.all(betPromises)
    _user = await User.findById(user._id)
    _bets = await Bet.find()
    expect(_bets.length).to.equal(1)
    expect(_user.bettingBalance).to.equal(9000)
})


it(`should calculate balance properly on placing multiple BACK bets on 
    each team`,
    async () => {
        let winningAmount = 0
        let exposure = 0
        await Promise.each([1, 2, 3, 4, 5, 6], async (i) => {
            let marketId = marketBooks[0].marketId
            let market = objects.markets[marketId]
            let runnerId = market.sortedRunners[0]
            if (i <= 6) runnerId = market.sortedRunners[2]
            if (i <= 4) runnerId = market.sortedRunners[1]
            if (i <= 2) runnerId = market.sortedRunners[0]
            let runner = market.runners[runnerId]
            let bet = getBet(
                marketId,
                market,
                runnerId,
                runner,
                runner.availableToBack[0].price
            )
            winningAmount = i <= 2
                ? winningAmount + bet.profit 
                : winningAmount - bet.exposure
            await bets.placeBet(bet)
        })
        let _bets = await Bet.find({})
        let _user = await User.findById(_bets[0].userId)
        expect(_bets.length).to.equal(6)
        _bets.forEach(bet => {
            expect(bet.matched).to.be.true
            expect(bet.profit).to.be.equal( bet.rate * bet.stake - bet.stake )
            expect(bet.exposure).to.be.equal(bet.stake)
            expect(bet.result).to.be.equal('PENDING')
        })
        exposure = 3180 // manually calculated
        expect(_user.balance).to.equal(user.balance)// calculate exposure
        expect(_user.exposure).to.equal(exposure) 
        expect(_user.bettingBalance).to.equal(_user.balance - exposure) 
        await mockMarketClose(_bets[0].marketId)
        await bets.closeBets(_bets[0].marketId)
        _user = await User.findById(_bets[0].userId)
        let commission = percentOf(winningAmount, 2)
        expect(_user.profitAndLoss).to.equal(winningAmount - commission) 
        expect(_user.balance).to.equal(user.balance + winningAmount - commission)
        expect(_user.bettingBalance).to.equal(user.balance + winningAmount - commission)
        expect(_user.exposure).to.equal(0)
        try{
            await assertParentBalance(winningAmount)
        }catch(err){
            throw err
        }
        let _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, winningAmount, _bet.shares, commission
        )
        await restoreMarketMock(_bets[0].marketId)
    }
)

it(`should calculate balance properly on placing multiple LAY bets on 
    each team`,
    async () => {
        let winningAmount = 0
        await Promise.each([1, 2, 3, 4, 5, 6], async (i) => {
            let marketId = marketBooks[0].marketId
            let market = objects.markets[marketId]
            let runnerId = market.sortedRunners[0]
            if (i <= 6) runnerId = market.sortedRunners[2]
            if (i <= 4) runnerId = market.sortedRunners[1]
            if (i <= 2) runnerId = market.sortedRunners[0]
            let runner = market.runners[runnerId]
            let bet = getBet(
                marketId,
                market,
                runnerId,
                runner,
                runner.availableToLay[0].price
            )
            bet.betType = 'LAY'
            getProfitAndExposure(bet)
            winningAmount = i > 2
                ? winningAmount + bet.profit
                : winningAmount - bet.exposure
            bet = await bets.placeBet(bet)
        })
        let _bets = await Bet.find({})
        let _user = await User.findById(_bets[0].userId)
        expect(_bets.length).to.equal(6)
        _bets.forEach(bet => {
            expect(bet.matched).to.be.true
            expect(bet.profit).to.be.equal(bet.stake)
            expect(bet.exposure).to.be.equal(bet.rate * bet.stake - bet.stake)
            expect(bet.result).to.be.equal('PENDING')
        })
        let exposure = 1200 // manually calculated
        expect(_user.balance).to.equal(user.balance)// calculate exposure
        expect(_user.exposure).to.equal(exposure)
        expect(_user.bettingBalance).to.equal(_user.balance - exposure)
        await mockMarketClose(_bets[0].marketId)
        await bets.closeBets(_bets[0].marketId)
        _user = await User.findById(_bets[0].userId)
        expect(_user.profitAndLoss).to.equal(winningAmount)
        expect(_user.balance).to.equal(user.balance + winningAmount)
        expect(_user.bettingBalance).to.equal(user.balance + winningAmount)
        expect(_user.exposure).to.equal(0)
        try{
            await assertParentBalance(winningAmount)
        }catch(err){
            throw err
        }
        let _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, winningAmount, _bet.shares
        )
        await restoreMarketMock(_bets[0].marketId)
    }
)

it(`should calculate balance properly on placing multiple BACK and LAY bets on 
    single team`,
    async () => {
        let winningAmount = 0
        await Promise.each([1, 2, 3, 4, 5], async (i) => {
            let marketId = marketBooks[0].marketId
            let market = objects.markets[marketId]
            let runnerId = market.sortedRunners[0]
            let runner = market.runners[runnerId]
            let price = runner.availableToBack[0].price
            if (i <= 3){
                price = runner.availableToLay[0].price
            }
            let bet = getBet(
                marketId,
                market,
                runnerId,
                runner,
                price
            )
            if (i <= 3) bet.betType = 'LAY' 
            getProfitAndExposure(bet)
            winningAmount = i > 3
                ? winningAmount + bet.profit
                : winningAmount - bet.exposure
            bet = await bets.placeBet(bet)
        })
        let _bets = await Bet.find({})
        let _user = await User.findById(_bets[0].userId)
        expect(_bets.length).to.equal(5)
        _bets.forEach(bet => {
            expect(bet.matched).to.be.true
            expect(bet.result).to.be.equal('PENDING')
        })
        let exposure = 3000 // manually calculated
        expect(_user.exposure).to.equal(exposure)
        expect(_user.balance).to.equal(user.balance)
        expect(_user.bettingBalance).to.equal(_user.balance - exposure)
        await mockMarketClose(_bets[0].marketId)
        await bets.closeBets(_bets[0].marketId)
        _user = await User.findById(_bets[0].userId)
        expect(_user.profitAndLoss).to.equal(winningAmount)
        expect(_user.balance).to.equal(user.balance + winningAmount)
        expect(_user.bettingBalance).to.equal(user.balance + winningAmount)
        expect(_user.exposure).to.equal(0)
        try{
            await assertParentBalance(winningAmount)
        }catch(err){
            throw err
        }
        let _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, winningAmount, _bet.shares
        )
        await restoreMarketMock(_bets[0].marketId)
    }
)

it(`should calculate balance properly on placing multiple BACK and LAY bets on 
    each team`,
    async () => {
        let winningAmount = 0
        await Promise.each([1, 2, 3, 4, 5, 6], async (i) => {
            let marketId = marketBooks[0].marketId
            let market = objects.markets[marketId]
            let runnerId = market.sortedRunners[0]
            if (i <= 6) runnerId = market.sortedRunners[2]
            if (i <= 4) runnerId = market.sortedRunners[1]
            if (i <= 2) runnerId = market.sortedRunners[0]
            let runner = market.runners[runnerId]
            let price = runner.availableToBack[0].price
            if (i % 2 === 0) {
                price = runner.availableToLay[0].price
            }
            let bet = getBet(
                marketId,
                market,
                runnerId,
                runner,
                price
            )
            if (i % 2 === 0) bet.betType = 'LAY'
            getProfitAndExposure(bet)
            let isBetWinning = (
                ((i <= 2) && bet.betType === 'BACK') || 
                ((i > 2) && bet.betType === 'LAY')
            )
            winningAmount = isBetWinning
                ? winningAmount + bet.profit
                : winningAmount - bet.exposure
            bet = await bets.placeBet(bet)
        })
        let _bets = await Bet.find({})
        let _user = await User.findById(_bets[0].userId)
        expect(_bets.length).to.equal(6)
        _bets.forEach(bet => {
            expect(bet.matched).to.be.true
            expect(bet.result).to.be.equal('PENDING')
        })
        let exposure = 1090 // manually calculated
        expect(_user.exposure).to.equal(exposure)
        expect(_user.balance).to.equal(user.balance)
        expect(_user.bettingBalance).to.equal(_user.balance - exposure)
        await mockMarketClose(_bets[0].marketId)
        await bets.closeBets(_bets[0].marketId)
        _user = await User.findById(_bets[0].userId)
        expect(_user.profitAndLoss).to.equal(winningAmount)
        expect(_user.balance).to.equal(user.balance + winningAmount)
        expect(_user.bettingBalance).to.equal(user.balance + winningAmount)
        expect(_user.exposure).to.equal(0)
        try{
            await assertParentBalance(winningAmount)
        }catch(err){
            throw err
        }
        let _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, winningAmount, _bet.shares
        )
        await restoreMarketMock(_bets[0].marketId)
    }
)

it(`should calculate balance properly on placing multiple BACK and LAY bets on 
    each team on multiple markets`,
    async () => {
        let winningAmount = 0
        await Promise.each([1,2,3,4,5,6,7,8,9,10,11,12], async (i) => {
            let marketId = i <= 6
                ? marketBooks[0].marketId
                : marketBooks[marketBooks.length - 1].marketId
            let market = objects.markets[marketId]
            let runnerId = market.sortedRunners[0]
            if ([5, 6, 11, 12].indexOf(i) >= 0) 
                runnerId = market.sortedRunners[2]
            if ([3, 4, 9, 10].indexOf(i) >= 0) 
                runnerId = market.sortedRunners[1]
            if ([1, 2, 7, 8].indexOf(i) >= 0) 
                runnerId = market.sortedRunners[0]
            let runner = market.runners[runnerId]
            let price = runner.availableToBack[0].price
            if (i % 2 === 0) {
                price = runner.availableToLay[0].price
            }
            let bet = getBet(
                marketId,
                market,
                runnerId,
                runner,
                price
            )
            if (i % 2 === 0) bet.betType = 'LAY'
            getProfitAndExposure(bet)
            let isBetWinning = (
                ((i <= 2) && bet.betType === 'BACK') ||
                ((i > 2) && bet.betType === 'LAY')
            )
            winningAmount = isBetWinning
                ? winningAmount + bet.profit
                : winningAmount - bet.exposure
            bet = await bets.placeBet(bet)
        })
        let _bets = await Bet.find({})
        let _user = await User.findById(_bets[0].userId)
        expect(_bets.length).to.equal(12)
        _bets.forEach(bet => {
            expect(bet.matched).to.be.true
            expect(bet.result).to.be.equal('PENDING')
        })
        let exposure = 1290 // manually calculated
        expect(_user.exposure).to.equal(exposure)
        expect(_user.balance).to.equal(user.balance)
        expect(_user.bettingBalance).to.equal(_user.balance - exposure)
        await mockMarketClose(marketBooks[0].marketId)
        await bets.closeBets(_bets[0].marketId)
        _user = await User.findById(_bets[0].userId)
        exposure = 200 // manually calculated
        expect(_user.exposure).to.equal(200)
        expect(_user.profitAndLoss).to.equal(winningAmount)
        expect(_user.balance).to.equal(user.balance + winningAmount)
        expect(_user.bettingBalance).to.equal(_user.balance - exposure)
        try{
            await assertParentBalance(winningAmount)
        }catch(err){
            throw err
        }
        let _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, winningAmount, _bet.shares
        )
        await mockMarketClose(marketBooks[marketBooks.length - 1].marketId)
        await bets.closeBets(marketBooks[marketBooks.length - 1].marketId)
        _user = await User.findById(_bets[0].userId)
        expect(_user.exposure).to.equal(0)
        expect(_user.profitAndLoss).to.equal(winningAmount - exposure)
        expect(_user.balance).to.equal(user.balance + winningAmount - exposure)
        expect(_user.bettingBalance).to.equal(_user.balance - _user.exposure)
        try{
            await assertParentBalance(winningAmount - exposure)
        }catch(err){
            throw err
        }
        _bet = _bets[0]
        await assertBalanceSheet(
            _bet.userId, _bet.parents, winningAmount - exposure, _bet.shares
        )
        await restoreMarketMock(marketBooks[marketBooks.length - 1].marketId)
        await restoreMarketMock(_bets[0].marketId)
    }
)

it('should calculate bets properly for matched and unmatchedBets', async () => {
    let winningAmount = 0
    let range = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
    let unmatchedIds = []
    await Promise.each(range, async (i) => {
        let marketId = i <= 8
            ? marketBooks[0].marketId
            : marketBooks[marketBooks.length - 1].marketId
        let market = objects.markets[marketId]
        let runnerId = market.sortedRunners[0]
        if ([5, 6, 13, 14].indexOf(i) >= 0)
            runnerId = market.sortedRunners[2]
        if ([3, 4, 11, 12, 8, 16].indexOf(i) >= 0)
            runnerId = market.sortedRunners[1]
        if ([1, 2, 9, 10, 7, 15].indexOf(i) >= 0)
            runnerId = market.sortedRunners[0]
        let runner = market.runners[runnerId]
        let price = runner.availableToBack[0].price
        if (i % 2 === 0) {
            price = runner.availableToLay[0].price
        }
        let bet = getBet(
            marketId,
            market,
            runnerId,
            runner,
            price
        )
        if (i % 2 === 0) bet.betType = 'LAY'
        if ([7,8,15,16].indexOf(i) >= 0) bet.rate = 1.3
        getProfitAndExposure(bet)
        bet = await bets.placeBet(bet)
        if ([7, 8, 15, 16].indexOf(i) >= 0) unmatchedIds.push('' + bet._id)
    })
    let _bets = await Bet.find({})
    let _user = await User.findById(_bets[0].userId)
    expect(_bets.length).to.equal(16)
    _bets.forEach(bet => {
        unmatchedIds.indexOf(''+bet._id) >= 0
            ? expect(bet.matched).to.be.false
            : expect(bet.matched).to.be.true
        expect(bet.result).to.be.equal('PENDING')
    })
    let exposure = 3890 // manually calculated
    expect(_user.exposure).to.equal(exposure)
    expect(_user.balance).to.equal(user.balance)
    expect(_user.bettingBalance).to.equal(_user.balance - exposure)
    await mockMarketClose(marketBooks[0].marketId)
    await bets.closeBets(_bets[0].marketId)
    _user = await User.findById(_bets[0].userId)
    exposure = 1500 // manually calculated 
    winningAmount = -200 // manually calculated
    expect(_user.exposure).to.equal(exposure)
    expect(_user.profitAndLoss).to.equal(winningAmount)
    expect(_user.balance).to.equal(user.balance + winningAmount)
    expect(_user.bettingBalance).to.equal(_user.balance - exposure)
    try{
        await assertParentBalance(winningAmount)
    }catch(err){
        throw err
    }
    let _bet = _bets[0]
    await assertBalanceSheet(
        _bet.userId, _bet.parents, winningAmount, _bet.shares
    )
    await mockMarketClose(marketBooks[marketBooks.length - 1].marketId)
    await bets.closeBets(marketBooks[marketBooks.length - 1].marketId)
    _user = await User.findById(_bets[0].userId)
    winningAmount = -400 // manually calculated
    expect(_user.exposure).to.equal(0)
    expect(_user.profitAndLoss).to.equal(winningAmount) 
    expect(_user.balance).to.equal(user.balance + winningAmount) 
    expect(_user.bettingBalance).to.equal(_user.balance - _user.exposure)
    try{
        await assertParentBalance(winningAmount)
    }catch(err){
        throw err
    }
    _bet = _bets[0]
    await assertBalanceSheet(
        _bet.userId, _bet.parents, winningAmount, _bet.shares
    )
    await restoreMarketMock(marketBooks[marketBooks.length - 1].marketId)
    await restoreMarketMock(_bets[0].marketId)
})


it('should close market properly and save it to database', async () => {
    closeAMarket()
    let marketId = marketBooks[0].marketId
    let runners = marketBooks[0].runners
    let winnerId = runners[0].selectionId
    let loserIds = [runners[1].selectionId, runners[2].selectionId]
    await delay(1600)
    let _market = await Market.findOne({ marketId })
    _market = _market.toObject()
    expect(_market.status).to.equal('CLOSED')
    expect(_market.runners[winnerId].status).to.equal('WINNER')
    expect(loserIds.map(loserId => {
        return _market.runners[loserId].status
    })).to.eql(['LOSER', 'LOSER'])
})

// todo: add settleClosedMarketsBeforeStart function
// todo: add close custom markets function

/* eslint no-unused-expressions: "off" */