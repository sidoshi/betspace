let User = require('_models/user')

const displayUser = async () => {
    let U = await User.find()
    let Us = U.map( u => {
            u = u.toObject()
            delete u.updatedAt
            delete u.createdAt
            delete u.blocked
            delete u.password
            delete u.online
            delete u.__v
            delete u.ipAddress
            delete u.parentBlocked
            delete u.parents
            delete u.children
            delete u._id
            delete u.directParent
            delete u.grandParent
            delete u.userType
            return u
    })
    console.log()
    console.table(Us)
    console.log()
}

module.exports = displayUser