let expect = require('chai').expect
let Promise = require('bluebird')
let { percentOf } = require('_helpers/numberUtils')

let BalanceSheet = require('_models/balanceSheet')
let {
    getDirectParent, 
    getDirectChild
} = require('_helpers/updateBalanceSheet/getRelatives')

const assertLossBalanceSheet = async (selfId, parents, loss, shares) => {
    loss = -loss
    let balanceSheet = await BalanceSheet.findOne({ userId: selfId })
    balanceSheet = balanceSheet.toObject()
    expect(balanceSheet.debit[selfId]).to.equal(loss)
    let directParent = getDirectParent({
        userId: selfId,
        parents: parents
    }, selfId)
    expect(balanceSheet.credit[directParent.userId]).to.equal(loss)
    let parentsReverse = [...parents].reverse()

    let remainingParentsAmount = loss
    let bettorId = selfId
    await Promise.each( parentsReverse, async parent => {
        let selfId = parent.userId
        let directChild = getDirectChild({
            userId: bettorId,
            parents: parents
        }, selfId)
        let directParent = getDirectParent({
            userId: bettorId,
            parents: parents
        }, selfId)
        let directChildId = directChild && directChild.userId
        let directParentId = directParent && directParent.userId
        let balanceSheet = await BalanceSheet.findOne({ userId: selfId })
        let selfRatio = shares[selfId]
        let selfAmount = percentOf(loss, selfRatio)
        balanceSheet = balanceSheet.toObject()
        expect(balanceSheet.debit[directChildId])
            .to.equal(remainingParentsAmount)
        expect(balanceSheet.credit[selfId]).to.equal(selfAmount)
        if (directParentId) {
            remainingParentsAmount -= selfAmount
            expect(balanceSheet.credit[directParentId])
                .to.equal(remainingParentsAmount)
        }
    })
}

const assertProfitBalanceSheet = async (
    selfId, 
    parents, 
    profit, 
    shares,
    commission
) => {
    let balanceSheet = await BalanceSheet.findOne({ userId: selfId })
    balanceSheet = balanceSheet.toObject()
    expect(balanceSheet.credit[selfId]).to.equal(profit - commission)
    let directParent = getDirectParent({
        userId: selfId,
        parents: parents
    }, selfId)
    expect(balanceSheet.debit[directParent.userId]).to.equal(profit - commission)
    let parentsReverse = [...parents].reverse()

    let remainingParentsAmount = profit
    let bettorId = selfId
    await Promise.each(parentsReverse, async parent => {
        let selfId = parent.userId
        let directChild = getDirectChild({
            userId: bettorId,
            parents: parents
        }, selfId)
        let directParent = getDirectParent({
            userId: bettorId,
            parents: parents
        }, selfId)
        let directChildId = directChild && directChild.userId
        let directParentId = directParent && directParent.userId
        let balanceSheet = await BalanceSheet.findOne({ userId: selfId })
        let selfRatio = shares[selfId]
        let selfAmount = percentOf(profit, selfRatio)
        balanceSheet = balanceSheet.toObject()
        expect(balanceSheet.credit[directChildId])
            .to.equal(remainingParentsAmount - commission)
        if(directParentId){
            remainingParentsAmount -= selfAmount
            expect(balanceSheet.debit[directParentId])
                .to.equal(remainingParentsAmount - commission)
            expect(balanceSheet.debit[selfId]).to.equal(selfAmount)
        }else{
            expect(balanceSheet.debit[selfId]).to.equal(selfAmount - commission)
        }
    })
}

module.exports = async (...args) => {
    let profitAndLoss = args[2]
    if(profitAndLoss > 0) await assertProfitBalanceSheet(...args)
    else if (profitAndLoss < 0) await assertLossBalanceSheet(...args)
}