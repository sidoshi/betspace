const hash = require('sha256')

let User = require('_models/user')
let BalanceSheet = require('_models/balanceSheet.js')


const USER_BOILERPLATE = {
    password: hash('password'),
    blocked: false,
    parents: [],
    directParent: null,
    grandParent: null,
    children: [],
    credit: 0,
    balance: 0,
    bettingBalance: 0,
    exposure: 0,
    profitAndLoss: 0,
    ipAddress: [],
    availableShare: 0,
    childCredit: 0
}

let BALANCE_SHEET_BOILERPLATE = {
    userId: null,
    debit: {},
    credit: {}
}

let MAIN_ADMIN_SHARE   = 100  // 30
let SUPER_MASTER_SHARE = 70   // 40
let MASTER_SHARE       = 30   // 30
let CLIENT_SHARE       = 0    // 0

let MAIN_ADMIN_CREDIT   = 50000  // 20000
let SUPER_MASTER_CREDIT = 30000  // 10000
let MASTER_CREDIT       = 20000  // 10000
let CLIENT_CREDIT       = 10000  // 10000

const getParents = parent => {
    let directParent = {
        userId:   parent._id,
        userType: parent.userType,
        username: parent.username
    }

    let parents = parent.parents
    parents.push(directParent)

    let grandParent = parent.directParent

    return {
        parents,
        directParent,
        grandParent
    }
}

const setUpMainAdmin = async () => {
    let mainAdmin            = Object.assign( {}, USER_BOILERPLATE )
    mainAdmin.username       = 'mainAdmin'
    mainAdmin.userType       = 'MAIN ADMIN'
    mainAdmin.credit         = MAIN_ADMIN_CREDIT
    mainAdmin.balance        = MAIN_ADMIN_CREDIT
    mainAdmin.availableShare = MAIN_ADMIN_SHARE
    
    mainAdmin                = new User(mainAdmin)
    let balanceSheet = Object.assign({}, BALANCE_SHEET_BOILERPLATE)
    balanceSheet.userId = mainAdmin._id
    await new BalanceSheet(balanceSheet).save()
    return await mainAdmin.save()
}

const setUpSuperMaster = async mainAdmin => {
    let superMaster = Object.assign( {}, USER_BOILERPLATE )
    superMaster.username = 'superMaster'
    superMaster.userType = 'SUPER MASTER'
    superMaster.credit = SUPER_MASTER_CREDIT
    superMaster.balance = SUPER_MASTER_CREDIT
    superMaster.availableShare = SUPER_MASTER_SHARE
    let { directParent, grandParent, parents } = getParents(mainAdmin)
    superMaster.directParent = directParent
    superMaster.parents = parents
    superMaster.grandParent = grandParent
    superMaster = new User(superMaster)

    mainAdmin = await User.findById(mainAdmin._id)
    mainAdmin.children.push(superMaster._id)
    mainAdmin.balance -= SUPER_MASTER_CREDIT
    mainAdmin.childCredit += SUPER_MASTER_CREDIT
    mainAdmin.share -= SUPER_MASTER_SHARE

    let balanceSheet = Object.assign({}, BALANCE_SHEET_BOILERPLATE)
    balanceSheet.userId = superMaster._id
    await new BalanceSheet(balanceSheet).save()
 
    mainAdmin = await mainAdmin.save()
    superMaster = await superMaster.save()

    return {
        mainAdmin,
        superMaster
    }
}

const setUpMaster = async superMaster => {
    let master = Object.assign( {}, USER_BOILERPLATE )
    master.username = 'master'
    master.userType = 'MASTER'
    master.credit = MASTER_CREDIT
    master.balance = MASTER_CREDIT
    master.availableShare = MASTER_SHARE
    let { directParent, grandParent, parents } = getParents(superMaster)
    master.directParent = directParent
    master.parents = parents
    master.grandParent = grandParent
    master = new User(master)

    superMaster = await User.findById(superMaster._id)
    superMaster.children.push(master._id)
    superMaster.balance -= MASTER_CREDIT
    superMaster.childCredit += MASTER_CREDIT
    superMaster.share -= MASTER_SHARE
 
    let balanceSheet = Object.assign({}, BALANCE_SHEET_BOILERPLATE)
    balanceSheet.userId = master._id
    await new BalanceSheet(balanceSheet).save()

    master = await master.save()
    superMaster = await superMaster.save()

    return {
        master,
        superMaster
    }
}

const setUpClient = async master => {
    let client = Object.assign( {}, USER_BOILERPLATE )
    client.username = 'client'
    client.userType = 'CLIENT'
    client.credit = CLIENT_CREDIT
    client.balance = CLIENT_CREDIT
    client.bettingBalance = CLIENT_CREDIT
    client.availableShare = CLIENT_SHARE
    let { directParent, grandParent, parents } = getParents(master)
    client.directParent = directParent
    client.parents = parents
    client.grandParent = grandParent
    client = new User(client)

    master = await User.findById(master._id)
    master.children.push(client._id)
    master.balance -= CLIENT_CREDIT
    master.childCredit += CLIENT_CREDIT
    master.share -= CLIENT_SHARE

    let balanceSheet = Object.assign({}, BALANCE_SHEET_BOILERPLATE)
    balanceSheet.userId = client._id
    await new BalanceSheet(balanceSheet).save()
 
    master = await master.save()
    client = await client.save()

    return {
        master,
        client
    }
}

const setUpUsers = async () => {
    let mainAdmin, superMaster, master, client, users

    mainAdmin                  = await setUpMainAdmin()
    
    users = await setUpSuperMaster(mainAdmin) 
    mainAdmin = users.mainAdmin
    superMaster = users.superMaster

    users = await setUpMaster(superMaster) 
    superMaster = users.superMaster
    master = users.master

    users = await setUpClient(master)
    master = users.master
    client = users.client

    return {
        client,
        master,
        superMaster,
        mainAdmin
    }
}

/* eslint no-unexpected-multiline: off */

module.exports = setUpUsers