let placeBet            = require('_lib/betfair/bets').placeBet
let cancelBet           = require('_lib/betfair/bets').cancelBet
let io                  = require('_api/socket/io').io

const handleClient = client => {
    client.on('PLACE_BET', bet => {
        placeBet(bet)
    })
    client.on('CANCEL_BET', bet => {
        cancelBet(bet)
    })
    client.on('NOTIFY', data => {
        io.emit('NOTIFY', data) 
    })
    client.on('SHARE', data => {
        io.emit(data.event, data.data)
    })
}

module.exports = handleClient
