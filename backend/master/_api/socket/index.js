let io                      = require('./io').io
let handleClient            = require('./client')
let secret                  = require('_config').jwt.secret

const authorise = (socket, next) => {
    socket.handshake.query.secret === secret &&
    next()
}

const createSocketApi = () => {
    io.use(authorise)
    io.on( 'connection', client => {
        debug('Client connected')
        handleClient(client) // handle socket events for betfair
    })
}

module.exports.createSocketApi      = createSocketApi
