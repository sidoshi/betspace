let http                = require('http')
let sio                 = require('socket.io')

let server              = http.createServer()
let io                  = sio(server)

module.exports.server               = server
module.exports.io                   = io
