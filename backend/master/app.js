require('app-module-path').addPath(__dirname)
require('app-module-path').addPath(__dirname + '/../')
require('waitjs')

let mongoose            = require('mongoose')
global.debug            = require('_helpers/debug')

let server              = require('_api/socket/io').server
let config              = require('_config')
let createSocketApi     = require('_api/socket').createSocketApi
let betfair             = require('_lib/betfair')
let setUpMainAdmin      = require('_lib/setUpMainAdmin')

let env = process.env.NODE_ENV
env === 'production' && require('log-timestamp')
mongoose.connect(config.mongo.url[env], config.mongo.options)
mongoose.Promise = global.Promise

createSocketApi()
setUpMainAdmin()
betfair.startCycle()

server.listen(config.master.port)
