const defaultMainAdmins = [
    {
	    username : "mainAdmin",
	    password : "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
	    userType : "MAIN ADMIN",
	    blocked : false,
	    parents : [ ],
	    directParent : null,
        children: [],
	    credit : 50000,
	    balance : 50000,
	    bettingBalance : 0,
	    exposure : 0,
	    profitAndLoss : 0,
        ipAddress: [],
	    online : false,
		availableShare: 100,
		childCredit: 0
    }
]

module.exports = defaultMainAdmins