const mongo = {
    url:        {
        test: 'mongodb://127.0.0.1/betspace-test',
        development: 'mongodb://127.0.0.1/betspace',
        production: 'mongodb://127.0.0.1/betspace'
    },
    options:    {}
}

module.exports = mongo
