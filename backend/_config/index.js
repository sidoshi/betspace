let master              = require('./master')
let slave               = require('./slave')
let betfair             = require('./betfair')
let jwt                 = require('./jwt')
let mongo               = require('./mongo')
let redis               = require('./redis')
let cookie              = require('./cookie')
let defaultMainAdmins   = require('./defaultMainAdmins')

let config = {
    master,
    slave,
    betfair,
    jwt,
    mongo,
    redis,
    cookie,
    defaultMainAdmins
}

module.exports = config
